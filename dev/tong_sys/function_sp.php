<?php
ini_set('date.timezone', 'Asia/Jakarta');
include('sqlsrv.php');

function SP_AU_Pasien($array) {
  global $dbConnection;
  /* SP_AU_Pasien */
  // $stmt = $dbConnection->prepare('EXEC AU_Pasien ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?');
  /* GANTI KESINI */
  $stmt = $dbConnection->prepare('EXEC AU_Pasien_Pendaftaran ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?');
  $key = 0;
  for ($i=1; $i <= count($array); $i++) { 
    $stmt->bindParam($i, $array[$key++]);
  }
  $stmt->execute();
  /* Sampai Sini */
}

function SP_Add_RegistrasiPasienMRSPilihDokterRJADM($array) {
  global $dbConnection;
  /* SP_Add_RegistrasiPasienMRSPilihDokterRJADM */
  // $stmt = $dbConnection->prepare('EXEC Add_RegistrasiPasienMRSPilihDokterRJADM ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?');
  // $stmt = $dbConnection->prepare('EXEC Add_RegistrasiPasienMRS_HILDA ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?');
  $stmt = $dbConnection->prepare('EXEC Add_RegistrasiPasienMRS_HILDA_NEW_BACKUP ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?');
  $key = 0;
  for ($i=1; $i <= count($array); $i++) { 
    $stmt->bindParam($i, $array[$key++]);
  }
  $stmt->execute();
  /* Sampai Sini */
}

function SP_Add_RegistrasiPasienMRSPilihDokterRJADM_USG_Anak($array) {
  global $dbConnection;
  /* SP_Add_RegistrasiPasienMRSPilihDokterRJADM */
  $stmt = $dbConnection->prepare('EXEC Add_RegistrasiPasienMRS_HILDA_NEW ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?');
  $key = 0;
  for ($i=1; $i <= count($array); $i++) { 
    $stmt->bindParam($i, $array[$key++]);
  }
  $stmt->execute();
  /* Sampai Sini */
}

function SP_Add_Pasien_Perjanjian_USG_Anak($array) {
  global $dbConnection;
  $stmt = $dbConnection->prepare('EXEC Add_Pasien_Perjanjian ?,?,?,?,?,?,?,?,?,?,?,?');
  $key = 0;
  for ($i=1; $i <= count($array); $i++) { 
    $stmt->bindParam($i, $array[$key++]);
  }
  $stmt->execute();
}

function SP_Add_Pasien_Perjanjian($array) {
  global $dbConnection;
  $stmt = $dbConnection->prepare('EXEC Add_Pasien_Perjanjian_BACKUP ?,?,?,?,?,?,?,?,?,?,?,?');
  $key = 0;
  for ($i=1; $i <= count($array); $i++) { 
    $stmt->bindParam($i, $array[$key++]);
  }
  $stmt->execute();
}

function SP_Add_RegistrasiPasienMRS($array) {
  global $dbConnection;
  /* SP_Add_RegistrasiPasienMRS */
  $stmt = $dbConnection->prepare('EXEC Add_RegistrasiPasienMRS ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?');
  $key = 0;
  for ($i=1; $i <= count($array); $i++) { 
    $stmt->bindParam($i, $array[$key++]);
  }
  $stmt->execute();
  /* Sampai Sini */
}

function SP_Add_RegistrasiPasienRI($array) {
  global $dbConnection;
  /* SP_Add_RegistrasiPasienRI */
  $stmt = $dbConnection->prepare('EXEC Add_RegistrasiPasienRI ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?');
  $key = 0;
  for ($i=1; $i <= count($array); $i++) { 
    $stmt->bindParam($i, $array[$key++]);
  }
  $stmt->execute();
  /* Sampai Sini */
}

function SP_Add_PasienMasukKamar($array) {
  global $dbConnection;
  /* SP_Add_PasienMasukKamar */
  $stmt = $dbConnection->prepare('EXEC Add_PasienMasukKamar ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?');
  $key = 0;
  for ($i=1; $i <= count($array); $i++) { 
    $stmt->bindParam($i, $array[$key++]);
  }
  $stmt->execute();
  /* Sampai Sini */
}

function SP_AU_AsuransiPasienJoinProgramAskesMultipel($array) {
  global $dbConnection;
  /* SP_AU_AsuransiPasienJoinProgramAskesMultipel */
  $stmt = $dbConnection->prepare('EXEC AU_AsuransiPasienJoinProgramAskesMultipel ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?');
  $key = 0;
  for ($i=1; $i <= count($array); $i++) { 
    $stmt->bindParam($i, $array[$key++]);
  }
  $stmt->execute();
  /* Sampai Sini */
}

function SP_AU_AsuransiPasienJoinProgramAskes($array) {
  global $dbConnection;
  /* SP_AU_AsuransiPasienJoinProgramAskes */
  $stmt = $dbConnection->prepare('EXEC AU_AsuransiPasienJoinProgramAskes ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?');
  $key = 0;
  for ($i=1; $i <= count($array); $i++) { 
    $stmt->bindParam($i, $array[$key++]);
  }
  $stmt->execute();
  /* Sampai Sini */
}

function SP_Add_BiayaPelayananOtomatisNew($array) {
  /* MATIIN DULU SAMPAI WAKTU YANG BELUM DITENTUKAN */ 
  global $dbConnection;
  /* SP_Add_BiayaPelayananOtomatisNew */
  $stmt = $dbConnection->prepare('EXEC Add_BiayaPelayananOtomatisNew ?,?,?,?,?,?,?,?,?');
  $key = 0;
  for ($i=1; $i <= count($array); $i++) { 
   $stmt->bindParam($i, $array[$key++]);
  }
  $stmt->execute();
  /* Sampai Sini */
}

function SP_Add_RegistrasiLaboratoryLsg ($array) {
  global $dbConnection;
  /* SP_Add_RegistrasiLaboratoryLsg */
  /* UPDATE 2022-01-04 */
  $stmt = $dbConnection->prepare('EXEC Add_RegistrasiLaboratoryLsg_HILDA ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?');
  $key = 0;
  for ($i=1; $i <= count($array); $i++) { 
    $stmt->bindParam($i, $array[$key++]);
  }
  $stmt->execute();
  /* Sampai Sini */
}