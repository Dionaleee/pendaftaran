<?php
session_start();
if ( !isset($_SESSION['PIN_ADM']) ) {
  echo "<script>alert('sesi anda telah habis. silahkan login kembali.'); window.location.href='index.php'</script>";
}
include('tong_sys/sqlsrv.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Dashboard Pendaftaran</title>

  <!-- Custom fonts for this template-->
  <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Titillium+Web:ital,wght@0,400;0,600;0,700;0,900;1,400;1,600;1,700&display=swap" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="assets/css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="assets/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

  <style>
    html, body, td {
      font-family: 'Titillium Web', sans-serif;
      color: #202124;
    }

    #content-wrapper {
      background-image: url('assets/img/bg.png');
      background-size: cover;
    }
    <?php if ( $_GET['modul'] != 'daftar_pasien' || $_GET['modul'] != 'daftar_pasien_perjanjian' || $_GET['modul'] != 'user' || $_GET['modul'] != 'laporan' ) : ?>
    #wrapper {
      min-height: 100vh;
    }
    <?php endif ?>
    
    .pointer {
      cursor: pointer;
    }
    .pointer:hover {
      /* border: 2px solid rgb(165 165 165 / 30%); */
      -webkit-box-shadow: 0 .125rem .25rem 0 rgba(58,59,69,.2)!important;
      box-shadow: 0 0.025rem 0.45rem 0 rgba(58,59,69,0.6)!important;
    }
    .text-pointer:hover {
      font-size: 1.5em !important;
    }
    .text-pointer-ranap:hover {
      font-size: 1.3em !important;
    }
    .poli-card:hover {
      border: solid 1px #CCC;
      -moz-box-shadow: 1px 1px 5px #0a0a0a;
      -webkit-box-shadow: 1px 1px 5px #0a0a0a;
      box-shadow: 1px 1px 5px #0a0a0a;
    }
    .teks-dalam-gambar {
      font-size: 1.7rem;
      position: absolute;
      text-align: center;
      padding-left: 10px;
      padding-right: 10px;
      bottom: 20px;
      width: 100%;
      font-weight: bold;
      color: #fff;
      -webkit-text-stroke: 1.8px black;
    }

    .custom-control-label::before, 
    .custom-control-label::after {
      top: .2rem;
      width: 1.25rem;
      height: 1.25rem;
    }
  </style>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php $sidebar = false; if ( $_GET['modul'] == 'daftar_pasien' || $_GET['modul'] == 'daftar_pasien_perjanjian' || $_GET['modul'] == 'user' || $_GET['modul'] == 'laporan' ) : ?>
      <!-- Sidebar -->
      <?php include('sidebar/index.php') ?>
      <!-- End of Sidebar -->
    <?php endif ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <div class="bg-hilda"></div>
      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>
          <?php if ( !$sidebar ) : ?>
          <div class="font-weight-bold m-0" onclick="window.location.href='page.php?modul=pendaftaran'" style="font-size: 1.3rem; cursor: pointer;">Aplikasi Pendaftaran</div>
          <?php endif; ?>

          <!-- Topbar Search -->
          <form class="d-none d-sm-inline-block form-inline ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" id="cari-pasien">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
            <div id="suggesstion-box-pasien" style="position: absolute; z-index: 9999;"></div>
          </form>
          <a href="page.php?modul=daftar_pasien_baru" class="ml-2 btn btn-success">Daftar Pasien Baru</a>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $_SESSION['NamaPegawai_ADM'] ?></span>
                <img class="img-profile rounded-circle" src="assets/img/user.png">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="modul/login/proses_login.php?act=do_logout">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
          <?php if ( $_GET['modul'] == 'pendaftaran' ) : ?>
            <?php
              unset($_SESSION['data_pasien']);
              unset($_SESSION['NamaPJ']);
              unset($_SESSION['AlamatPJ']);
              unset($_SESSION['TeleponPJ']);
              unset($_SESSION['HubunganPJ']);
              unset($_SESSION['PropinsiPJ']);
              unset($_SESSION['KotaPJ']);
              unset($_SESSION['KecamatanPJ']);
              unset($_SESSION['KelurahanPJ']);
              unset($_SESSION['RTRWPJ']);
              unset($_SESSION['KodePosPJ']);
              unset($_SESSION['PekerjaanPJ']);
              unset($_SESSION['NoIdentitasPJ']);
            ?>
            <form action="modul/daftar_1_cek_data_peserta/process.php" method="POST">
            <div class="row">
              <div class="col-7 col-md-4">
                <input type="text" class="form-control" name="nopasien" id="nopasien" autofocus placeholder="Masukkan NoRM / NoBPJS">
              </div>
              <div class="col-2">
                <button type="submit" class="btn btn-primary">Daftar</button>
              </div>
            </div>
            </form>
          <?php else : ?>
            <?php include('modul/index.php') ?>
          <?php endif ?>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Aplikasi Pendaftaran RSUD Ciracas - 2020</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="ajaxModal" role="dialog"></div>

  <!-- Print Area -->
  <div id="print-area-1" class="print-area"></div>
  <textarea id="printing-css" style="display:none;">html,body,div,span,applet,object,iframe,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,caption,article,aside,canvas,details,embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,mark,audio,video{margin:0;padding:0;border:0;font-size:10px;font:inherit;vertical-align:baseline}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:1}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:before,blockquote:after,q:before,q:after{content:'';content:none}table{border-collapse:collapse;border-spacing:0}body{font:normal normal .8125em/1.4 Arial,Sans-Serif;background-color:white;color:#333}img {vertical-align: middle;border: 0;}strong,b{font-weight:bold}cite,em,i{font-style:italic}a{text-decoration:none}a:hover{text-decoration:underline}a{border:none}abbr,acronym{border-bottom:1px dotted;cursor:help}sup,sub{vertical-align:baseline;position:relative;top:-.4em;font-size:86%}sub{top:.4em}small{font-size:86%}kbd{font-size:80%;border:1px solid #999;padding:2px 5px;border-bottom-width:2px;border-radius:3px}mark{background-color:#ffce00;color:black}p,blockquote,pre,table,figure,hr,form,ol,ul,dl{margin:0em 0}hr{height:1px;border:none;background-color:#666}h4,h5,h6{font-weight:bold;line-height:normal;margin:1.5em 0 0}h4{font-size:140%}h5{font-size:120%}h6{font-size:100%}ol,ul,dl{margin-left:3em}ol{list-style:decimal outside}ul{list-style:disc outside}li{margin:.5em 0}dt{font-weight:bold}dd{margin:0 0 .5em 2em}input,button,select,textarea{font:inherit;font-size:100%;line-height:normal;vertical-align:baseline}textarea{display:block;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}pre,code{font-family:"Courier New",Courier,Monospace;color:inherit}pre{white-space:pre;word-wrap:normal;overflow:auto}blockquote{margin-left:2em;margin-right:2em;border-left:4px solid #ccc;padding-left:1em;font-style:italic}table[border="1"] th,table[border="1"] td,table[border="1"] caption{border:1px solid;padding:.5em 1em;text-align:left;vertical-align:top}th{font-weight:bold}table[border="1"] caption{border:none;font-style:italic}.no-print{display:none}table,tbody,tr{margin:0;padding:0;border:0;font-size:15px;}h1,h2{font-weight:bold;line-height:normal;font-size:100%}td{padding-bottom:3px;border:0;font-size:15px;}</textarea>
  <iframe id="printing-frame" name="print_frame" src="about:blank" style="display:none;"></iframe>

  <!-- Bootstrap core JavaScript-->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="assets/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="assets/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="assets/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="assets/js/demo/datatables-demo.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<?php
  include('modul/all_script.php');
?>

</body>

</html>
