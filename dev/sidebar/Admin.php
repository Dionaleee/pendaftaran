<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="page.php?modul=pendaftaran">
    <div class="sidebar-brand-icon">
      <img src="assets/img/icon.png" alt="RSUD Ciracas" height="60">
    </div>
    <div class="sidebar-brand-text">RSUD Ciracas<div style="font-size: 0.7rem;">Martina, S. Tr.Keb</div></div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <li class="nav-item <?= $_GET['modul'] == 'daftar_pasien' ? 'active' : '' ?>">
    <a class="nav-link" href="page.php?modul=daftar_pasien">
      <i class="fas fa-fw fa-list"></i>
      <span>Daftar Pasien</span></a>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <li class="nav-item <?= $_GET['modul'] == 'daftar_pasien_perjanjian' ? 'active' : '' ?>">
    <a class="nav-link" href="page.php?modul=daftar_pasien_perjanjian">
      <i class="fas fa-fw fa-list"></i>
      <span>Daftar Pasien Perjanjian</span></a>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <li class="nav-item <?= $_GET['modul'] == 'laporan' ? 'active' : '' ?>">
    <a class="nav-link <?= $_GET['modul'] == 'laporan' ? '' : 'collapsed' ?>" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
      <i class="fas fa-fw fa-cog"></i>
      <span>Laporan</span>
    </a>
    <div id="collapseTwo" class="collapse <?= $_GET['modul'] == 'laporan' ? 'show' : '' ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item <?= $_GET['sub_modul'] == 'kunjungan_harian' ? 'active' : '' ?>" href="page.php?modul=laporan&sub_modul=kunjungan_harian">Kunjungan Harian</a>
        <a class="collapse-item <?= $_GET['sub_modul'] == 'kunjungan_bulanan' ? 'active' : '' ?>" href="page.php?modul=laporan&sub_modul=kunjungan_bulanan">Kunjungan Bulanan</a>
        <a class="collapse-item <?= $_GET['sub_modul'] == 'rekap_perbulan' ? 'active' : '' ?>" href="page.php?modul=laporan&sub_modul=rekap_perbulan">Rekap Per Bulan</a>
        <a class="collapse-item <?= $_GET['sub_modul'] == 'kunjungan_per_wilayah' ? 'active' : '' ?>" href="page.php?modul=laporan&sub_modul=kunjungan_per_wilayah">Kunjungan Per Wilayah</a>
      </div>
    </div>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>