<?php session_start(); session_unset(); session_destroy(); ?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Login Pendaftaran RSUD Ciracas</title>

  <!-- Custom fonts for this template-->
  <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="assets/css/sb-admin-2.min.css" rel="stylesheet">

  <style>
    body {
      background-image: url('assets/img/bg.png');
      background-size: cover;
    }
  </style>

</head>

<body>

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-5 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Login Pendaftaran RSUD Ciracas</h1>
              </div>
              <form class="user" method="POST" action="modul/login/proses_login.php?act=do_login">
                <div class="form-group">
                  <input type="text" class="form-control form-control-user" name="pin" id="pin" placeholder="Enter PIN..." autofocus>
                </div>
                <div class="form-group">
                  <input type="password" class="form-control form-control-user" name="pass" id="pass" placeholder="Password">
                </div>
                <div class="form-group">
                  <select class="form-control" name="klinik_bidan" id="klinik_bidan">
                    <option value="bidan">Bidan</option>
                    <option value="klinik">Klinik</option>
                  </select>
                  <small id="klinik_bidan" class="form-text text-danger font-weight-bold font-italic">Pilih Klinik Untuk Login Di Klinik Hilda Alnaira.</small>
                </div>
                <button type="submit" class="btn btn-primary btn-user btn-block">
                  Login
                </button>
              </form>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="assets/js/sb-admin-2.min.js"></script>

</body>

</html>
