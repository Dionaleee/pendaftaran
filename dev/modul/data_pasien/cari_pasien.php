<?php
    $PasienParam = $TglLahir = '';
    if ( isset($_GET['NoPasien']) ) {
        $PasienParam = $_GET['NoPasien'];
    }

    if ( isset($_POST['NoPasien']) ) {
        $PasienParam = $_POST['NoPasien'];
        if ( $_POST['TglLahir'] != '' ) {
            $TglLahir = date('Y-m-d', strtotime($_POST['TglLahir']));
        }
    }
?>
<div class="card mb-3 shadow-sm">
    <div class="card-body">
        <form method="POST">
            <div class="form-row">
                <div class="form-group col-md-5">
                    <label for="NoPasien">No RM / NIK / Nama Pasien</label>
                    <input type="text" class="form-control" name="NoPasien" id="NoPasien" placeholder="No RM / NIK / Nama Pasien" value="<?= $PasienParam ?>">
                </div>
                <div class="form-group col-md-5">
                    <label for="TglLahir">Tgl Lahir</label>
                    <input type="date" class="form-control" name="TglLahir" id="TglLahir" value="<?= $TglLahir ?>">
                </div>
                <div class="form-group col-md-2">
                    <label class="d-none d-md-block">&nbsp;</label>
                    <button class="btn btn-danger w-100" type="submit">Cari</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="card mb-3 shadow-sm">
    <div class="card-body">
        <table class="table table-bordered table-hover">
            <thead class="thead-dark">
                <tr>
                    <th>NoCM</th>
                    <th>NamaPasien</th>
                    <th>NIK</th>
                    <th>TglLahir</th>
                    <th>AlamatLengkap</th>
                    <th class="text-center">#</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if ( $TglLahir != '' ) {
                        $ParamTglLahir = "AND TglLahir = '$TglLahir'";
                    } else {
                        $ParamTglLahir = "";
                    }
                    $data_pasien = $dbConnection->query("SELECT * FROM Pasien WHERE NoCM LIKE '%$PasienParam%' OR NoIdentitas LIKE '%$PasienParam%' OR NamaLengkap LIKE '%$PasienParam%' $ParamTglLahir ORDER BY NamaLengkap")->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($data_pasien as $row) :
                ?>
                <tr>
                    <td><?= $row['NoCM'] ?></td>
                    <td><?= $row['NamaLengkap'] ?></td>
                    <td><?= $row['NoIdentitas'] ?></td>
                    <td><?= date('Y-m-d', strtotime($row['TglLahir'])) ?></td>
                    <td><?= $row['Alamat'] ?></td>
                    <td class="text-center"><a class="btn btn-sm btn-primary" href="page.php?modul=data_pasien&NoCM=<?= $row['NoCM'] ?>">Daftar</a></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <small class="font-italic"><?= count($data_pasien) ?> row(s) affected</small>
    </div>
</div>