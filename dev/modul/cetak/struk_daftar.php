<style>
    html, body, div {
        margin: 0;
        padding: 0;
        text-align: center;
        font-size: 12px;
    }
</style>
<?php
session_start();
include('../../tong_sys/sqlsrv.php');
$NoPendaftaran = $_GET['nopendaftaran'];

$stmt = $dbConnection->prepare("SELECT a.NoPendaftaran, d.NoRM, CONVERT(VARCHAR(10), a.TglMasuk, 20) AS TglMasuk, b.NamaRuangan, c.NamaLengkap AS NamaDokter, a.JamMulai, a.JamSelesai, a.NoAntrian, dbo.S_HitungUmur(d.TglLahir, a.TglMasuk) AS Umur, g.NamaLengkap AS NamaPetugas, d.NamaLengkap AS NamaPasien, CONVERT(VARCHAR(20), d.TglLahir, 20) AS TglLahir, d.JenisKelamin AS JK, i.NamaInstalasi,k.jenispasien
FROM
RegistrasiRJJam a LEFT JOIN
Ruangan b ON a.KdRuangan = b.KdRuangan LEFT JOIN
DataPegawai c ON a.IdDokter = c.IdPegawai LEFT JOIN
Pasien d ON a.NoCM = d.NoCM LEFT JOIN
PasienMasukRumahSakit f ON a.NoPendaftaran = f.NoPendaftaran AND a.NoCM = f.NoCM LEFT JOIN
DataPegawai g ON f.IdUser = g.IdPegawai LEFT JOIN
Instalasi i ON b.KdInstalasi = i.KdInstalasi left join
pasiendaftar j on a.nopendaftaran=j.nopendaftaran left join
kelompokpasien k on j.kdkelompokpasien=k.kdkelompokpasien
WHERE a.NoPendaftaran = :nopendaftaran");
$stmt->execute([ 'nopendaftaran' => $NoPendaftaran ]);
$y = $stmt->fetch(PDO::FETCH_ASSOC);
?>
<?php if ( $_SESSION['TempatLogin_ADM'] == 'klinik' ) : ?>
    <div style="font-weight: bold">KLINIK<br>HILDA ALNAIRA</div>
<?php else : ?>
    <div style="font-weight: bold">KLINIK BIDAN<br>HILDA MARTINA</div>
<?php endif ?>
<div style="font-size: 17px; font-weight: bold;"><?= $y['NoAntrian'] ?></div>
<div><?= $y['TglMasuk'] . ', ' . $y['JamMulai'] . '-' . $y['JamSelesai'] ?></div>
<div><?= $y['NamaPasien'] ?></div>
<div><?= $y['NoPendaftaran'] . ' - ' . $y['NoRM'] ?></div>
<div><?= $y['NamaRuangan'] ?></div>
<div><?= $y['jenispasien'] ?></div>
<div><img alt="BARCODE" src="<?php echo 'tong_sys/barcode.php?text=' . $y['NoPendaftaran']; ?>" style="height: 17px;"/></div>