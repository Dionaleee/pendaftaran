<?php
include('../../tong_sys/sqlsrv.php');
//Cetak SEP
$NoPendaftaran = $_GET['nopendaftaran'];
$stmt = $dbConnection->prepare("SELECT a.NoPendaftaran, d.NoRM, CONVERT(VARCHAR(20), a.TglMasuk, 20) AS TglMasuk, b.NamaRuangan, c.NamaLengkap AS NamaDokter, a.JamMulai, a.JamSelesai, a.NoAntrian, dbo.S_HitungUmur(d.TglLahir, a.TglMasuk) AS Umur, g.NamaLengkap AS NamaPetugas, d.NamaLengkap AS NamaPasien, CONVERT(VARCHAR(20), d.TglLahir, 20) AS TglLahir, d.JenisKelamin AS JK, i.NamaInstalasi
FROM
RegistrasiRJJam a LEFT JOIN
Ruangan b ON a.KdRuangan = b.KdRuangan LEFT JOIN
DataPegawai c ON a.IdDokter = c.IdPegawai LEFT JOIN
Pasien d ON a.NoCM = d.NoCM LEFT JOIN
PasienMasukRumahSakit f ON a.NoPendaftaran = f.NoPendaftaran AND a.NoCM = f.NoCM LEFT JOIN
DataPegawai g ON f.IdUser = g.IdPegawai LEFT JOIN
Instalasi i ON b.KdInstalasi = i.KdInstalasi
WHERE a.NoPendaftaran = :nopendaftaran");
$stmt->execute([ 'nopendaftaran' => $NoPendaftaran ]);
$y = $stmt->fetch(PDO::FETCH_ASSOC);

$tgl_registrasi = date("d/m/Y H:i:s",strtotime($y['TglMasuk']));
$TglLahir 	    = date("d/m/Y",strtotime($y['TglLahir']));
// $TglDirujuk 	= date("d/m/Y",strtotime($y['TglDirujuk']));
// $TglSJP     	= date("d F Y",strtotime($y['TglSJP']));
$TglPeriksa 	= date("d/m/Y",strtotime($y['TglMasuk']));
?>
<table style="height: auto;" width="100%;">
	<tbody>
		<tr>
			<td colspan="5"><strong><?php echo $y['NamaInstalasi']; ?></strong></td>
		</tr>
		<tr>
			<td colspan="5"><strong>Klinik RSUD Ciracas Martina, S. Tr.Keb</strong></td>
		</tr>
		<tr>
			<td colspan="3"><span>Dokter Pemeriksa : <?php echo $y['NamaDokter']; ?></span><br />
			<span>Perkiraan Pelayanan : <?php echo $TglPeriksa.", $y[JamMulai] S/D $y[JamSelesai]"; ?></span></td>
			<td colspan="2" style="text-align: center;">
				<h1 style="font-size: 50; margin: 0;"><strong><?php echo $y['NoAntrian']; ?></strong></h1>
			</td>
		</tr>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="5">
			<h2 style="font-size: 25; margin: 0;"><strong><?php echo $y['NamaRuangan']; ?></strong></h2>
			</td>
		</tr>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td>No Registrasi</td>
			<td>:</td>
			<td><?php echo $y['NoPendaftaran']; ?></td>
			<td colspan="2" rowspan="2" align="center"><img alt="BARCODE" src="<?php echo 'tong_sys/barcode.php?text=' . $y['NoPendaftaran']; ?>" style="height: 30px;"/></td>
		</tr>
		<tr>
			<td>NoCM</td>
			<td>:</td>
			<td><?php echo $y['NoRM']; ?></td>
		</tr>
		<tr>
			<td>Tgl. Registrasi</td>
			<td>:</td>
			<td><?php echo $tgl_registrasi; ?></td>
			<td colspan="2" rowspan="2" align="center"><h3 style="font-size: 20;"><?php echo $y['NoPendaftaran']; ?></h3></td>
		</tr>
		<tr>
			<td>Umur</td>
			<td>:</td>
			<td><?php echo $y['Umur']; ?></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5">1) Poliklinik,......... &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; dr............................ , diagnosa......................... tindakan,................... ttd,..........</td>
		</tr>
		
		<tr>
			<td colspan="5">2) Poliklinik,......... &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; dr............................ , diagnosa......................... tindakan,................... ttd,..........</td>
		</tr>

		<tr>
			<td colspan="5">3) Poliklinik,......... &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; dr............................ , diagnosa......................... tindakan,................... ttd,..........</td>
		</tr>

		<tr>
		<td colspan="5">
		<hr />
		</td>
		</tr>

		<!-- <tr>
			<td>No Rujukan</td>
			<td>:</td>
			<td><?php //echo $y['NoRujukan']; ?></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr> -->
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td><?php //echo $TglDirujuk; ?></td>
			<td colspan="2" style="text-align:right;">Petugas :&nbsp;<?php echo $y['NamaPetugas']; ?></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<!-- <tr>
			<td><img width="220" src="assets/img/logo-bpjs.png"></td>
			<td colspan="4" align="center"><STRONG>SURAT ELEGIBITAS PESERTA<BR />Klinik RSUD Ciracas Martina</STRONG></td>
		</tr>
		
		
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>No.SEP</td>
			<td>:</td>
			<td><?php //echo $y['NoSJP']; ?></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Tgl.SEP</td>
			<td>:</td>
			<td><?php //echo $TglSJP; ?></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>No.Kartu</td>
			<td>:</td>
			<td><?php //echo $y['NoKartuPeserta']; ?></td>
			<td colspan="2" rowspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Peserta : <?php //echo $y['UnitBagian']; ?></td>
		</tr>
		<tr>
			<td>Nama Peserta</td>
			<td>:</td>
			<td><?php //echo $y['NamaPasien']; ?></td>
		</tr>
		<tr>
			<td>Tgl.Lahir</td>
			<td>:</td>
			<td><?php //echo $TglLahir; ?></td>
			<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COB :</td>
		</tr>
		<tr>
			<td>Jenis Kelamin</td>
			<td>:</td>
			<td><?php //echo $y['JK'] == 'L' ? 'Laki-Laki' : 'Perempuan'; ?></td>
			<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jenis Rawat : <?php //echo $y['NamaInstalasi']; ?></td>
		</tr>
		<tr>
			<td>Poli / Ruangan Tujuan</td>
			<td>:</td>
			<td><?php //echo $y['NamaRuangan']; ?></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Asal Faskes TK.I</td>
			<td>:</td>
			<td><?php //echo $y['DetailAsalRujukan']; ?></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Diagnosa Awal</td>
			<td>:</td>
			<td colspan="3"><?php //echo $y['DiagnosaRujukan']; ?></td>
		</tr>
		<tr>
			<td>Catatan</td>
			<td>:</td>
			<td>-</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>Pasien/&nbsp;</td>
			<td>Petugas</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>Keluarga Pasien</td>
			<td>BPJS Kesehatan</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>________________</td>
			<td>________________</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5"><i>* Saya Menyetujui BPJS Kesehatan menggunakan infomasi Medis Pasien jika diperlukan</i></td>
		</tr>
		<tr>
			<td colspan="5"><i>* SEP bukan sebagai bukti penjamin peserta</i></td>
		</tr> -->
	</tbody>
</table>

<?php //} ?>