<?php
$NoBooking = $_POST['NoBooking'];
$stmt = $dbConnection->prepare("SELECT TOP 1 a.NoBooking, a.NoCM, a.KdRuangan, d.NamaRuangan AS RuanganPerawatan, a.KdSubInstalasi, b.NamaLengkap AS NamaPasien, c.NamaLengkap AS DokterPemeriksa, CONVERT(VARCHAR(20), a.TglPerjanjian, 20) AS TglPerjanjian FROM Booking_Online a INNER JOIN Pasien b ON a.NoCM = b.NoCM INNER JOIN DataPegawai c ON a.IdDokter = c.IdPegawai INNER JOIN DaftarPoli d ON a.KdRuangan = d.KdRuangan WHERE a.NoBooking = :nobooking");
$stmt->execute([ 'nobooking' => $NoBooking ]);
$data_pasien = $stmt->fetch(PDO::FETCH_ASSOC);
?>
<div class="modal-dialog modal-lg" role="document" id="modal-lg">
  <div class="modal-content">
    <div class="modal-body">
    <form action="modul/daftar_pasien/process.php" method="POST">
      <input type="hidden" name="act" value="proses_batal_pasien_perjanjian">
      <input type="hidden" name="KdSubInstalasi" value="<?= $data_pasien['KdSubInstalasi'] ?>">
      <input type="hidden" name="KdRuangan" value="<?= $data_pasien['KdRuangan'] ?>">
      <div class="form-group row">
        <label for="NamaPasien" class="col-sm-4 col-form-label">Nama Pasien</label>
        <div class="col-sm-8">
          <input type="text" readonly class="form-control" id="NamaPasien" name="NamaPasien" value="<?= $data_pasien['NamaPasien'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="NoBooking" class="col-sm-4 col-form-label">NoPerjanjian</label>
        <div class="col-sm-8">
          <input type="text" readonly class="form-control" id="NoBooking" name="NoBooking" value="<?= $data_pasien['NoBooking'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="NoCM" class="col-sm-4 col-form-label">NoCM</label>
        <div class="col-sm-8">
          <input type="text" readonly class="form-control" id="NoCM" name="NoCM" value="<?= $data_pasien['NoCM'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="RuanganPerawatan" class="col-sm-4 col-form-label">Nama Ruangan</label>
        <div class="col-sm-8">
          <input type="text" readonly class="form-control" id="RuanganPerawatan" name="RuanganPerawatan" value="<?= $data_pasien['RuanganPerawatan'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="TglPerjanjian" class="col-sm-4 col-form-label">Tgl Perjanjian</label>
        <div class="col-sm-8">
          <input type="text" readonly class="form-control" id="TglPerjanjian" name="TglPerjanjian" value="<?= $data_pasien['TglPerjanjian'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="Keterangan" class="col-sm-4 col-form-label">Keterangan</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" id="Keterangan" name="Keterangan">
        </div>
      </div>
      <button class="btn btn-danger">Batal</button>
    </form>
    </div>
  </div>
</div>
