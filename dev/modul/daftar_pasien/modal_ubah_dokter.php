<?php
$NoPendaftaran = $_POST['NoPendaftaran'];
$stmt = $dbConnection->prepare("SELECT TOP 1 a.NoPendaftaran, a.NoCM, RuanganPerawatan, NamaPasien, DokterPemeriksa, CONVERT(VARCHAR(20), TglMasuk, 20) AS TglMasuk, a.KdSubInstalasi, a.KdRuangan, IdDokter, b.IdAsuransi AS NoKartu, b.NoSJP AS NoSEP FROM V_DaftarInfoPasienAll a LEFT JOIN PemakaianAsuransi b ON a.NoPendaftaran = b.NoPendaftaran AND a.NoCM = b.NoCM WHERE a.NoPendaftaran = :nopendaftaran");
$stmt->execute([ 'nopendaftaran' => $NoPendaftaran ]);
$data_pasien = $stmt->fetch(PDO::FETCH_ASSOC);
$KdRuangan = $data_pasien['KdRuangan'];
//$stmt1 = $dbConnection->prepare("SELECT DISTINCT a.IdDokter, b.NamaLengkap AS NamaDokter FROM MasterJadwalPraktekDokter a INNER JOIN DataPegawai b ON a.IdDokter = b.IdPegawai WHERE a.KdRuangan = :ruangan ORDER BY b.NamaLengkap");
$stmt1 = $dbConnection->prepare("SELECT IdPegawai AS IdDokter, NamaLengkap AS NamaDokter FROM DataPegawai WHERE KdJenisPegawai in ('001','002','006', '011') ORDER BY NamaLengkap");
$stmt1->execute();
$data_dokter = $stmt1->fetchAll(PDO::FETCH_ASSOC);
?>
<div class="modal-dialog modal-lg" role="document" id="modal-lg">
  <div class="modal-content">
    <div class="modal-body">
    <form action="modul/daftar_pasien/process.php" method="POST">
      <input type="hidden" name="act" value="proses_ubah_dokter">
      <div class="form-group row">
        <label for="NamaPasien" class="col-sm-4 col-form-label">Nama Pasien</label>
        <div class="col-sm-8">
          <input type="text" readonly class="form-control" id="NamaPasien" name="NamaPasien" value="<?= $data_pasien['NamaPasien'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="NoPendaftaran" class="col-sm-4 col-form-label">NoPendaftaran</label>
        <div class="col-sm-8">
          <input type="text" readonly class="form-control" id="NoPendaftaran" name="NoPendaftaran" value="<?= $data_pasien['NoPendaftaran'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="NoCM" class="col-sm-4 col-form-label">NoCM</label>
        <div class="col-sm-8">
          <input type="text" readonly class="form-control" id="NoCM" name="NoCM" value="<?= $data_pasien['NoCM'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="RuanganPerawatan" class="col-sm-4 col-form-label">Nama Ruangan</label>
        <div class="col-sm-8">
          <input type="text" readonly class="form-control" id="RuanganPerawatan" name="RuanganPerawatan" value="<?= $data_pasien['RuanganPerawatan'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="TglMasuk" class="col-sm-4 col-form-label">Tgl Pendaftaran</label>
        <div class="col-sm-8">
          <input type="text" readonly class="form-control" id="TglMasuk" name="TglMasuk" value="<?= $data_pasien['TglMasuk'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="Ruangan" class="col-sm-4 col-form-label">Ruangan</label>
        <div class="col-sm-8">
          <input type="text" readonly class="form-control" id="Ruangan" name="Ruangan" value="<?= $data_pasien['RuanganPerawatan'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="NamaDokter" class="col-sm-4 col-form-label">Nama Dokter</label>
        <div class="col-sm-8">
          <input type="text" readonly class="form-control" id="NamaDokter" name="NamaDokter" value="<?= $data_pasien['DokterPemeriksa'] ?>">
          <input type="hidden" name="IdDokterLama" value="<?= $data_pasien['IdDokter'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="Keterangan" class="col-sm-4 col-form-label">Keterangan</label>
        <div class="col-sm-8">
          <select class="form-control" name="IdDokterBaru" id="IdDokterBaru" required>
            <option value="">--PILIH DOKTER--</option>
            <?php foreach ( $data_dokter as $row ) : ?>
              <option value="<?= $row['IdDokter'] ?>"><?= $row['NamaDokter'] ?></option>
            <?php endforeach ?>
          </select>
        </div>
      </div>
      <button class="btn btn-success">Ubah</button>
    </form>
    </div>
  </div>
</div>
