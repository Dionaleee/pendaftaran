<?php
  $data_pasien = $_SESSION['data_pasien'];
?>
<div class="card shadow-sm" style="background-color: #ffffffd4;">
  <div class="card-body">
    <table class="table table-bordered text-center shadow-sm">
      <thead class="thead-dark">
        <tr>
          <th>Nama Pasien</th>
          <th>No Rekam Medis</th>
          <th>No Kartu BPJS / KIS</th>
          <th>Umur</th>
        </tr>
      </thead>
      <tbody class="bg-white font-weight-bold">
        <tr>
          <td><?= $data_pasien['NamaLengkap'] ?></td>
          <td><?= $data_pasien['NoCM'] ?></td>
          <td><?= $data_pasien['IdAsuransi'] ?></td>
          <td><?= $data_pasien['UmurPasien'] ?></td>
        </tr>
      </tbody>
    </table>
    <div class="row">
      <div class="col-4 text-center">
        <div class="card shadow-sm pointer" style="height: 200px;" onclick="window.location.href='page.php?modul=daftar_rajal'">
          <div class="card-body font-weight-bold d-flex justify-content-center align-items-center text-pointer" style="font-size: 1.3em;">
            PENDAFTARAN RAWAT JALAN
          </div>
        </div>
      </div>
      <!-- <div class="col-4 text-center">
        <div class="card shadow-sm pointer" style="height: 200px;" onclick="window.location.href='page.php?modul=daftar_ranap'">
          <div class="card-body font-weight-bold d-flex justify-content-center align-items-center text-pointer" style="font-size: 1.3em;">
            PENDAFTARAN RAWAT INAP
          </div>
        </div>
      </div> -->
      <div class="col-4 text-center">
        <?php $NoCM = $data_pasien['NoCM'] ?>
        <div class="card shadow-sm pointer" style="height: 200px;" onclick="window.location.href='page.php?modul=redirect_ke_pendaftaran_helpdesk'">
          <div class="card-body font-weight-bold d-flex justify-content-center align-items-center text-pointer" style="font-size: 1.3em;">
            HELPDESK
          </div>
        </div>
      </div>
    </div>
  </div>
</div>