<?php
include('../../tong_sys/sqlsrv.php');
if ( $_GET['act'] == 'edit_user' ) {
  include('modal_edit_user.php');
} elseif ( $_GET['act'] == 'tambah_user' ) {
  include('modal_tambah_user.php');
} elseif ( $_GET['act'] == 'hapus_user' ) {
  echo $_GET['PIN'];
  $stmt = $dbConnection->prepare("DELETE FROM LoginAplikasi WHERE PIN = :pin");
  $stmt->execute([ 'pin' => $_GET['PIN'] ]);

  echo "<script>alert('User berhasil dihapus!'); window.location.href='../../page.php?modul=user'</script>";
} elseif ( $_GET['act'] == 'simpan_tambah_user' ) {
  $stmt = $dbConnection->prepare("INSERT INTO LoginAplikasi (PIN, NamaPegawai, Pegawai, IdPegawai, Username, UserPassword, KdRuangan, KdRuanganMandiri, Gambar, PermissionsAllowed, UserSession) VALUES (:pin, :nama, null, :idpegawai, :pinjuga, :pass, :ruangan, null, 'icon.png', :permisi, null)");
  $stmt->execute([ 'pin' => $_POST['PIN'], 'nama' => $_POST['NamaPegawai'], 'idpegawai' => $_POST['IdPegawai'], 'pinjuga' => $_POST['PIN'], 'pass' => $_POST['Password'], 'ruangan' => $_POST['Ruangan'], 'permisi' => $_POST['Permissions'] ]);
  
  echo "<script>alert('User berhasil ditambahkan!'); window.location.href='../../page.php?modul=user'</script>";
} elseif ( $_GET['act'] == 'simpan_edit_user' ) {
  session_start();
  $stmt = $dbConnection->prepare("UPDATE LoginAplikasi SET PIN = :pinBaru , NamaPegawai = :nama , IdPegawai = :pegawai , UserPassword = :pass , KdRuangan = :ruangan , PermissionsAllowed = :permission WHERE PIN = :pinLama");
  $stmt->execute([ 'pinBaru' => $_POST['PIN_baru'], 'nama' => $_POST['NamaPegawai'], 'pegawai' => $_POST['IdPegawai'], 'pass' => $_POST['UserPassword'], 'ruangan' => $_POST['Ruangan'], 'permission' => $_POST['Permissions'], 'pinLama' => $_POST['PIN_lama'] ]);
  
  if ( $_SESSION['PIN_ADM'] == $_POST['PIN_lama'] ) {
    unset($_SESSION['PIN_ADM'], $_SESSION['IdPegawai_ADM'], $_SESSION['NamaPegawai_ADM'], $_SESSION['Username_ADM'], $_SESSION['UserPassword_ADM']);
    /* update session */
    $_SESSION['PIN_ADM'] = $_POST['PIN_baru'];
    $_SESSION['IdPegawai_ADM'] = $_POST['IdPegawai'];
    $_SESSION['NamaPegawai_ADM'] = $_POST['NamaPegawai'];
    $_SESSION['Username_ADM'] = $_POST['PIN_baru'];
    $_SESSION['UserPassword_ADM'] = $_POST['UserPassword'];
  }

  echo "<script>alert('Berhasil mengupdate data user!'); window.location.href='../../page.php?modul=user'</script>";
} elseif ( $_GET['act'] == 'cek_pin' ) {
  $stmt = $dbConnection->prepare("SELECT * FROM LoginAplikasi WHERE PIN = '" . $_POST['keyword'] . "'");
  $stmt->execute();
  $user = $stmt->fetch(PDO::FETCH_ASSOC);
  
  if ( $user ) {
    echo json_encode(['code' => 1, 'data' => $user]);
  } else {
    echo json_encode(['code' => 0]);
  }
}