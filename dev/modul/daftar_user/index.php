<?php
  // include('../../tong_sys/sqlsrv.php');
  $modul = "daftar_user";
  $query = "SELECT PIN, NamaPegawai, Pegawai, IdPegawai, Username, UserPassword, a.KdRuangan, b.NamaRuangan, KdRuanganMandiri, Gambar, PermissionsAllowed, UserSession
  FROM LoginAplikasi a LEFT JOIN Ruangan b ON a.KdRuangan = b.KdRuangan";
  $stmt = $dbConnection->prepare($query);
  $stmt->execute();
  $data_user = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>
<div class="card my-3">
  <div class="card-body">
    <?php //echo $query ?>
    <div class="d-sm-flex align-items-center justify-content-between mb-2">
      <h1 class="h3 mb-0 text-gray-800">Daftar User</h1>
      <span class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" onclick="tambah_user()"><i class="fas fa-user-plus fa-sm text-white-50"></i> Tambah User</span>
    </div>
    <div class="table-responsive">
      <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
        <thead class="thead-dark">
          <tr>
            <th>PIN</th>
            <th>NamaPegawai</th>
            <th>IdPegawai</th>
            <th>UserPassword</th>
            <th>Ruangan</th>
            <th>Permissions</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody style="white-space: nowrap;">
          <?php foreach ( $data_user as $row ) : ?>
            <tr>
              <td><?= $row['PIN'] ?></td>
              <td><?= $row['NamaPegawai'] ?></td>
              <td><?= $row['IdPegawai'] ?></td>
              <td><?= $row['UserPassword'] ?></td>
              <td><?= $row['NamaRuangan'] ?></td>
              <td><?= $row['PermissionsAllowed'] == '1' ? 'SuperAdmin' : 'Admin' ?></td>
              <td><span class="badge badge-warning mr-2" style="cursor: pointer;" onclick="edit_user('<?= $row['PIN'] ?>')">Edit</span><a href="modul/daftar_user/process.php?act=hapus_user&PIN=<?= $row['PIN'] ?>" class="badge badge-danger" style="cursor: pointer;" onclick="return confirm('Anda yakin untuk menghapus user ini?')">Delete</a></td>
            </tr>
          <?php endforeach ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
