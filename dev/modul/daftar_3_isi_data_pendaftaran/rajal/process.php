<?php
include('../../../tong_sys/sqlsrv.php');
$hari = date('D');
$kdruangan = $_POST['kdruangan'];

$stmt = $dbConnection->prepare('SELECT DISTINCT a.IdDokter, b.NamaLengkap AS NamaDokter, a.KdRuangan, c.NamaRuangan, c.KdSubInstalasi
FROM MasterJadwalPraktekDokter a INNER JOIN DataPegawai b ON a.IdDokter = b.IdPegawai
INNER JOIN DaftarPoli c ON a.KdRuangan = c.KdRuangan
WHERE a.KdRuangan = :kdruangan AND Hari = :hari ORDER BY NamaLengkap DESC');
$stmt->execute([ 'kdruangan' => $kdruangan, 'hari' => $hari ]);
$daftar_dokter = $stmt->fetchAll(PDO::FETCH_ASSOC);

if ( isset($_POST['cek_kuota_dokter']) ) {
  echo $daftar_dokter ? json_encode(array('kuota' => 'ada dokternya')) : json_encode(array('kuota' => 0));
} elseif ( isset($_POST['cek_kelengkapan_param_daftar_dokter']) ) {
  if ( $daftar_dokter ) {
    echo json_encode(array('message' => 'Dokter Ada', 'KdRuangan' => $daftar_dokter[0]['KdRuangan'], 'NamaRuangan' => $daftar_dokter[0]['NamaRuangan'], 'KdSubInstalasi' => $daftar_dokter[0]['KdSubInstalasi'] ));
  } else {
    $data_ruangan = $dbConnection->query("SELECT * FROM DaftarPoli WHERE KdRuangan = '$kdruangan'")->fetch(PDO::FETCH_ASSOC);
    echo json_encode(['message' => 'Dokter Belum Diisi', 'KdRuangan' => $data_ruangan['KdRuangan'],'NamaRuangan' => $data_ruangan['NamaRuangan'], 'KdSubInstalasi' => $data_ruangan['KdSubInstalasi'] ]);
  }
} else {
  include('dokter2.php');
}