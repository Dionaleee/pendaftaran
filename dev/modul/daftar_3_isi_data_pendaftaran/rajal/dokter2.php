<div class="modal-dialog modal-xl" role="document" id="modal-xl">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" style="width: 100%; text-align: center; font-size: 30px; font-weight: 600;">Dokter <?= $_POST['namaruangan'] ?></h5>
      <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
    </div>
    <div class="modal-body" style="text-align: center;">
      <?php
        $hari = date('D'); $tgl = date('Y-m-d');
      ?>
      <?php if ( count($daftar_dokter) > 3 ): ?>
        <script>
          document.getElementById('modal-xl').style.maxWidth = '95vw';
        </script>
      <?php endif ?>
      <?php foreach ($daftar_dokter as $row): ?>
        <?php $IdDokter = $row['IdDokter']; $NamaDokter = $row['NamaDokter'] ?>
        <?php if ( strlen($row['NamaDokter']) > 32 ): ?>
          <script>
            document.getElementById('<?= $IdDokter ?>').style.padding = '.75rem 0.25rem';
            document.getElementById('<?= $IdDokter ?>').style.fontSize = '14px';
          </script>
        <?php endif ?>
        <div class="card mx-1 shadow" style="display: inline-flex; text-align: center; width: 19rem;" onclick="simpan_daftar('<?= $IdDokter ?>','08.00','09.00')">
        <?php $fontSize = strlen($row['NamaDokter']) > 28 ? (strlen($row['NamaDokter']) > 30 ? '14px' : '16px') : '18px'; ?>
          <div class="card-header" id="<?= $IdDokter ?>" style="font-size: <?= $fontSize ?>; font-weight: 600;">
            <?= $row['NamaDokter'] ?>
          </div>
          <div class="card-body" style="padding: 0.25rem;">
            <?php //$FotoDokter = $this->db2->query("SELECT * FROM staf_medis WHERE id_pegawai = '$IdDokter'")->result_array(); $Dokter = (count($FotoDokter) == 0 ? '' : $FotoDokter[0]['gambar']); ?>
            <img src="<?php echo 'assets/img/dokter.png' ?>" class="card-img-top" alt="Dokter <?= $row['NamaDokter'] ?>" style="height: 18rem; width: 18rem;" >
          </div>
        </div>
      <?php endforeach ?>
    </div>
  </div>
</div>
