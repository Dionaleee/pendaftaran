<div class="card shadow-sm mb-3" style="background-color: #ffffffd4;">
  <div class="card-body">
  <h6 class="card-title font-weight-bold">Kamar Dan Bed Rawat Inap</h6>
    <div class="row">
    <?php foreach ( $kamar_bed as $row ) : ?>
      <?php $KdKamar = $row['KdKamar']; $NoBed = $row['NoBed']; ?>
      <div class="col-3 text-center">
        <div class="card shadow-sm pointer" style="height: 100px;">
          <div class="card-body font-weight-bold d-flex justify-content-center align-items-center text-pointer-ranap" style="font-size: 1.2em;">
            <?= $row['NoKamar'] . ' - ' . $row['NoBed'] ?>
          </div>
        </div>
      </div>
    <?php endforeach ?>
    </div>
  </div>
</div>