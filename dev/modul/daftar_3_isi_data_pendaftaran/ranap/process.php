<?php
include('../../../tong_sys/sqlsrv.php');
if ( $_GET['act'] == 'daftar_kelas_kamar_per_ruangan_ranap' ) {
  $stmt = $dbConnection->prepare("SELECT DISTINCT KdKelas, Kelas, KdKamar, NoKamar, NoBed FROM V_KamarRegRawatInap WHERE (KdRuangan = :ruangan) AND KdKelas = :kelas AND StatusEnabled = :status");
  $stmt->execute([ 'ruangan' => $_POST['KdRuangan'], 'kelas' => $_POST['KdKelas'], 'status' => '1' ]);
  $kamar_bed = $stmt->fetchAll(PDO::FETCH_ASSOC);
  if ( $kamar_bed ) {
    include('daftar_kelas_kamar_per_ruangan_ranap.php');
  } else {
    $umumbpjs = $_POST['PilihanRanap'];
    echo "<script>alert('Mohon maaf, belum ada bed kosong saat ini!'); window.location.href='page.php?modul=isi_data_ranap&ranap=$umumbpjs';</script>";
  }
}