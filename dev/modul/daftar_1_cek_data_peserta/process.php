<?php
include('../../tong_sys/sqlsrv.php');
$data_perjanjian = $dbConnection->query("SELECT a.* FROM Booking_Online a LEFT JOIN Pasien b ON a.NoCM = b.NoCM LEFT JOIN (SELECT TOP 1 * FROM AsuransiPasien WHERE NoCM = '$_POST[nopasien]' OR IdAsuransi = '$_POST[nopasien]' ORDER BY TglBerlaku DESC) c ON a.NoCM = c.NoCM
WHERE (a.NoCM = '$_POST[nopasien]' OR b.NoIdentitas = '$_POST[nopasien]' OR c.IdAsuransi = '$_POST[nopasien]') AND CONVERT(VARCHAR(10), a.TglPerjanjian, 20) = CONVERT(VARCHAR(10), GETDATE(), 20) ORDER BY a.TglPerjanjian DESC")->fetch(PDO::FETCH_ASSOC);

if ( $data_perjanjian ) {
  session_start();
  $stmt = $dbConnection->prepare('SELECT TOP 1 a.*, b.IdAsuransi, dbo.S_HitungUmur(a.TglLahir, getdate()) AS UmurPasien FROM Pasien a LEFT JOIN (SELECT TOP 1 * FROM AsuransiPasien WHERE NoCM = ? OR IdAsuransi = ? ORDER BY TglBerlaku DESC) b ON a.NoCM = b.NoCM WHERE a.NoCM = ? OR a.NoIdentitas = ? OR b.IdAsuransi = ? ORDER BY a.TglDaftarMembership DESC');
  for ($i=1; $i <= 6; $i++) { 
    $stmt->bindParam($i, $_POST['nopasien']);
  }
  $stmt->execute();
  $row = $stmt->fetch(PDO::FETCH_ASSOC);

  if ( $row ) {
    $_SESSION['data_pasien'] = $row;
    $_SESSION['data_perjanjian'] = $data_perjanjian;
    header("Location: ../../page.php?modul=pilih_pendaftaran");
  } else {
    echo "<script>alert('data pasien tidak ditemukan. silahkan mendaftar sebagai pasien baru.'); window.location.href='../../page.php?modul=daftar_pasien_baru'</script>";
  }
} else {
  $stmt = $dbConnection->prepare("SELECT TOP 1 a.* FROM PasienMasukRumahSakit a
  LEFT JOIN Pasien b ON a.NoCM = b.NoCM
  LEFT JOIN (SELECT TOP 1 * FROM AsuransiPasien WHERE NoCM = ? OR IdAsuransi = ? ORDER BY TglBerlaku DESC) c ON a.NoCM = c.NoCM
  WHERE (a.NoCM = ? OR b.NoIdentitas = ? OR c.IdAsuransi = ?) AND a.StatusPeriksa <> 'B' AND CONVERT(VARCHAR(10), a.TglMasuk, 20) = CONVERT(VARCHAR(10), GETDATE(), 20) ORDER BY a.TglMasuk DESC");
  for ($i=1; $i <= 5; $i++) { 
    $stmt->bindParam($i, $_POST['nopasien']);
  }
  $stmt->execute();
  $cek_daftar = $stmt->fetch(PDO::FETCH_ASSOC);

  if ( $cek_daftar ) {
    echo "<script>alert('Pasien Sudah Didaftarkan Dengan NoPendaftaran " . $cek_daftar['NoPendaftaran'] . ". Mohon periksa kembali.'); window.location.href='../../page.php?modul=pendaftaran'</script>";
  } else {
    $stmt = $dbConnection->prepare('SELECT TOP 1 a.*, b.IdAsuransi, dbo.S_HitungUmur(a.TglLahir, getdate()) AS UmurPasien FROM Pasien a LEFT JOIN (SELECT TOP 1 * FROM AsuransiPasien WHERE NoCM = ? OR IdAsuransi = ? ORDER BY TglBerlaku DESC) b ON a.NoCM = b.NoCM WHERE a.NoCM = ? OR a.NoIdentitas = ? OR b.IdAsuransi = ? ORDER BY a.TglDaftarMembership DESC');
    for ($i=1; $i <= 6; $i++) { 
      $stmt->bindParam($i, $_POST['nopasien']);
    }
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
  
    if ( $row ) {
      session_start();
      $_SESSION['data_pasien'] = $row;
      header("Location: ../../page.php?modul=pilih_pendaftaran");
    } else {
      echo "<script>alert('data pasien tidak ditemukan. silahkan mendaftar sebagai pasien baru.'); window.location.href='../../page.php?modul=daftar_pasien_baru'</script>";
    }
  }
}
