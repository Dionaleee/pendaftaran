<?php
    
?>
<div class="card">
    <div class="card-body">
        <h4>Pasien Rujukan Rawat Jalan</h4>
        <form action="" method="POST">
            <div class="row">
                <label for="TglAwal" class="col-md-1">Periode</label>
                <div class="col-md-5">
                    <input type="date" class="form-control" name="TglAwal" id="TglAwal" value="<?= date('Y-m-d') ?>">
                </div>
                <label for="TglAkhir" class="col-md-1 text-center">s/d</label>
                <div class="col-md-5">
                    <input type="date" class="form-control" name="TglAkhir" id="TglAkhir" value="<?= date('Y-m-d') ?>">
                </div>
            </div>
            <button type="submit">Cari</button>
        </form>
    </div>
</div>
<div class="card mt-3">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                <thead class="thead-dark">
                    <tr>
                        <th>No Pendaftaran</th>
                        <th>No RM</th>
                        <th>Nama Pasien</th>
                        <th>Tgl Masuk</th>
                        <th>Perujuk</th>
                        <th>Ruangan</th>
                        <th>Petugas Periksa</th>
                        <th>Status Pasien</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>