<?php
  $tahun = date('Y');
  if ( isset($_POST['tahun']) ) {
    $tahun = $_POST['tahun'];
  }

  $query = "select b.RuanganPelayanan,
  b.JenisPasien,
 sum(b.Januari) as Januari, 
 sum(b.Februari) as Februari,
 sum(b.maret) as Maret,
 sum(b.April) as April,
 sum(b.Mei) as Mei,
 sum(b.Juni) as Juni,
 sum(b.Juli) as Juli,
 sum(b.Agustus) as Agustus,
 sum(b.September) as September,
 sum(b.Oktober) as Oktober,
 sum(b.November) as November,
 sum(b.Desember) as Desember
 from(
   select 
     b.RuanganPelayanan, 
     b.JenisPasien,
     b.jmlpasien,
     case	when b.Bulan = 1 then b.jmlpasien
       else 0
     end as Januari,
     case	when b.Bulan = 2 then b.jmlpasien
       else 0
     end as Februari,
     case	when b.Bulan = 3 then b.jmlpasien
       else 0
     end as Maret,
     case	when b.Bulan = 4 then b.jmlpasien
       else 0
     end as April,
     case	when b.Bulan = 5 then b.jmlpasien
       else 0
     end as Mei,
     case	when b.Bulan = 6 then b.jmlpasien
       else 0
     end as Juni,
     case	when b.Bulan = 7 then b.jmlpasien
       else 0
     end as Juli,
     case	when b.Bulan = 8 then b.jmlpasien
       else 0
     end as Agustus,
     case	when b.Bulan = 9 then b.jmlpasien
       else 0
     end as September,
     case	when b.Bulan = 10 then b.jmlpasien
       else 0
     end as Oktober,
     case	when b.Bulan = 11 then b.jmlpasien
       else 0
     end as November,
     case	when b.Bulan = 12 then b.jmlpasien
       else 0
     end as Desember,
     '' as A
   from(
       Select RuanganPelayanan,JenisPasien,MONTH(TglPendaftaran) as bulan, sum (jmlpasien) as jmlpasien
       from V_DataKunjunganPasienMasukyusep 
       WHERE YEAR(TglPendaftaran)='$tahun'
       and KdInstalasi ='02' and judul='KUNJUNGAN' 
       --and JenisPasien in ('BPJS PBI','BPJS NON PBI')
       --and JenisPasien in ('UMUM')
       group by ruanganpelayanan,JenisPasien,MONTH(TglPendaftaran),detail
     )b
   )b
 group by b.RuanganPelayanan,b.JenisPasien
 order by b.RuanganPelayanan";
  $stmt = $dbConnection->prepare($query);
  $stmt->execute();
  $data_laporan = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>
<div class="card my-3">
  <div class="card-body">
    <?php //echo $query ?>
    <div class="d-sm-flex align-items-center justify-content-between mb-2">
      <h1 class="h3 mb-0 text-gray-800">Laporan Kunjungan Bulanan</h1>
      <span class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm" onclick="kunjungan_bulanan()"><i class="fas fa-file-excel fa-sm text-white-50"></i> Save As Excel</span>
    </div>
    <form action="page.php?modul=laporan&sub_modul=kunjungan_bulanan" method="POST">
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="tahun">Tahun</label>
          <select class="form-control" name="tahun" id="tahun">
            <?php $year = '2020'; for ($i=0; $i < 5; $i++) : ?>
              <option value="<?= $year ?>" <?= $year == $tahun ? 'selected' : '' ?>><?= $year ?></option>
            <?php $year++; endfor ?>
          </select>
        </div>
        <div class="form-group col-md-6">
          <label class="d-none d-md-block">&nbsp;</label>
          <button class="btn btn-primary w-100" type="submit">Cari</button>
        </div>
      </div>
    </form>
  </div>
</div>
<div class="card mb-3">
  <div class="card-body">
    <table class="table table-bordered table-sm">
      <tr>
        <td>RuanganPelayanan</td>
        <td>JenisPasien</td>
        <td>Januari</td>
        <td>Februari</td>
        <td>Maret</td>
        <td>April</td>
        <td>Mei</td>
        <td>Juni</td>
        <td>Juli</td>
        <td>Agustus</td>
        <td>September</td>
        <td>Oktober</td>
        <td>November</td>
        <td>Desember</td>
      </tr>
      <?php
      $totJan = $totFeb = $totMar = $totApr = $totMei = $totJun = $totJul = $totAgs = $totSep = $totOkt = $totNov = $totDes = 0;
      foreach ( $data_laporan as $row ) : ?>
      <tr>
        <td><?= $row['RuanganPelayanan'] ?></td>
        <td><?= $row['JenisPasien'] ?></td>
        <td class="text-center"><?= $row['Januari'] ?></td>
        <td class="text-center"><?= $row['Februari'] ?></td>
        <td class="text-center"><?= $row['Maret'] ?></td>
        <td class="text-center"><?= $row['April'] ?></td>
        <td class="text-center"><?= $row['Mei'] ?></td>
        <td class="text-center"><?= $row['Juni'] ?></td>
        <td class="text-center"><?= $row['Juli'] ?></td>
        <td class="text-center"><?= $row['Agustus'] ?></td>
        <td class="text-center"><?= $row['September'] ?></td>
        <td class="text-center"><?= $row['Oktober'] ?></td>
        <td class="text-center"><?= $row['November'] ?></td>
        <td class="text-center"><?= $row['Desember'] ?></td>
      </tr>
      <?php
      $totJan = $totJan + $row['Januari'];
      $totFeb = $totFeb + $row['Februari'];
      $totMar = $totMar + $row['Maret'];
      $totApr = $totApr + $row['April'];
      $totMei = $totMei + $row['Mei'];
      $totJun = $totJun + $row['Juni'];
      $totJul = $totJul + $row['Juli'];
      $totAgs = $totAgs + $row['Agustus'];
      $totSep = $totSep + $row['September'];
      $totOkt = $totOkt + $row['Oktober'];
      $totNov = $totNov + $row['November'];
      $totDes = $totDes + $row['Desember'];
      endforeach ?>
      <tr>
        <td colspan="2" class="font-weight-bold">Total</td>
        <td class="font-weight-bold text-center"><?= $totJan ?></td>
        <td class="font-weight-bold text-center"><?= $totFeb ?></td>
        <td class="font-weight-bold text-center"><?= $totMar ?></td>
        <td class="font-weight-bold text-center"><?= $totApr ?></td>
        <td class="font-weight-bold text-center"><?= $totMei ?></td>
        <td class="font-weight-bold text-center"><?= $totJun ?></td>
        <td class="font-weight-bold text-center"><?= $totJul ?></td>
        <td class="font-weight-bold text-center"><?= $totAgs ?></td>
        <td class="font-weight-bold text-center"><?= $totSep ?></td>
        <td class="font-weight-bold text-center"><?= $totOkt ?></td>
        <td class="font-weight-bold text-center"><?= $totNov ?></td>
        <td class="font-weight-bold text-center"><?= $totDes ?></td>
      </tr>
    </table>
  </div>
</div>