<?php
include('../../tong_sys/function_sp.php');
if ( @$_POST['act'] == 'daftar_pasien_baru' ) {
  session_start();
  $TglDaftarMembership = gmdate("Y/m/d H:i:s", time()+60*60*7);

  if ( $_POST['NoCM'] == '' ) {
    $NoCM = null;
  } else {
    $NoCM = $_POST['NoCM'];
  }
  
  SP_AU_Pasien([$NoCM, $_POST['NoIdentitas'], $TglDaftarMembership, $_POST['NamaDepan'], $_POST['NamaLengkap'], $_POST['TempatLahir'], $_POST['TglLahir'], $_POST['JenisKelamin'], $_POST['Alamat'], $_POST['Telepon'], $_POST['Propinsi'], $_POST['Kota'], $_POST['Kecamatan'], $_POST['Kelurahan'], $_POST['RTRW'], $_POST['KodePos'], $NoCM, null, null, '', $_SESSION['IdPegawai_ADM']]);
  
  $stmt = $dbConnection->prepare("SELECT * FROM Pasien WHERE TglDaftarMembership = :tgl");
  $stmt->execute([ 'tgl' => $TglDaftarMembership ]);
  $pasien = $stmt->fetch(PDO::FETCH_ASSOC);
  
  if ( $pasien ) {
    unset($_SESSION['data_pasien']);
    $_SESSION['data_pasien'] = $pasien;
    echo "<script>alert('berhasil mendaftarkan pasien baru'); window.location.href='../../page.php?modul=redirect_ke_pendaftaran_helpdesk'</script>";
  } else {
    echo "<script>alert('gagal mendaftarkan pasien baru'); window.location.href='../../page.php?modul=pendaftaran'</script>";
  }
} elseif ( $_GET['act'] == 'cek_data_pasien_by_nik' ) {
  $data_pasien = $dbConnection->query("SELECT * FROM Pasien WHERE NoIdentitas = '$_POST[NIK]' ORDER BY TglDaftarMembership DESC")->fetch(PDO::FETCH_ASSOC);
  
  if ( $data_pasien ) {
    echo json_encode(['code' => '200', 'message' => "Pasien telah terdaftar dengan nama " . $data_pasien['NamaLengkap'] . " dan No Rekam Medis " . trim($data_pasien['NoCM']) . ". Jika data yang dimaksud adalah sama, silahkan mendaftar dengan data tersebut."]);
  } else {
    echo json_encode(['code' => '404', 'message' => 'Data tidak ditemukan, silahkan lanjutkan pendaftaran']);
  }
}