<form action="modul/pendaftaran_helpdesk/process.php" method="POST">
<?php
  include('modul/data_pasien/index.php');
?>
<div class="card mb-3 shadow-sm">
  <div class="card-body">
    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          <label for="Instalasi">Instalasi</label>
          <select class="form-control" name="Instalasi" id="Instalasi" onchange="tampil_pendaftaran_helpdesk(this.value, document.getElementById('Penjamin').value, '<?= $_GET['NoCM'] ?>')">
            <option value="">--PILIH INSTALASI--</option>
            <option value="02">Instalasi Rawat Jalan</option>
            <option value="03">Instalasi Rawat Inap</option>
            <option value="09">Instalasi Laboratorium</option>
          </select>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label for="Penjamin">Penjamin</label>
          <select class="form-control" name="Penjamin" id="Penjamin" onchange="tampil_pendaftaran_helpdesk(document.getElementById('Instalasi').value, this.value, '<?= $_GET['NoCM'] ?>')">
            <option value="">--PILIH PENJAMIN--</option>
            <?php
              $kelompokpasien = $dbConnection->query("SELECT * FROM KelompokPasien WHERE StatusEnabled = '1'")->fetchAll(PDO::FETCH_ASSOC);
              foreach ($kelompokpasien as $row) :
            ?>
            <option value="<?= $row['KdKelompokPasien'] ?>"><?= $row['JenisPasien'] ?></option>
            <?php endforeach; ?>
            <!-- <option value="20">BPJS</option>
            <option value="01">UMUM</option>
            <option value="07">Karyawan Klinik</option> -->
          </select>
        </div>
      </div>
      <div class="col-md-3">
        <label class="d-none d-md-block" for="Penjamin">&nbsp;</label>
        <span class="btn btn-warning" onclick="modal_penanggungjawab_pasien(document.getElementById('NoCM').value)">Isi Data Penanggung Jawab</span>
      </div>
    </div>
  </div>
</div>
<?php
  // var_dump($_SESSION);
?>
<div id="sub_pendaftaran_helpdesk"></div>
</form>