<?php
include('../../tong_sys/sqlsrv.php');
ini_set('date.timezone', 'Asia/Jakarta');
if ( $_POST['KdKelompokPasien'] == '20' ) {
  $pilihranap = 'BPJS';
  $stmt = $dbConnection->prepare("SELECT TOP 1 * FROM AsuransiPasien WHERE NoCM = :nocm AND KdInstitusiAsal = :kode ORDER BY TglBerlaku DESC");
  $stmt->execute([ 'nocm' => $_POST['NoCM'], 'kode' => '0024' ]);
  $cek_noka = $stmt->fetch(PDO::FETCH_ASSOC);  
} else {
  $pilihranap = 'UMUM';
  $cek_noka = '';
}

session_start();
/* ambil buat data penanggung jawab */
$data_pasien = $_SESSION['data_pasien'];

// $stmt = $dbConnection->prepare("SELECT DISTINCT KdKelas, Kelas, KdRuangan, NamaRuangan FROM V_KelasPelayanan WHERE KdInstalasi = :instalasi and Expr2 = :status");
$stmt = $dbConnection->prepare("SELECT DISTINCT a.KdRuangan, a.NamaRuangan, b.KdKelas, c.DeskKelas AS Kelas FROM Ruangan a
INNER JOIN KelasRuangan b ON a.KdRuangan = b.KdRuangan
INNER JOIN KelasPelayanan c ON b.KdKelas = c.KdKelas
WHERE a.KdInstalasi = :instalasi AND a.StatusEnabled = :status");
$stmt->execute([ 'instalasi' => '03', 'status' => '1' ]);
$kelas_ranap = $stmt->fetchAll(PDO::FETCH_ASSOC);

$stmt = $dbConnection->prepare("SELECT DISTINCT KdRujukanAsal, RujukanAsal FROM RujukanAsal WHERE StatusEnabled = :status");
$stmt->execute([ 'status' => '1' ]);
$asal_rujukan = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>
<input type="hidden" name="act" value="simpan_data_ranap">
<input type="hidden" name="KdKelompokPasien" value="<?= $_POST['KdKelompokPasien'] ?>">
<div class="card mb-3 shadow-sm">
  <div class="card-body my-0 py-3">
    <h6 class="card-title font-weight-bold">Data Registrasi Rawat Inap</h6>
    <div class="form-row">
      <?php if ( !$cek_noka && $pilihranap == 'BPJS' ) : ?>
        <div class="form-group col-md-4">
          <label for="NoKartu">NoKartu BPJS Pasien</label>
          <input class="form-control" type="text" name="NoKartu" id="NoKartu" onchange="buat_noka(this.value)" required>
        </div>
      <?php endif ?>
      <div class="form-group col-md-4">
        <label for="TglDaftarRanap">Tanggal Dan Jam Masuk Rawat Inap</label>
        <input type="datetime-local" class="form-control" name="TglDaftarRanap" id="TglDaftarRanap" value="<?= date('Y-m-d\TH:i:s') ?>">
      </div>
      <div class="form-group col-md-4">
        <label for="kelas_ruangan_ranap">Kelas Dan Ruangan Rawat Inap</label>
        <select name="kelas_ruangan_ranap" id="kelas_ruangan_ranap" class="form-control" onchange="daftar_subinstalasi_per_ruangan(this.value, '<?= $pilihranap ?>'); daftar_kelas_kamar_per_ruangan(this.value);">
          <option value="">Pilih Kelas Rawat...</option>
          <?php foreach ( $kelas_ranap as $row ) : ?>
            <option value="<?= $row['KdKelas'] . '|' . $row['KdRuangan'] ?>"><?= $row['Kelas'] . ' - ' . $row['NamaRuangan'] ?></option>
          <?php endforeach ?>
        </select>
      </div>
      <div class="form-group col-md-2">
        <label for="smf">SMF</label>
        <select name="smf" id="smf" class="form-control" readonly>
          <option>Pilih SMF...</option>
        </select>
      </div>
      <input type="hidden" name="asalrujukan" value="01">
      <!-- <div class="form-group col-md-2">
        <label for="asalrujukan">Asal Rujukan</label>
        <select name="asalrujukan" id="asalrujukan" class="form-control">
          <option>Pilih Asal Rujukan...</option>
          <?php //foreach ( $asal_rujukan as $row ) : ?>
            <option value="<?php //echo $row['KdRujukanAsal'] ?>" <?php //echo $row['KdRujukanAsal'] == '01' ? 'selected' : '' ?>><?php //echo $row['RujukanAsal'] ?></option>
          <?php //endforeach ?>
        </select>
      </div> -->
      <input type="hidden" name="cara_masuk" value="03">
      <!-- <div class="form-group col-md-3">
        <label for="cara_masuk">Cara Masuk</label>
        <select name="cara_masuk" id="cara_masuk" class="form-control">
          <option>Pilih Cara Masuk...</option>
          <?php //foreach ( $cara_masuk as $row ) : ?>
            <option value="<?php //echo $row['KdCaraMasuk'] ?>" <?php //echo $row['KdCaraMasuk'] == '01' ? 'selected' : '' ?>><?php //echo $row['CaraMasuk'] ?></option>
          <?php //endforeach ?>
        </select>
      </div> -->
      <div class="form-group col-md-3">
        <label for="kamar_bed_ranap">Kamar Dan Bed Rawat Inap</label>
        <select name="kamar_bed_ranap" id="kamar_bed_ranap" class="form-control" readonly>
          <option>Pilih Kelas Kamar...</option>
        </select>
      </div>
      <div class="form-group col-md-3">
        <label for="kamar_bed_ranap">Diagnosa</label>
        <input class="form-control" type="text" name="Diagnosa" id="Diagnosa">
        <div id="suggesstion-box-diagnosa" style="position: absolute; z-index: 9999;"></div>
      </div>
    </div>
    <button type="submit" class="btn btn-primary w-100">Daftar</button>
  </div>
</div>
<?php
  // var_dump($_SESSION['data_pasien']);
?>
<input type="hidden" name="NamaLengkapPenanggung" id="NamaLengkapPenanggung" value="<?= $data_pasien['NamaLengkap'] ?>" class="form-control">
<!-- 17 : lain-lain -->
<input type="hidden" name="HubunganPenanggung" id="HubunganPenanggung" value="17" class="form-control">
<!-- 11 : lain-lain -->
<input type="hidden" name="PekerjaanPenanggung" id="PekerjaanPenanggung" value="11" class="form-control">
<input type="hidden" name="NoIdentitasPenanggung" id="NoIdentitasPenanggung" value="<?= $data_pasien['NoIdentitas'] ?>" class="form-control">
<input type="hidden" name="AlamatPenanggung" id="AlamatPenanggung" value="<?= $data_pasien['Alamat'] ?>">
<input type="hidden" name="RTRWPenanggung" id="RTRWPenanggung" value="<?= $data_pasien['RTRW'] ?>">
<input type="hidden" name="KelurahanPenanggung" id="KelurahanPenanggung" value="<?= $data_pasien['Kelurahan'] ?>">
<input type="hidden" name="KecamatanPenanggung" id="KecamatanPenanggung" value="<?= $data_pasien['Kecamatan'] ?>">
<input type="hidden" name="KotaPenanggung" id="KotaPenanggung" value="<?= $data_pasien['Kota'] ?>">
<input type="hidden" name="ProvinsiPenanggung" id="ProvinsiPenanggung" value="<?= $data_pasien['Propinsi'] ?>">
<input type="hidden" name="KodePosPenanggung" id="KodePosPenanggung" value="<?= $data_pasien['KodePos'] ?>">
<input type="hidden" name="TeleponPenanggung" id="TeleponPenanggung" value="<?= $data_pasien['Telepon'] ?>">

<!-- <div class="card mb-3 shadow-sm">
  <div class="card-body my-0 py-3">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h6 class="card-title font-weight-bold">Data Penanggungjawab Pasien</h6>
      <div class="d-none d-sm-inline-block form-check">
        <input class="form-check-input" type="checkbox" id="dewekan">
        <label class="form-check-label text-danger font-weight-bold" for="dewekan">
          Diri Sendiri
        </label>
      </div>
    </div>
    <div class="form-row">
      <div class="form-group col-md-2">
        <label for="NamaLengkapPenanggung">Nama Lengkap</label>
        <input type="text" name="NamaLengkapPenanggung" id="NamaLengkapPenanggung" value="" class="form-control">
      </div>
      <div class="form-group col-md-2">
        <label for="HubunganPenanggung">Hubungan</label>
        <select name="HubunganPenanggung" id="HubunganPenanggung" class="form-control">
          <option value="">Pilih Hubungan...</option>
          <?php //foreach ( $hubungan_penanggung as $row ) : ?>
            <option value="<?php //echo $row['Hubungan'] ?>"><?php //echo $row['NamaHubungan'] ?></option>
          <?php //endforeach ?>
        </select>
      </div>
      <div class="form-group col-md-2">
        <label for="PekerjaanPenanggung">Pekerjaan</label>
        <select name="PekerjaanPenanggung" id="PekerjaanPenanggung" class="form-control">
          <option value="">Pilih Pekerjaan...</option>
          <?php //foreach ( $pekerjaan_penanggung as $row ) : ?>
            <option value="<?php //echo $row['KdPekerjaan'] ?>"><?php //echo $row['Pekerjaan'] ?></option>
          <?php //endforeach ?>
        </select>
      </div>
      <div class="form-group col-md-2">
        <label for="NoIdentitasPenanggung">No. Identitas</label>
        <input type="text" name="NoIdentitasPenanggung" id="NoIdentitasPenanggung" value="" class="form-control">
      </div>
      <div class="form-group col-md-2">
        <label for="AlamatPenanggung">Alamat Lengkap</label>
        <input type="text" name="AlamatPenanggung" id="AlamatPenanggung" value="" class="form-control">
      </div>
      <div class="form-group col-md-2">
        <label for="RTRWPenanggung">RT/RW</label>
        <input type="text" class="form-control" name="RTRWPenanggung" id="RTRWPenanggung" value="">
      </div>
      <div class="form-group col-md-2">
        <label for="KelurahanPenanggung">Kelurahan</label>
        <input type="text" name="KelurahanPenanggung" id="KelurahanPenanggung" value="" class="form-control">
        <div id="suggesstion-box-kelurahan-penanggung"></div>
      </div>
      <div class="form-group col-md-2">
        <label for="KecamatanPenanggung">Kecamatan</label>
        <input type="text" name="KecamatanPenanggung" id="KecamatanPenanggung" value="" class="form-control">
      </div>
      <div class="form-group col-md-2">
        <label for="KotaPenanggung">Kota/Kabupaten</label>
        <input type="text" name="KotaPenanggung" id="KotaPenanggung" value="" class="form-control">
      </div>
      <div class="form-group col-md-2">
        <label for="ProvinsiPenanggung">Provinsi</label>
        <input type="text" name="ProvinsiPenanggung" id="ProvinsiPenanggung" value="" class="form-control">
      </div>
      <div class="form-group col-md-2">
        <label for="KodePosPenanggung">Kode Pos</label>
        <input type="text" name="KodePosPenanggung" id="KodePosPenanggung" value="" class="form-control">
      </div>
      <div class="form-group col-md-2">
        <label for="TeleponPenanggung">Telepon</label>
        <input type="text" name="TeleponPenanggung" id="TeleponPenanggung" value="" class="form-control">
      </div>
      <div class="form-group col-md-2">
        <button class="btn btn-primary" type="submit">Simpan</button>
      </div>
      <div class="form-group col-md-1">
        <label>&nbsp;</label>
        <a href="<?php //echo base_url('dashboard') ?>" class="form-control btn btn-danger">Batal</a>
      </div>
    </div>
  </div>
</div> -->
<?php
  //var_dump($data_pasien)
?>