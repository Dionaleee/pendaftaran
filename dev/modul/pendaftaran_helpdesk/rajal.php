<?php
session_start();
include('../../tong_sys/sqlsrv.php');
if ( $_POST['KdKelompokPasien'] == '20' ) {
  $pilihrajal = 'BPJS';
  $stmt = $dbConnection->prepare("SELECT TOP 1 * FROM AsuransiPasien WHERE NoCM = :nocm AND KdInstitusiAsal = :kode ORDER BY TglBerlaku DESC");
  $stmt->execute([ 'nocm' => $_POST['NoCM'], 'kode' => '0024' ]);
  $cek_noka = $stmt->fetch(PDO::FETCH_ASSOC);  
} else {
  $pilihrajal = 'UMUM';
  $cek_noka = '';
}

// if ( $_SESSION['TempatLogin_ADM'] == 'klinik' ) {
  /* poli yang ada didepan cuma poli anak dan obgyn */
  /* tambah poli umum dan imunisasi 2020-12-30 011,009 */
  // $stmt = $dbConnection->prepare("SELECT * FROM DaftarPoli WHERE KdRuangan IN ('007','009','011','016') AND StatusEnabled = :status");
//;} else {
  $stmt = $dbConnection->prepare('SELECT * FROM DaftarPoli a INNER JOIN Ruangan b ON a.KdRuangan = b.KdRuangan WHERE a.StatusEnabled = :status AND b.NamaExternal = :TempatLogin');
// }
$stmt->execute([ 'status' => '1', 'TempatLogin' => $_SESSION['TempatLogin_ADM'] ]);
$daftar_poli = $stmt->fetchAll(PDO::FETCH_ASSOC);

/* data penanggungjawab */
$data_pj = $dbConnection->query("SELECT TOP 1 * FROM PenanggungJawabPasien WHERE NoCM = '' ORDER BY NoPendaftaran DESC")->fetch(PDO::FETCH_ASSOC);
?>
<div class="card mb-3 shadow-sm">
  <div class="card-body">
    <div class="d-sm-flex align-items-center justify-content-between">
      <h6 class="card-title font-weight-bold">Data Registrasi Rawat Jalan</h6>
      <div class="form-check">
        <input class="form-check-input" type="checkbox" id="perjanjian">
        <label class="form-check-label text-danger font-weight-bold font-italic" for="perjanjian">
          Daftar Perjanjian
        </label>
      </div>
    </div>
    <div class="row">
      <?php if ( !$cek_noka && $pilihrajal == 'BPJS' ) : ?>
        <div class="col-md-3">
          <div class="form-group">
            <label for="NoKartu">NoKartu BPJS Pasien</label>
            <input class="form-control" type="text" name="NoKartu" id="NoKartu" onchange="buat_noka(this.value)" required>
          </div>
        </div>
      <?php endif ?>
      <div class="col-md-3" style="display: none;" id="tgl_perjanjian">
        <div class="form-group">
          <label for="TglPerjanjian">Tgl Perjanjian</label>
          <input class="form-control" type="date" id="TglPerjanjian" name="TglPerjanjian" value="<?= date('Y-m-d', strtotime('+1 day')) ?>">
          <small id="TglPerjanjianHelper" class="form-text text-danger font-weight-bold">
            Pilih Tanggal Pelayanan Perjanjian.
          </small>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label for="Ruangan">Poliklinik</label>
          <select class="form-control" name="Ruangan" id="Ruangan" onchange="daftar_dokter_rajal_helpdesk(this.value, <?= $_POST['KdKelompokPasien'] ?>)">
            <option value="">--PILIH POLIKLINIK--</option>
            <?php foreach ( $daftar_poli as $row ) : ?>
              <option value="<?= $row['KdRuangan'] ?>"><?= $row['NamaRuangan'] ?></option>
            <?php endforeach ?>
          </select>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label for="Perujuk_Rajal">Perujuk</label>
          <input type="text" class="form-control" name="Perujuk_Rajal" id="Perujuk_Rajal">
        </div>
      </div>
    </div>
  </div>
</div>