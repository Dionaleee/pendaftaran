<?php
session_start();
include('../../../tong_sys/sqlsrv.php');
include('../../../tong_sys/function_sp.php');
$data_pasien = $_SESSION['data_pasien'];
$TglDaftarMembership	= gmdate("Y/m/d H:i:s", time()+60*60*7);
$NoCM = $data_pasien['NoCM'];
$IdUser = $_SESSION['IdPegawai_ADM'];
$KdRuangan = $_POST['KdRuangan'];
$KdSubInstalasi = $_POST['KdSubInstalasi'];
$IdDokter = $_POST['IdDokter'];
$JamMulai = $_POST['JamMulai'];
$JamSelesai = $_POST['JamSelesai'];
$KdKelasPMRS = '06';

if ( $_GET['act'] == 'simpan_bpjs' ) {
  $noka = $data_pasien['IdAsuransi']; // $rujukan['noKartu'];
  $nmJenisPeserta = ''; // $rujukan['nmJenisPeserta'];
  $kdKelas = '3'; // $rujukan['kdKelas'];
  $PKUkdProvider = ''; // $rujukan['PKUkdProvider'];
  $PKUnmProvider = ''; // rtrim($rujukan['PKUnmProvider']);
  $noKunjungan = ''; // $rujukan['noKunjungan'];
  $kdDiag = ''; // $rujukan['kdDiag'];
  $nmDiag = ''; // $rujukan['nmDiag'];

  $NoSEP = $_POST['NoSEP'];
  $NoBP = 'a24'; /* yang melalui ADM selalu a24 */
  
  $cariPBI = '"'.$nmJenisPeserta.'"';
  $adaPBI	= 'PBI';
  $ketemu = strpos($cariPBI,$adaPBI);
  if ($ketemu === 1) {
    $KdKelompokPasien		= '19';
  }else{
    $KdKelompokPasien		= '20';
  }
  if($kdKelas == '1'){
    $KdKelasDiTanggung		= '01';
  }elseif($kdKelas == '2'){
    $KdKelasDiTanggung		= '02';
  }else{
    $KdKelasDiTanggung		= '03';
  }

  SP_AU_Pasien([$NoCM, $data_pasien['NoIdentitas'], $TglDaftarMembership, $data_pasien['Title'], $data_pasien['NamaLengkap'], $data_pasien['TempatLahir'], $data_pasien['TglLahir'], $data_pasien['JenisKelamin'], $data_pasien['Alamat'], $data_pasien['Telepon'], $data_pasien['Propinsi'], $data_pasien['Kota'], $data_pasien['Kecamatan'], $data_pasien['Kelurahan'], $data_pasien['RTRW'], $data_pasien['KodePos'], $NoCM, null, null, '346', $IdUser]);
  
  $KdRujukanAsal = '02';

  if ( $KdRuangan == '016' || $KdRuangan == '026' || $KdRuangan == '201' ) {
    SP_Add_RegistrasiPasienMRSPilihDokterRJADM_USG_Anak([$NoCM, $KdSubInstalasi, $KdRuangan, $TglDaftarMembership, $TglDaftarMembership, $KdKelasPMRS, $KdKelompokPasien, $IdUser, null, null, null, null, '01', null, $IdDokter, $JamMulai, $JamSelesai, $KdRujukanAsal]);
  } else {
    SP_Add_RegistrasiPasienMRSPilihDokterRJADM([$NoCM, $KdSubInstalasi, $KdRuangan, $TglDaftarMembership, $TglDaftarMembership, $KdKelasPMRS, $KdKelompokPasien, $IdUser, null, null, null, null, '01', null, $IdDokter, $JamMulai, $JamSelesai, $KdRujukanAsal]);
  }

  $TglPendaftaran = date('Y-m-d');
  $stmt = $dbConnection->prepare("SELECT TOP 1 NoPendaftaran FROM RegistrasiRJJam WHERE NoCM = :nocm AND CONVERT(VARCHAR(10), TglMasuk, 20) = :tgldaftar AND IdDokter = :iddokter AND KdRuangan = :kdruangan AND JamMulai = :jammulai ORDER BY TglMasuk DESC");
  $stmt->execute([ 'nocm' => $NoCM, 'tgldaftar' => $TglPendaftaran, 'iddokter' => $IdDokter, 'kdruangan' => $KdRuangan, 'jammulai' => $JamMulai ]);
  $data_pendaftaran = $stmt->fetch(PDO::FETCH_ASSOC);

  $NoPendaftaran = $data_pendaftaran['NoPendaftaran'];

  if ( $NoPendaftaran == '' ) {
    echo json_encode([
      'NoPendaftaran' => '',
      'message' => 'Tidak dapat mendaftar di ADM. Silahkan mendaftar melalui Helpdesk.'
    ]);
    exit();
  }
  
  /* simpan penanggungjawab 23 juni 2021 */
  // if ( isset($_SESSION['NamaPJ']) ) {
  //   $dbConnection->exec("INSERT INTO PenanggungJawabPasien (NoPendaftaran, NoCM, NamaPJ, AlamatPJ, TeleponPJ, Hubungan, Propinsi, Kota, Kecamatan, Kelurahan, RTRW, KodePos, Pekerjaan, NoIdentitas) VALUES ('$NoPendaftaran', '$NoCM', '$_SESSION[NamaPJ]', '$_SESSION[AlamatPJ]', '$_SESSION[TeleponPJ]', '$_SESSION[HubunganPJ]', '$_SESSION[PropinsiPJ]', '$_SESSION[KotaPJ]', '$_SESSION[KecamatanPJ]', '$_SESSION[KelurahanPJ]', '$_SESSION[RTRWPJ]', '$_SESSION[KodePosPJ]', '$_SESSION[PekerjaanPJ]', '$_SESSION[NoIdentitasPJ]')");
  // } else {
    $data_pj = $dbConnection->query("SELECT TOP 1 * FROM PenanggungJawabPasien WHERE NoCM = '$NoCM' ORDER BY NoPendaftaran DESC")->fetch(PDO::FETCH_ASSOC);
    $dbConnection->exec("INSERT INTO PenanggungJawabPasien (NoPendaftaran, NoCM, NamaPJ, AlamatPJ, TeleponPJ, Hubungan, Propinsi, Kota, Kecamatan, Kelurahan, RTRW, KodePos, Pekerjaan, NoIdentitas) VALUES ('$NoPendaftaran', '$NoCM', '$data_pj[NamaPJ]', '$data_pj[AlamatPJ]', '$data_pj[TeleponPJ]', '$data_pj[Hubungan]', '$data_pj[Propinsi]', '$data_pj[Kota]', '$data_pj[Kecamatan]', '$data_pj[Kelurahan]', '$data_pj[RTRW]', '$data_pj[KodePos]', '$data_pj[Pekerjaan]', '$data_pj[NoIdentitas]')");
  // }

  SP_AU_AsuransiPasienJoinProgramAskes(['0000000022', $noka, $NoCM, $data_pasien['NamaLengkap'], $data_pasien['NoIdentitas'], NULL, $data_pasien['TglLahir'], $data_pasien['Alamat'], $NoPendaftaran, '01', $NoSEP, $TglDaftarMembership, $IdUser, $NoBP, 1, NULL, 'O', 0, $nmJenisPeserta, NULL, $noKunjungan, $KdRujukanAsal, $PKUnmProvider, $PKUkdProvider, '-', $TglDaftarMembership, $nmDiag, $kdDiag, '0024', $KdKelasDiTanggung, $TglDaftarMembership]);

  $stmt = $dbConnection->prepare("INSERT INTO PasienBelumBayar(NoPendaftaran, NoCM) VALUES ('$NoPendaftaran', '$NoCM')");
  $stmt->execute();

  // $TempatLogin = $_SESSION['TempatLogin_ADM'];
  // $dbConnection->exec("INSERT INTO PasienSumberDaftar (NoPendaftaran, NoCM, KdInstalasi, TglPendaftaran, KdRuangan, IdUser, IdDokter, TempatLogin) VALUES ('$NoPendaftaran', '$NoCM', '02', '$TglDaftarMembership', '$KdRuangan', '$IdUser', '$IdDokter', '$TempatLogin')");
  
  if ( $_SESSION['TempatLogin_ADM'] == 'klinik' ) {
    SP_Add_BiayaPelayananOtomatisNew([$NoPendaftaran, $KdSubInstalasi, $KdRuangan, $TglDaftarMembership, NULL, $KdKelasPMRS, NULL, $IdDokter, 'AL']);
  }

  // SP_Add_BiayaPelayananOtomatisNew([$NoPendaftaran, $KdSubInstalasi, $KdRuangan, $TglDaftarMembership, NULL, $KdKelasPMRS, NULL, $IdDokter, 'AL']);
  
  echo json_encode([ 'NoPendaftaran' => $NoPendaftaran ]);
} elseif ( $_GET['act'] == 'simpan_perjanjian' ) {
  // var_dump($_POST); die();
  $TglPerjanjian = $_POST['TglPerjanjian'];

  if ( $KdRuangan == '016' || $KdRuangan == '026' || $KdRuangan == '201' ) {
    SP_Add_Pasien_Perjanjian_USG_Anak([NULL, $_POST['TglPerjanjian'], $TglDaftarMembership, $NoCM, $_POST['KdKelompokPasien'], $KdRuangan, $KdSubInstalasi, $IdDokter, $JamMulai, $JamSelesai, NULL, $IdUser]);
  } else {
    SP_Add_Pasien_Perjanjian([NULL, $_POST['TglPerjanjian'], $TglDaftarMembership, $NoCM, $_POST['KdKelompokPasien'], $KdRuangan, $KdSubInstalasi, $IdDokter, $JamMulai, $JamSelesai, NULL, $IdUser]);
  }

  $data_booking = $dbConnection->query("SELECT * FROM Booking_Online WHERE  NoCM = '$NoCM' AND TglPerjanjian = '$TglPerjanjian' AND KdRuangan = '$KdRuangan'")->fetch(PDO::FETCH_ASSOC);

  echo json_encode($data_booking);
} else {
  SP_AU_Pasien([$NoCM, $data_pasien['NoIdentitas'], $TglDaftarMembership, $data_pasien['Title'], $data_pasien['NamaLengkap'], $data_pasien['TempatLahir'], $data_pasien['TglLahir'], $data_pasien['JenisKelamin'], $data_pasien['Alamat'], $data_pasien['Telepon'], $data_pasien['Propinsi'], $data_pasien['Kota'], $data_pasien['Kecamatan'], $data_pasien['Kelurahan'], $data_pasien['RTRW'], $data_pasien['KodePos'], $NoCM, null, null, '346', $IdUser]);

  $KdRujukanAsal = ($_POST['Perujuk_Rajal'] != '' ? $_POST['Perujuk_Rajal'] : '01');
  $KdKelompokPasien = $_POST['KdKelompokPasien'];

  if ( $KdRuangan == '016' || $KdRuangan == '026' || $KdRuangan == '201' ) {
    SP_Add_RegistrasiPasienMRSPilihDokterRJADM_USG_Anak([$NoCM, $KdSubInstalasi, $KdRuangan, $TglDaftarMembership, $TglDaftarMembership, $KdKelasPMRS, $KdKelompokPasien, $IdUser, null, null, null, null, '01', null, $IdDokter, $JamMulai, $JamSelesai, $KdRujukanAsal]);
  } else {
    SP_Add_RegistrasiPasienMRSPilihDokterRJADM([$NoCM, $KdSubInstalasi, $KdRuangan, $TglDaftarMembership, $TglDaftarMembership, $KdKelasPMRS, $KdKelompokPasien, $IdUser, null, null, null, null, '01', null, $IdDokter, $JamMulai, $JamSelesai, $KdRujukanAsal]);
  }

  $TglPendaftaran = date('Y-m-d');
  $stmt = $dbConnection->prepare("SELECT TOP 1 NoPendaftaran FROM RegistrasiRJJam WHERE NoCM = :nocm AND CONVERT(VARCHAR(10), TglMasuk, 20) = :tgldaftar AND IdDokter = :iddokter AND KdRuangan = :kdruangan AND JamMulai = :jammulai ORDER BY TglMasuk DESC");
  $stmt->execute([ 'nocm' => $NoCM, 'tgldaftar' => $TglPendaftaran, 'iddokter' => $IdDokter, 'kdruangan' => $KdRuangan, 'jammulai' => $JamMulai ]);
  $data_pendaftaran = $stmt->fetch(PDO::FETCH_ASSOC);

  $NoPendaftaran = $data_pendaftaran['NoPendaftaran'];

  /* simpan penanggungjawab 23 juni 2021 */
  // if ( isset($_SESSION['NamaPJ']) ) {
  //   $dbConnection->exec("INSERT INTO PenanggungJawabPasien (NoPendaftaran, NoCM, NamaPJ, AlamatPJ, TeleponPJ, Hubungan, Propinsi, Kota, Kecamatan, Kelurahan, RTRW, KodePos, Pekerjaan, NoIdentitas) VALUES ('$NoPendaftaran', '$NoCM', '$_SESSION[NamaPJ]', '$_SESSION[AlamatPJ]', '$_SESSION[TeleponPJ]', '$_SESSION[HubunganPJ]', '$_SESSION[PropinsiPJ]', '$_SESSION[KotaPJ]', '$_SESSION[KecamatanPJ]', '$_SESSION[KelurahanPJ]', '$_SESSION[RTRWPJ]', '$_SESSION[KodePosPJ]', '$_SESSION[PekerjaanPJ]', '$_SESSION[NoIdentitasPJ]')");
  // } else {
    $data_pj = $dbConnection->query("SELECT TOP 1 * FROM PenanggungJawabPasien WHERE NoCM = '$NoCM' ORDER BY NoPendaftaran DESC")->fetch(PDO::FETCH_ASSOC);
    $dbConnection->exec("INSERT INTO PenanggungJawabPasien (NoPendaftaran, NoCM, NamaPJ, AlamatPJ, TeleponPJ, Hubungan, Propinsi, Kota, Kecamatan, Kelurahan, RTRW, KodePos, Pekerjaan, NoIdentitas) VALUES ('$NoPendaftaran', '$NoCM', '$data_pj[NamaPJ]', '$data_pj[AlamatPJ]', '$data_pj[TeleponPJ]', '$data_pj[Hubungan]', '$data_pj[Propinsi]', '$data_pj[Kota]', '$data_pj[Kecamatan]', '$data_pj[Kelurahan]', '$data_pj[RTRW]', '$data_pj[KodePos]', '$data_pj[Pekerjaan]', '$data_pj[NoIdentitas]')");
  // }

  if ( $NoPendaftaran == '' ) {
    echo json_encode([
      'NoPendaftaran' => '',
      'message' => 'Tidak dapat mendaftar di ADM. Silahkan mendaftar melalui Helpdesk.'
    ]);
    exit();
  }
  
  if ( $KdKelompokPasien == '65' ) {
    SP_AU_AsuransiPasienJoinProgramAskes(['0000000054', '0', $NoCM, $data_pasien['NamaLengkap'], $data_pasien['NoIdentitas'], NULL, $data_pasien['TglLahir'], $data_pasien['Alamat'], $NoPendaftaran, '01', '', $TglDaftarMembership, $IdUser, 'a24', 1, NULL, 'O', 0, '', NULL, '', '01', '', '', '-', $TglDaftarMembership, '', '', '0044', '', $TglDaftarMembership]);
  }

  $stmt = $dbConnection->prepare("INSERT INTO PasienBelumBayar(NoPendaftaran, NoCM) VALUES ('$NoPendaftaran', '$NoCM')");
  $stmt->execute();

  // $TempatLogin = $_SESSION['TempatLogin_ADM'];
  // $dbConnection->exec("INSERT INTO PasienSumberDaftar (NoPendaftaran, NoCM, KdInstalasi, TglPendaftaran, KdRuangan, IdUser, IdDokter, TempatLogin) VALUES ('$NoPendaftaran', '$NoCM', '02', '$TglDaftarMembership', '$KdRuangan', '$IdUser', '$IdDokter', '$TempatLogin')");
  
  // if ( $_SESSION['TempatLogin_ADM'] == 'klinik' ) {
    SP_Add_BiayaPelayananOtomatisNew([$NoPendaftaran, $KdSubInstalasi, $KdRuangan, $TglDaftarMembership, NULL, $KdKelasPMRS, NULL, $IdDokter, 'AL']);
  // }

  // SP_Add_BiayaPelayananOtomatisNew([$NoPendaftaran, $KdSubInstalasi, $KdRuangan, $TglDaftarMembership, NULL, $KdKelasPMRS, NULL, $IdDokter, 'AL']);
  
  echo json_encode([ 
    'NoPendaftaran' => $NoPendaftaran,
    'message' => "Pasien $NoCM dengan NoPendaftaran $NoPendaftaran berhasil didaftarkan."
  ]);
}