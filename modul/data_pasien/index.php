<?php
  $stmt = $dbConnection->prepare("SELECT NamaTitle FROM Title WHERE StatusEnabled = '1'");
  $stmt->execute();
  $title = $stmt->fetchAll(PDO::FETCH_ASSOC);
  
  $stmt = $dbConnection->prepare("SELECT Singkatan, JenisKelamin FROM JenisKelamin WHERE StatusEnabled = '1'");
  $stmt->execute();
  $jenis_kelamin = $stmt->fetchAll(PDO::FETCH_ASSOC);

  $stmt = $dbConnection->prepare("SELECT TOP 1 *, dbo.S_HitungUmur(TglLahir, getdate()) AS UmurPasien FROM Pasien WHERE NoCM = :nocm ORDER BY TglDaftarMembership DESC");
  $stmt->execute([ 'nocm' => $_GET['NoCM'] ]);
  $pasien = $stmt->fetch(PDO::FETCH_ASSOC);
?>
<div class="card mb-3 shadow-sm">
  <div class="card-body my-0 py-3">
    <h6 class="card-title font-weight-bold">Data Pasien</h6>
    <div class="form-row">
      <div class="form-group col-md-2">
        <label for="NoCM">No. RM</label>
        <input type="text" class="form-control" name="NoCM" id="NoCM" value="<?= $pasien['NoCM'] ?>" readonly>
      </div>
      <div class="form-group col-md-2">
        <label for="NamaDepan">Nama Depan</label>
        <select name="NamaDepan" id="NamaDepan" class="form-control">
          <option>Pilih...</option>
          <?php foreach ( $title as $row ) : ?>
            <option value="<?= $row['NamaTitle'] ?>" <?= $row['NamaTitle'] == $pasien['Title'] ? 'selected' : '' ?>><?= $row['NamaTitle'] ?></option>
          <?php endforeach ?>
        </select>
      </div>
      <div class="form-group col-md-3">
        <label for="NamaLengkap">Nama Lengkap</label>
        <input type="text" class="form-control" name="NamaLengkap" id="NamaLengkap" value="<?= $pasien['NamaLengkap'] ?>">
      </div>
      <div class="form-group col-md-3">
        <label for="NoIdentitas">No. Identitas (KTP/SIM/...)</label>
        <input type="text" class="form-control" name="NoIdentitas" id="NoIdentitas" value="<?= $pasien['NoIdentitas'] ?>">
      </div>
      <div class="form-group col-md-2">
        <label for="JenisKelamin">Jenis Kelamin</label>
        <select name="JenisKelamin" id="JenisKelamin" class="form-control">
          <option>Pilih...</option>
          <?php foreach ( $jenis_kelamin as $row ) : ?>
            <option value="<?= $row['Singkatan'] ?>" <?= $row['Singkatan'] == $pasien['JenisKelamin'] ? 'selected' : '' ?>><?= $row['JenisKelamin'] ?></option>
          <?php endforeach ?>
        </select>
      </div>
    </div>
    <div class="form-row">
      <div class="form-group col-md-3">
        <label for="TempatLahir">Tempat Lahir</label>
        <input type="text" class="form-control" name="TempatLahir" id="TempatLahir" value="<?= $pasien['TempatLahir'] ?>">
      </div>
      <div class="form-group col-md-2">
        <label for="TglLahir">Tanggal Lahir</label>
        <input type="date" class="form-control" name="TglLahir" id="TglLahir" value="<?= date('Y-m-d', strtotime($pasien['TglLahir'])) ?>">
      </div>
      <?php
        $dob = new DateTime($pasien['TglLahir']);
        $today = new DateTime;
        $age = $today->diff($dob);
      ?>
      <div class="form-group col-md-1">
        <label for="TahunLahir">Tahun</label>
        <input type="text" class="form-control" readonly value="<?= $age->format("%y") ?>">
      </div>
      <div class="form-group col-md-1">
        <label for="BulanLahir">Bulan</label>
        <input type="text" class="form-control" readonly value="<?= $age->format("%m") ?>">
      </div>
      <div class="form-group col-md-1">
        <label for="HariLahir">Hari</label>
        <input type="text" class="form-control" readonly value="<?= $age->format("%d") ?>">
      </div>
      <div class="form-group col-md-2">
        <label>&nbsp;</label>
        <span class="form-control btn btn-primary" onclick="update_data_pasien()">Update Data Pasien</span>
      </div>
      <div class="form-group col-md-2">
        <label>&nbsp;</label>
        <span class="form-control btn btn-warning" onclick="modal_detail_pasien(document.getElementById('NoCM').value)">Detail Pasien</span>
      </div>
    </div>
  </div>
</div>
<div class="card mb-3 shadow-sm">
  <div class="card-body my-0 py-3">
    <h6 class="card-title font-weight-bold">Alamat Pasien</h6>
    <div class="form-row">
      <div class="form-group col-md-4">
        <label for="AlamatLengkap">Alamat Lengkap</label>
        <input type="text" class="form-control" name="Alamat" id="AlamatLengkap" value="<?= $pasien['Alamat'] ?>">
      </div>
      <div class="form-group col-md-2">
        <label for="RTRW">RT/RW</label>
        <input type="text" class="form-control" name="RTRW" id="RTRW" value="<?= $pasien['RTRW'] == '' || $pasien['RTRW'] == null ? '/' : $pasien['RTRW'] ?>">
      </div>
      <div class="form-group col-md-2">
        <label for="Telepon">Telepon</label>
        <input type="text" class="form-control" name="Telepon" id="Telepon" value="<?= $pasien['Telepon'] ?>">
      </div>
      <div class="form-group col-md-4">
        <label for="kelurahan">Kelurahan</label>
        <input type="text" class="form-control" name="Kelurahan" id="kelurahan" value="<?= $pasien['Kelurahan'] ?>">
        <div id="suggesstion-box" style="position: absolute; z-index: 9999;"></div>
      </div>
      <div class="form-group col-md-3">
        <label for="kecamatan">Kecamatan</label>
        <input type="text" class="form-control" name="Kecamatan" id="kecamatan" value="<?= $pasien['Kecamatan'] ?>">
      </div>
      <div class="form-group col-md-3">
        <label for="kota">Kota (Kabupaten)</label>
        <input type="text" class="form-control" name="Kota" id="kota" value="<?= $pasien['Kota'] ?>">
      </div>
      <div class="form-group col-md-3">
        <label for="provinsi">Provinsi</label>
        <input type="text" class="form-control" name="Propinsi" id="provinsi" value="<?= $pasien['Propinsi'] ?>">
      </div>
      <div class="form-group col-md-3">
        <label for="kodepos">KodePos</label>
        <input type="text" class="form-control" name="KodePos" id="kodepos" value="<?= $pasien['KodePos'] ?>">
      </div>
      <?php if ( $_GET['modul'] == 'data_pasien' ) : ?>
        <div class="form-group col-md-2">
          <a href="page.php?modul=rekam_sesi_pasien_untuk_pendaftaran_helpdesk&NoCM=<?= $pasien['NoCM'] ?>" class="col-12 btn btn-danger">Daftar</a>
        </div>
      <?php endif ?>
    </div>
  </div>
</div>