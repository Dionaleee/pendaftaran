<?php
include('../../tong_sys/function_sp.php');
if ( $_GET['act'] == 'update_data_pasien' ) {
  session_start();
  $TglDaftarMembership = gmdate("Y/m/d H:i:s", time()+60*60*7);
  
  SP_AU_Pasien([$_POST['NoCM'], $_POST['NoIdentitas'], $TglDaftarMembership, $_POST['NamaDepan'], $_POST['NamaLengkap'], $_POST['TempatLahir'], $_POST['TglLahir'], $_POST['JenisKelamin'], $_POST['Alamat'], $_POST['Telepon'], $_POST['Propinsi'], $_POST['Kota'], $_POST['Kecamatan'], $_POST['Kelurahan'], $_POST['RTRW'], $_POST['KodePos'], $_POST['NoCM'], null, null, '', $_SESSION['IdPegawai_ADM']]);
  
  $stmt = $dbConnection->prepare("SELECT * FROM Pasien WHERE NoCM = :nocm AND TglDaftarMembership = :tgl");
  $stmt->execute([ 'nocm' => $_POST['NoCM'], 'tgl' => $TglDaftarMembership ]);
  $pasien = $stmt->fetch(PDO::FETCH_ASSOC);
  
  if ( $pasien ) {
    unset($_SESSION['data_pasien']);
    $_SESSION['data_pasien'] = $pasien;
    $array = ['code' => 200, 'message' => 'Data Pasien Berhasil Diupdate.'];
  } else {
    $array = ['code' => 501, 'message' => 'Data Pasien Tidak Berhasil Diupdate.'];
  }
  echo json_encode($array);
} elseif ( $_GET['act'] == 'proses_detail_pasien' ) {
  if ( $_POST['ada_detail'] == 'ada' ) {
    $dbConnection->exec("UPDATE DetailPasien SET GolDarah = '$_POST[GolDarah]', Pekerjaan = '$_POST[Pekerjaan]', Agama = '$_POST[Agama]', Pendidikan = '$_POST[Pendidikan]', NamaAyah = '$_POST[NamaAyah]', NamaIbu = '$_POST[NamaIbu]' WHERE NoCM = '$_POST[NoCM]'");
  } else {
    $dbConnection->exec("INSERT INTO DetailPasien (NoCM, GolDarah, Pekerjaan, Agama, Pendidikan, NamaAyah, NamaIbu) VALUES ('$_POST[NoCM]', '$_POST[GolDarah]', '$_POST[Pekerjaan]', '$_POST[Agama]', '$_POST[Pendidikan]', '$_POST[NamaAyah]', '$_POST[NamaIbu]')");
  }
  
  echo "<script>alert('Berhasil mengupdate detail pasien'); window.location.href='../../page.php?modul=data_pasien&NoCM=$_POST[NoCM]'</script>";
} elseif ( $_GET['act'] == 'proses_pj_pasien' ) {
  $NoCM = $_POST['NoCM'];
  $NamaPJ = $_POST['NamaPJ'];
  $AlamatPJ = $_POST['AlamatPJ'];
  $TeleponPJ = $_POST['TeleponPJ'];
  $HubunganPJ = $_POST['Hubungan'];
  $PropinsiPJ = $_POST['Propinsi'];
  $KotaPJ = $_POST['Kota'];
  $KecamatanPJ = $_POST['Kecamatan'];
  $KelurahanPJ = $_POST['Kelurahan'];
  $RTRWPJ = $_POST['RTRW'];
  $KodePosPJ = $_POST['KodePos'];
  $PekerjaanPJ = $_POST['Pekerjaan'];
  $NoIdentitasPJ = $_POST['NoIdentitas'];

  $dbConnection->exec("INSERT INTO PenanggungJawabPasien (NoPendaftaran, NoCM, NamaPJ, AlamatPJ, TeleponPJ, Hubungan, Propinsi, Kota, Kecamatan, Kelurahan, RTRW, KodePos, Pekerjaan, NoIdentitas) VALUES ('', '$NoCM', '$NamaPJ', '$AlamatPJ', '$TeleponPJ', '$HubunganPJ', '$PropinsiPJ', '$KotaPJ', '$KecamatanPJ', '$KelurahanPJ', '$RTRWPJ', '$KodePosPJ', '$PekerjaanPJ', '$NoIdentitasPJ')");
  
  echo "<script>window.location.href='../../page.php?modul=pendaftaran_helpdesk&NoCM=$_POST[NoCM]'</script>";
}