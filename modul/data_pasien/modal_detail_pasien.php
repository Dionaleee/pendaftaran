<?php
include('../../tong_sys/sqlsrv.php');
$NoCM = $_POST['NoCM'];
$stmt = $dbConnection->prepare("SELECT     NoCM,  GolDarah,  Pekerjaan, Agama,Pendidikan, NamaAyah, NamaIbu FROM DetailPasien WHERE NoCM = :nocm");
$stmt->execute([ 'nocm' => $NoCM ]);
$detail_pasien = $stmt->fetch(PDO::FETCH_ASSOC);
$ada = 'ada';

if ( $detail_pasien ) {
    $detail_pasien = $detail_pasien;
} else {
    $detail_pasien['NoCM'] = $NoCM;
    $detail_pasien['GolDarah'] = '';
    $detail_pasien['Pekerjaan'] = '';
    $detail_pasien['Agama'] = '';
    $detail_pasien['Pendidikan'] = '';
    $detail_pasien['NamaAyah'] = '';
    $detail_pasien['NamaIbu'] = '';
    $ada = 'tidak ada';
}

$pekerjaan = $dbConnection->query("SELECT Pekerjaan FROM Pekerjaan WHERE StatusEnabled = '1'")->fetchAll(PDO::FETCH_ASSOC);

$agama = $dbConnection->query("SELECT Agama FROM Agama WHERE StatusEnabled = '1'")->fetchAll(PDO::FETCH_ASSOC);

$pendidikan = $dbConnection->query("SELECT Pendidikan FROM Pendidikan WHERE StatusEnabled = '1'")->fetchAll(PDO::FETCH_ASSOC);
?>
<div class="modal-dialog modal-xl" role="document" id="modal-xl">
  <div class="modal-content">
    <div class="modal-body">
    <form action="modul/data_pasien/process.php?act=proses_detail_pasien" method="POST">
      <input type="hidden" name="ada_detail" value="<?= $ada ?>">
      <div class="form-row">
        <div class="form-group col-md-3">
          <label for="NoCM">NoCM</label>
          <input type="text" class="form-control" name="NoCM" id="NoCM" value="<?= $detail_pasien['NoCM'] ?>" readonly>
        </div>
        <div class="form-group col-md-3">
          <label for="GolDarah">Gol. Darah</label>
          <select class="form-control" name="GolDarah" id="GolDarah">
              <option value="">--Gol Darah--</option>
              <option value="A" <?= $detail_pasien['GolDarah'] == 'A' ? 'selected' : '' ?>>A</option>
              <option value="B" <?= $detail_pasien['GolDarah'] == 'B' ? 'selected' : '' ?>>B</option>
              <option value="AB" <?= $detail_pasien['GolDarah'] == 'AB' ? 'selected' : '' ?>>AB</option>
              <option value="O" <?= $detail_pasien['GolDarah'] == 'O' ? 'selected' : '' ?>>O</option>
          </select>
        </div>
        <div class="form-group col-md-3">
          <label for="Pekerjaan">Pekerjaan</label>
          <select class="form-control" name="Pekerjaan" id="Pekerjaan">
              <option value="">--Pekerjaan--</option>
              <?php foreach ( $pekerjaan as $row ) : ?>
                  <option value="<?= $row['Pekerjaan'] ?>" <?= $row['Pekerjaan'] == $detail_pasien['Pekerjaan'] ? 'selected' : '' ?>><?= $row['Pekerjaan'] ?></option>
              <?php endforeach ?>
          </select>
        </div>
        <div class="form-group col-md-3">
          <label for="Agama">Agama</label>
          <select class="form-control" name="Agama" id="Agama">
              <option value="">--Agama--</option>
              <?php foreach ( $agama as $row ) : ?>
                  <option value="<?= $row['Agama'] ?>" <?= $row['Agama'] == $detail_pasien['Agama'] ? 'selected' : '' ?>><?= $row['Agama'] ?></option>
              <?php endforeach ?>
          </select>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="Pendidikan">Pendidikan</label>
          <select class="form-control" name="Pendidikan" id="Pendidikan">
              <option value="">--Pendidikan--</option>
              <?php foreach ( $pendidikan as $row ) : ?>
                  <option value="<?= $row['Pendidikan'] ?>" <?= $row['Pendidikan'] == $detail_pasien['Pendidikan'] ? 'selected' : '' ?>><?= $row['Pendidikan'] ?></option>
              <?php endforeach ?>
          </select>
        </div>
        <div class="form-group col-md-4">
          <label for="NamaAyah">Nama Ayah</label>
          <input type="text" class="form-control" name="NamaAyah" id="NamaAyah" value="<?= $detail_pasien['NamaAyah'] ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="NamaIbu">Nama Ibu</label>
          <input type="text" class="form-control" name="NamaIbu" id="NamaIbu" value="<?= $detail_pasien['NamaIbu'] ?>">
        </div>
      </div>
      <button class="btn btn-primary">Simpan</button>
    </form>
    </div>
  </div>
</div>
