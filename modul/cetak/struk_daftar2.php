<style>
    html, body, div, table {
        margin: 0;
        padding: 0;
        text-align: center;
        font-size: 14px;
    }
    td {
        font-size: 11px;
    }
    .berpadding {
        padding-left: 11px;
        padding-right: 12px;
    }
</style>
<?php
session_start();
include('../../tong_sys/sqlsrv.php');
$NoPendaftaran = $_GET['nopendaftaran'];

if ( $_GET['nocm'] != '' ) {
    $NoCMParam = "AND a.NoCM = '$_GET[nocm]'";
} else {
    $NoCMParam = "";
}


//jadi di sini pengecekan pasien ini ranap atau rajal
//query
            $stmt = $dbConnection->prepare("SELECT * FROM V_DaftarInfoPasienAll
            WHERE NoPendaftaran = :nopendaftaran");
            $stmt->execute([ 'nopendaftaran' => $NoPendaftaran ]);
            $r = $stmt->fetch(PDO::FETCH_ASSOC);

if($r['KdInstalasi'] == '03'){
    //query ranap dan cetakan htmlnya



        $stmt = $dbConnection->query("SELECT a.NoPendaftaran, d.NoCM, CONVERT(VARCHAR(20), a.TglMasuk, 20) AS TglMasuk, b.NamaRuangan, c.NamaLengkap AS NamaDokter, a.JamMulai, a.JamSelesai, a.NoAntrian, dbo.S_HitungUmur(d.TglLahir, a.TglMasuk) AS Umur, g.NamaLengkap AS NamaPetugas, d.NamaLengkap AS NamaPasien, CONVERT(VARCHAR(20), d.TglLahir, 20) AS TglLahir, d.JenisKelamin AS JK, i.NamaInstalasi,k.jenispasien, d.NoIdentitas
                FROM
                (
                    SELECT NoPendaftaran, NoCM, TglMasuk, NoAntrian, JamMulai, JamSelesai, IdDokter, KdRuangan FROM RegistrasiRJJam
                    UNION
                    SELECT NoPendaftaran, NoCM, TglPendaftaran AS TglMasuk, NoAntrian, '0' + cast(DATEPART(hour, TglPendaftaran) as varchar) + ':00' AS JamMulai, '0' + cast((cast(DATEPART(hour, TglPendaftaran) as int)+1) as varchar) + ':00' AS JamSelesai, IdDokter, KdRuangan FROM RegistrasiLaboratorium
                ) AS a LEFT JOIN
                Ruangan b ON a.KdRuangan = b.KdRuangan LEFT JOIN
                DataPegawai c ON a.IdDokter = c.IdPegawai LEFT JOIN
                Pasien d ON a.NoCM = d.NoCM LEFT JOIN
                PasienMasukRumahSakit f ON a.NoPendaftaran = f.NoPendaftaran AND a.NoCM = f.NoCM LEFT JOIN
                DataPegawai g ON f.IdUser = g.IdPegawai LEFT JOIN
                Instalasi i ON b.KdInstalasi = i.KdInstalasi left join
                pasiendaftar j on a.nopendaftaran=j.nopendaftaran left join
                kelompokpasien k on j.kdkelompokpasien=k.kdkelompokpasien
                WHERE a.NoPendaftaran = '$NoPendaftaran' $NoCMParam");
                $y = $stmt->fetch(PDO::FETCH_ASSOC);
                $dede ="SELECT a.NoPendaftaran, d.NoCM, CONVERT(VARCHAR(20), a.TglMasuk, 20) AS TglMasuk, b.NamaRuangan, c.NamaLengkap AS NamaDokter, a.JamMulai, a.JamSelesai, a.NoAntrian, dbo.S_HitungUmur(d.TglLahir, a.TglMasuk) AS Umur, g.NamaLengkap AS NamaPetugas, d.NamaLengkap AS NamaPasien, CONVERT(VARCHAR(20), d.TglLahir, 20) AS TglLahir, d.JenisKelamin AS JK, i.NamaInstalasi,k.jenispasien, d.NoIdentitas
                FROM
                (
                    SELECT NoPendaftaran, NoCM, TglMasuk, NoAntrian, JamMulai, JamSelesai, IdDokter, KdRuangan FROM RegistrasiRJJam
                    UNION
                    SELECT NoPendaftaran, NoCM, TglPendaftaran AS TglMasuk, NoAntrian, '0' + cast(DATEPART(hour, TglPendaftaran) as varchar) + ':00' AS JamMulai, '0' + cast((cast(DATEPART(hour, TglPendaftaran) as int)+1) as varchar) + ':00' AS JamSelesai, IdDokter, KdRuangan FROM RegistrasiLaboratorium
                ) AS a LEFT JOIN
                Ruangan b ON a.KdRuangan = b.KdRuangan LEFT JOIN
                DataPegawai c ON a.IdDokter = c.IdPegawai LEFT JOIN
                Pasien d ON a.NoCM = d.NoCM LEFT JOIN
                PasienMasukRumahSakit f ON a.NoPendaftaran = f.NoPendaftaran AND a.NoCM = f.NoCM LEFT JOIN
                DataPegawai g ON f.IdUser = g.IdPegawai LEFT JOIN
                Instalasi i ON b.KdInstalasi = i.KdInstalasi left join
                pasiendaftar j on a.nopendaftaran=j.nopendaftaran left join
                kelompokpasien k on j.kdkelompokpasien=k.kdkelompokpasien
                WHERE a.NoPendaftaran = '$NoPendaftaran' $NoCMParam";
                // echo $dede;
                ?>
                <div style="display: inline-block; font-weight: bold; margin-right: 3px;"><?= $_SESSION['TempatLogin_ADM'] == 'klinik' ? "KLINIK<br>HILDA ALNAIRA" : "PMB Hilda Martina, S.Tr.Keb" ?></div>
                <div style="display: inline-block; font-weight: bold; border: 1px solid black; padding: 1px; font-size: 17.5px;">No : <?= $y['NoAntrian'] ?></div>
                <div style="font-weight: bold;"><?= date('d/m/Y H:i:s', strtotime($y['TglMasuk'])) ?></div>
                <?php
                    $tgl_tgllahir = date('d', strtotime($y['TglLahir']));
                    $thn_tgllahir = date('Y', strtotime($y['TglLahir']));
                    switch (date('m', strtotime($y['TglLahir']))) {
                        case '1':
                            $bln_tgllahir = 'Januari';
                            break;
                        case '2':
                            $bln_tgllahir = 'Februari';
                            break;
                        case '3':
                            $bln_tgllahir = 'Maret';
                            break;
                        case '4':
                            $bln_tgllahir = 'April';
                            break;
                        case '5':
                            $bln_tgllahir = 'Mei';
                            break;
                        case '6':
                            $bln_tgllahir = 'Juni';
                            break;
                        case '7':
                            $bln_tgllahir = 'Juli';
                            break;
                        case '8':
                            $bln_tgllahir = 'Agustus';
                            break;
                        case '9':
                            $bln_tgllahir = 'September';
                            break;
                        case '10':
                            $bln_tgllahir = 'Oktober';
                            break;
                        case '11':
                            $bln_tgllahir = 'November';
                            break;
                        default:
                            $bln_tgllahir = 'Desember';
                            break;
                    }
                    $TglLahir = $tgl_tgllahir . ' ' . $bln_tgllahir . ' ' . $thn_tgllahir;
                ?>


                <table style="margin-right: auto; margin-left: auto; font-weight: bold; text-align: left; font-size: 9px;">
                    <tr>
                        <td>NoRM</td>
                        <td class="berpadding">:</td>
                        <td><?= $y['NoCM'] ?></td>
                    </tr>
                    <tr>
                        <td>Nama</td>
                        <td class="berpadding">:</td>
                        <td><?= $y['NamaPasien'] ?></td>
                    </tr>
                    <tr>
                        <td>TglLahir</td>
                        <td class="berpadding">:</td>
                        <td style="font-size: 10px;"><?= $TglLahir ?> / Umur : <?= $y['Umur'] ?> / JK : <?= $y['JK'] ?></td>
                    </tr>
                    <?php //if ( $_SESSION['TempatLogin_ADM'] != 'klinik' ) : ?>
                        <tr>
                            <td>NIK</td>
                            <td class="berpadding">:</td>
                            <td><?= $y['NoIdentitas'] ?></td>
                        </tr>
                        <tr>
                            <td>Unit</td>
                            <td class="berpadding">:</td>
                            <td><?= $y['NamaRuangan'] ?> / <?= $y['NamaDokter'] ?></td>
                        </tr>
                    <?php //endif ?>
                    <tr>
                        <td>Penjamin</td>
                        <td class="berpadding">:</td>
                        <td><?= $y['jenispasien'] ?></td>
                    </tr>
                </table>
<?php
}else{
    $stmt = $dbConnection->prepare("SELECT a.NoPendaftaran, d.NoRM, CONVERT(VARCHAR(10), a.TglMasuk, 20) AS TglMasuk, b.NamaRuangan, c.NamaLengkap AS NamaDokter, a.JamMulai, a.JamSelesai, a.NoAntrian, dbo.S_HitungUmur(d.TglLahir, a.TglMasuk) AS Umur, g.NamaLengkap AS NamaPetugas, d.NamaLengkap AS NamaPasien, CONVERT(VARCHAR(20), d.TglLahir, 20) AS TglLahir, d.JenisKelamin AS JK, i.NamaInstalasi,k.jenispasien
            FROM
            RegistrasiRJJam a LEFT JOIN
            Ruangan b ON a.KdRuangan = b.KdRuangan LEFT JOIN
            DataPegawai c ON a.IdDokter = c.IdPegawai LEFT JOIN
            Pasien d ON a.NoCM = d.NoCM LEFT JOIN
            PasienMasukRumahSakit f ON a.NoPendaftaran = f.NoPendaftaran AND a.NoCM = f.NoCM LEFT JOIN
            DataPegawai g ON f.IdUser = g.IdPegawai LEFT JOIN
            Instalasi i ON b.KdInstalasi = i.KdInstalasi left join
            pasiendaftar j on a.nopendaftaran=j.nopendaftaran left join
            kelompokpasien k on j.kdkelompokpasien=k.kdkelompokpasien
            WHERE a.NoPendaftaran = :nopendaftaran");
            $stmt->execute([ 'nopendaftaran' => $NoPendaftaran ]);
            $y = $stmt->fetch(PDO::FETCH_ASSOC);
            ?>
            <?php if ( $_SESSION['TempatLogin_ADM'] == 'klinik' ) : ?>
                <div style="font-weight: bold">KLINIK<br>HILDA ALNAIRA</div>
            <?php else : ?>
                <div style="font-weight: bold">KLINIK BIDAN<br>HILDA MARTINA</div>
            <?php endif ?>
            <div style="font-size: 17px; font-weight: bold;"><?= $y['NoAntrian'] ?></div>
            <div><?= $y['TglMasuk'] . ', ' . $y['JamMulai'] . '-' . $y['JamSelesai'] ?></div>
            <div><?= $y['NamaPasien'] ?></div>
            <div><?= $y['NoPendaftaran'] . ' - ' . $y['NoRM'] ?></div>
            <div><?= $y['NamaRuangan'] ?></div>
            <div><?= $y['jenispasien'] ?></div>
            <div><img alt="BARCODE" src="<?php echo 'tong_sys/barcode.php?text=' . $y['NoPendaftaran']; ?>" style="height: 17px;"/></div>

<?php } ?>

