<?php
  date_default_timezone_set('Asia/Jakarta');
  setlocale(LC_ALL, 'IND');
  $tahun = date('Y');
  if ( isset($_POST['tahun']) ) {
    $tahun = $_POST['tahun'];
  }
  $query = "Select RuanganPelayanan,JenisPasien,MONTH(TglPendaftaran) as bulan, sum (jmlpasien) as jmlpasien
  from V_DataKunjunganPasienMasukyusep 
  WHERE YEAR(TglPendaftaran)='$tahun'
  and KdInstalasi ='02' and judul='KUNJUNGAN' 
  --and JenisPasien in ('BPJS PBI','BPJS NON PBI')
  --and JenisPasien in ('UMUM')
  group by ruanganpelayanan,MONTH(TglPendaftaran),JenisPasien";
  $stmt = $dbConnection->prepare($query);
  $stmt->execute();
  $data_laporan = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>
<div class="card my-3">
  <div class="card-body">
    <?php //echo $query ?>
    <div class="d-sm-flex align-items-center justify-content-between mb-2">
      <h1 class="h3 mb-0 text-gray-800">Laporan Rekap Per Bulan</h1>
      <span class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm" onclick="rekap_perbulan()"><i class="fas fa-file-excel fa-sm text-white-50"></i> Save As Excel</span>
    </div>
    <form action="page.php?modul=laporan&sub_modul=rekap_perbulan" method="POST">
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="tahun">Tahun</label>
          <select class="form-control" name="tahun" id="tahun">
            <?php $year = '2020'; for ($i=0; $i < 5; $i++) : ?>
              <option value="<?= $year ?>" <?= $year == $tahun ? 'selected' : '' ?>><?= $year ?></option>
            <?php $year++; endfor ?>
          </select>
        </div>
        <div class="form-group col-md-6">
          <label class="d-none d-md-block">&nbsp;</label>
          <button class="btn btn-primary w-100" type="submit">Cari</button>
        </div>
      </div>
    </form>
  </div>
</div>
<div class="card mb-3">
  <div class="card-body">
    <h4 class="text-center">Rekap Perbulan Bulan <?= strftime('%B') ?></h4>
    <table class="table table-bordered table-sm text-center">
      <tr>
        <td>RuanganPelayanan</td>
        <td>JenisPasien</td>
        <td>JumlahPasien</td>
      </tr>
      <?php $total = 0; foreach ( $data_laporan as $row ) : ?>
      <tr>
        <td class="text-left"><?= $row['RuanganPelayanan'] ?></td>
        <td class="text-left"><?= $row['JenisPasien'] ?></td>
        <td><?= $row['jmlpasien'] ?></td>
      </tr>
      <?php $total = $total + $row['jmlpasien']; endforeach ?>
      <tr>
        <td colspan="2">Total</td>
        <td><?= $total ?></td>
      </tr>
    </table>
  </div>
</div>