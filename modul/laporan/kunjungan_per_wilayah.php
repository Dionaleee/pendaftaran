<?php
  date_default_timezone_set('Asia/Jakarta');
  setlocale(LC_ALL, 'IND');
  $tahun = date('Y'); $bulan = date('m');
  $kelurahan_option = $dbConnection->query("SELECT DISTINCT NamaKelurahan FROM V_WilayahAll WHERE NamaKelurahan IN (SELECT DISTINCT Kelurahan FROM Pasien) AND StatusEnabled = '1' ORDER BY NamaKelurahan")->fetchAll(PDO::FETCH_ASSOC);
  $kelurahan_in = $kelurahan_selected = '';

  if ( isset($_POST['tahun']) ) {
    $tahun = $_POST['tahun'];
    $bulan = $_POST['bulan'];
    $kelurahan_selected = $_POST['kelurahan'];
    for ($i=0; $i < COUNT($kelurahan_selected); $i++) {
      if ( (COUNT($kelurahan_selected) - 1) == ($i) ) {
        $kelurahan_in .= "'" . $kelurahan_selected[$i] . "'";
      } else {
        $kelurahan_in .= "'" . $kelurahan_selected[$i] . "',";
      }
    }
  }

  $kelurahan_param = (isset($_POST['kelurahan']) ? "AND b.Kelurahan IN ($kelurahan_in)" : '' );

  $kecamatan = $dbConnection->query("SELECT DISTINCT b.Kecamatan FROM PasienMasukRumahSakit a INNER JOIN Pasien b ON a.NoCM = b.NoCM WHERE YEAR(TglMasuk) = '$tahun' AND MONTH(TglMasuk) = '$bulan' AND StatusPeriksa <> 'B' $kelurahan_param")->fetchAll(PDO::FETCH_ASSOC);
?>
<div class="card my-3">
  <div class="card-body">
    <?php // echo $kelurahan_in ?>
    <div class="d-sm-flex align-items-center justify-content-between mb-2">
      <h1 class="h3 mb-0 text-gray-800">Laporan Kunjungan Per Wilayah</h1>
      <span class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm" onclick="kunjungan_per_wilayah()"><i class="fas fa-file-excel fa-sm text-white-50"></i> Save As Excel</span>
    </div>
    <form action="page.php?modul=laporan&sub_modul=kunjungan_per_wilayah" method="POST">
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="tahun">Tahun</label>
          <select class="form-control" name="tahun" id="tahun">
            <?php $year = '2020'; for ($i=0; $i < 5; $i++) : ?>
              <option value="<?= $year ?>" <?= $year == $tahun ? 'selected' : '' ?>><?= $year ?></option>
            <?php $year++; endfor ?>
          </select>
        </div>
        <div class="form-group col-md-6">
          <label for="bulan">Bulan</label>
          <select class="form-control" name="bulan" id="bulan">
            <?php for ($i=1; $i <= 12; $i++) : ?>
              <option value="<?= $i ?>" <?= $i == $bulan ? 'selected' : '' ?>><?= strftime('%B', mktime(0, 0, 0, $i, 10)) ?></option>
            <?php endfor ?>
          </select>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="kelurahan">Kelurahan</label>
          <select class="form-control js-example-basic-multiple" name="kelurahan[]" id="kelurahan" multiple="multiple">
            <?php foreach ( $kelurahan_option as $row ) : ?>
              <?php
                $selected = (in_array($row['NamaKelurahan'], $kelurahan_selected) ? 'selected="selected"' : '');
              ?>
              <option value="<?= $row['NamaKelurahan'] ?>" <?= $selected ?>><?= $row['NamaKelurahan'] ?></option>
            <?php endforeach ?>
          </select>
        </div>
        <div class="form-group col-md-6">
          <label class="d-none d-md-block">&nbsp;</label>
          <button class="btn btn-primary w-100" type="submit">Cari</button>
        </div>
      </div>
    </form>
  </div>
</div>

<div class="card mb-3">
  <div class="card-body">
    <table class="table table-bordered">
      <thead class="thead-dark">
        <tr>
          <th>Kecamatan</th>
          <th>Kelurahan</th>
          <th>Jumlah Pasien</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ( $kecamatan as $camat ) : ?>
          <?php
            $kelurahan = $dbConnection->query("SELECT DISTINCT b.Kelurahan, count(b.Kelurahan) AS JmlPasien FROM PasienMasukRumahSakit a INNER JOIN Pasien b ON a.NoCM = b.NoCM WHERE YEAR(TglMasuk) = '$tahun' AND MONTH(TglMasuk) = '$bulan' AND StatusPeriksa <> 'B' AND b.Kecamatan = '$camat[Kecamatan]' $kelurahan_param GROUP BY b.Kelurahan")->fetchAll(PDO::FETCH_ASSOC);
          ?>
          <tr>
            <td rowspan="<?= COUNT($kelurahan) ?>"><?= $camat['Kecamatan'] ?></td>
            <?php foreach ( $kelurahan as $lurah ) : ?>
              <td><?= $lurah['Kelurahan'] ?></td>
              <td><?= $lurah['JmlPasien'] ?></td>
            <?php if ( COUNT($kelurahan) == 1 ) : ?>
            </tr>
            <?php else : ?>
            </tr>
            <tr>
            <?php endif ?>
            <?php endforeach ?>
        <?php endforeach ?>
          
      </tbody>
    </table>
  </div>
</div>