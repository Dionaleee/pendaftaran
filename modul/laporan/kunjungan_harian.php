<?php
  date_default_timezone_set('Asia/Jakarta');
  setlocale(LC_ALL, 'IND');
  $tahun = date('Y'); $bulan = date('m');
  if ( isset($_POST['tahun']) ) {
    $tahun = $_POST['tahun'];
    $bulan = $_POST['bulan'];
  }
  $query = "select b.ruanganpelayanan, b.judul, b.namainstalasi,b.bulan,b.Tahun,
  sum(b.satu) as '1', 
  sum(b.dua) as '2',
  sum(b.tiga) as '3',
  sum(b.empat) as '4',
  sum(b.lima) as '5',
  sum(b.enam) as '6',
  sum(b.tujuh) as '7',
  sum(b.delapan) as '8',
  sum(b.sembilan) as '9',
  sum(b.sepuluh) as '10',
  sum(b.sebelas) as '11',
  sum(b.duabelas) as '12',
  sum(b.tigabelas) as '13',
  sum(b.empatbelas) as '14',
  sum(b.limabelas) as  '15',
  sum(b.enambelas) as '16',
  sum(b.tujuhbelas) as '17',
  sum(b.delapanbelas) as '18',
  sum(b.sembilanbelas) as '19',
  sum(b.duapuluh) as '20',
  sum(b.duasatu) as '21',
  sum(b.duadua) as '22',
  sum(b.duatiga) as '23',
  sum(b.duaempat) as '24',
  sum(b.dualima) as '25',
  sum(b.duaenam) as '26',
  sum(b.duatujuh) as '27',
  sum(b.duadelapan) as '28',
  sum(b.duasembilan) as '29',
  sum(b.tigapuluh) as '30',
  sum(b.tigasatu) as '31',
  SUM(b.satu+b.dua+b.tiga+b.empat+b.lima+b.enam+b.tujuh+b.delapan+b.sembilan+b.sepuluh+b.sebelas+b.duabelas+b.tigabelas+b.empatbelas+b.limabelas
    +b.enambelas+b.tujuhbelas+b.delapanbelas+b.sembilanbelas+b.duapuluh+b.duasatu+b.duadua+b.duatiga+b.duaempat+b.dualima+b.duaenam+b.duatujuh+b.duadelapan+b.duasembilan
    +b.tigapuluh+b.tigasatu) as total
  from(
    select 
    a.ruanganpelayanan, 
    a.judul, 
    a.namainstalasi,
    a.kdinstalasi,
    a.bulan,
    a.tahun,
    case	when a.hari = 1 then a.JmlPasien
      else 0
    end as satu,
    case	when a.hari = 2 then a.JmlPasien
      else 0
    end as dua,
    case	when a.hari = 3 then a.JmlPasien
      else 0
    end as tiga,
    case	when a.hari = 4 then a.JmlPasien
      else 0
    end as empat,
    case	when a.hari = 5 then a.JmlPasien
      else 0
    end as lima,
    case	when a.hari = 6 then a.JmlPasien
      else 0
    end as enam,
    case	when a.hari = 7 then a.JmlPasien
      else 0
    end as tujuh,
    case	when a.hari = 8 then a.JmlPasien
      else 0
    end as delapan,
    case	when a.hari = 9 then a.JmlPasien
      else 0
    end as sembilan,
    case	when a.hari = 10 then a.JmlPasien
      else 0
    end as sepuluh,
    case	when a.hari = 11 then a.JmlPasien
      else 0
    end as sebelas,
    case	when a.hari = 12 then a.JmlPasien
      else 0
    end as duabelas,
    case	when a.hari = 13 then a.JmlPasien
      else 0
    end as tigabelas,
    case	when a.hari = 14 then a.JmlPasien
      else 0
    end as empatbelas,
    case	when a.hari = 15 then a.JmlPasien
      else 0
    end as limabelas,
    case	when a.hari = 16 then a.JmlPasien
      else 0
    end as enambelas,
    case	when a.hari = 17 then a.JmlPasien
      else 0
    end as tujuhbelas,
    case	when a.hari = 18 then a.JmlPasien
      else 0
    end as delapanbelas,
    case	when a.hari = 19 then a.JmlPasien
      else 0
    end as sembilanbelas,
    case	when a.hari = 20 then a.JmlPasien
      else 0
    end as duapuluh,
    case	when a.hari = 21 then a.JmlPasien
      else 0
    end as duasatu,
    case	when a.hari = 22 then a.JmlPasien
      else 0
    end as duadua,
    case	when a.hari = 23 then a.JmlPasien
      else 0
    end as duatiga,
    case	when a.hari = 24 then a.JmlPasien
      else 0
    end as duaempat,
    case	when a.hari = 25 then a.JmlPasien
      else 0
    end as dualima,
    case	when a.hari = 26 then a.JmlPasien
      else 0
    end as duaenam,
    case	when a.hari = 27 then a.JmlPasien
      else 0
    end as duatujuh,
    case	when a.hari = 28 then a.JmlPasien
      else 0
    end as duadelapan,
    case	when a.hari = 29 then a.JmlPasien
      else 0
    end as duasembilan,
    case	when a.hari = 30 then a.JmlPasien
      else 0
    end as tigapuluh,
    case	when a.hari = 31 then a.JmlPasien
      else 0
    end as tigasatu,
    '' as A
    from(
      Select ruanganpelayanan,
           judul, 
           day(TglPendaftaran) as hari,
           month(TglPendaftaran) as bulan,
           YEAR (TglPendaftaran) as Tahun,
           sum (jmlpasien) as jmlpasien, 		 
           namainstalasi, 
           kdinstalasi 
      from V_datakunjunganpasienmasukbstatuspasien    
      where 	 
         judul='KUNJUNGAN' 
         and (YEAR(TglPendaftaran) = '$tahun') 
         and (month(TglPendaftaran) = '$bulan') 
         and KdInstalasi in ('02','03')
         and ruanganpelayanan not like '%mandiri%'
      group by ruanganpelayanan,judul,namainstalasi,kdinstalasi,day(TglPendaftaran), month(TglPendaftaran),year(TglPendaftaran)
    ) a
  where a.JmlPasien > 0
  )b
group by b.ruanganpelayanan,b.judul,b.NamaInstalasi,b.bulan,b.Tahun
order by ruanganpelayanan";
  // V_DatakunjunganPasienMasukBjenisBstausPasien
  $stmt = $dbConnection->prepare($query);
  $stmt->execute();
  $data_laporan = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>
<div class="card my-3">
  <div class="card-body">
    <?php //echo $query ?>
    <div class="d-sm-flex align-items-center justify-content-between mb-2">
      <h1 class="h3 mb-0 text-gray-800">Laporan Kunjungan Harian</h1>
      <a href="<?= "modul/laporan/cetak_excel_kunjungan_harian.php?tahun=" . $tahun . "&bulan=" . $bulan ?>" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm"><i class="fas fa-file-excel fa-sm text-white-50"></i> Save As Excel</a>
    </div>
    <form action="page.php?modul=laporan&sub_modul=kunjungan_harian" method="POST">
      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="tahun">Tahun</label>
          <select class="form-control" name="tahun" id="tahun">
            <?php $year = '2020'; for ($i=0; $i < 5; $i++) : ?>
              <option value="<?= $year ?>" <?= $year == $tahun ? 'selected' : '' ?>><?= $year ?></option>
            <?php $year++; endfor ?>
          </select>
        </div>
        <div class="form-group col-md-4">
          <label for="bulan">Bulan</label>
          <select class="form-control" name="bulan" id="bulan">
            <?php for ($i=1; $i <= 12; $i++) : ?>
              <option value="<?= $i ?>" <?= $i == $bulan ? 'selected' : '' ?>><?= strftime('%B', mktime(0, 0, 0, $i, 10)) ?></option>
            <?php endfor ?>
          </select>
        </div>
        <div class="form-group col-md-4">
          <label class="d-none d-md-block">&nbsp;</label>
          <button class="btn btn-primary w-100" type="submit">Cari</button>
        </div>
      </div>
    </form>
  </div>
</div>
<div class="card mb-3">
  <div class="card-body" style="overflow-x: scroll;">
    <table class="table table-bordered table-sm text-center">
      <tr>
        <td>RuanganPelayanan</td>
        <td>NamaInstalasi</td>
        <td>Bulan</td>
        <td>Tahun</td>
        <?php for ($i=1; $i <= 31; $i++) : ?>
          <td><?= $i ?></td>
        <?php endfor ?>
        <td class="font-weight-bold">Total</td>
      </tr>
      <?php foreach ( $data_laporan as $row ) : ?>
      <tr>
        <td class="text-left"><?= $row['ruanganpelayanan'] ?></td>
        <td class="text-left"><?= $row['namainstalasi'] ?></td>
        <td><?= strftime('%B', mktime(0, 0, 0, $row['bulan'], 10)) ?></td>
        <td><?= $row['Tahun'] ?></td>
        <?php for ($i=1; $i <= 31; $i++) : ?>
          <td><?= $row[$i] ?></td>
        <?php endfor ?>
        <td class="font-weight-bold"><?= $row['total'] ?></td>
      </tr>
      <?php endforeach ?>
    </table>
  </div>
</div>