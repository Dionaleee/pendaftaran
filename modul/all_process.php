<?php
include('../tong_sys/sqlsrv.php');

if ( $_GET['act'] == 'cari_kelurahan' ) {
  if(!empty($_POST["keyword"])) {
    $stmt = $dbConnection->prepare("SELECT TOP 5 * FROM V_MasterWilayah WHERE NamaKelurahan LIKE '%" . $_POST['keyword'] . "%'");
    $stmt->execute();
    $kelurahan = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    if(!empty($kelurahan)) {
?>
      <ul class="list-group" id="country-list" style="width: 30vw;">
<?php
      foreach($kelurahan as $row) {
?>
        <li class="list-group-item font-weight-bold" onClick="pilih_kelurahan('<?php echo $row['KdKelurahan']; ?>');"><?php echo $row["NamaKelurahan"] . ', ' . $row['NamaKecamatan']; ?></li>
<?php 
      }
?>
      </ul>
<?php
    }
  }
} elseif ( $_GET['act'] == 'cari_kelurahan_pj' ) {
  if(!empty($_POST["keyword"])) {
    $stmt = $dbConnection->prepare("SELECT TOP 5 * FROM V_MasterWilayah WHERE NamaKelurahan LIKE '%" . $_POST['keyword'] . "%'");
    $stmt->execute();
    $kelurahan = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    if(!empty($kelurahan)) {
?>
      <ul class="list-group" id="country-list" style="width: 30vw;">
<?php
      foreach($kelurahan as $row) {
?>
        <li class="list-group-item font-weight-bold" onClick="pilih_kelurahan_pj('<?php echo $row['KdKelurahan']; ?>');"><?php echo $row["NamaKelurahan"] . ', ' . $row['NamaKecamatan']; ?></li>
<?php 
      }
?>
      </ul>
<?php
    }
  }
} elseif ( $_GET['act'] == 'cari_diagnosa' ) {
  if(!empty($_POST["keyword"])) {
    $stmt = $dbConnection->prepare("SELECT TOP 5 * FROM DiagnosaINACBG WHERE StatusEnabled = '1' AND (KdDiagnosa LIKE '%" . $_POST['keyword'] . "%' OR NamaDiagnosa LIKE '%" . $_POST['keyword'] . "%') ORDER BY NamaDiagnosa");
    $stmt->execute();
    $diagnosa = $stmt->fetchAll(PDO::FETCH_ASSOC);
      
    if(!empty($diagnosa)) {
?>
      <ul class="list-group" id="country-list" style="width: 30vw;">
<?php
      foreach($diagnosa as $row) {
?>
      <li class="list-group-item font-weight-bold" onClick="pilih_diagnosa('<?php echo $row['KdDiagnosa']; ?>');"><?php echo $row['KdDiagnosa'] . '-' . $row['NamaDiagnosa']; ?></li>
<?php 
      }
?>
      </ul>
<?php
    }
  }
} elseif ( $_GET['act'] == 'update_wilayah' ) {
  $stmt = $dbConnection->prepare("SELECT TOP 1 * FROM V_MasterWilayah WHERE KdKelurahan = :kdkelurahan");
  $stmt->execute([ 'kdkelurahan' => $_POST['kelurahan'] ]);
  echo json_encode($stmt->fetch(PDO::FETCH_ASSOC));
} elseif ( $_GET['act'] == 'update_diagnosa' ) {
  $stmt = $dbConnection->prepare("SELECT TOP 1 * FROM DiagnosaINACBG WHERE KdDiagnosa = :kddiagnosa");
  $stmt->execute([ 'kddiagnosa' => $_POST['KdDiagnosa'] ]);
  echo json_encode($stmt->fetch(PDO::FETCH_ASSOC));
} elseif ( $_GET['act'] == 'cari_pasien' ) {
  if(!empty($_POST["keyword"])) {
    $stmt = $dbConnection->prepare("SELECT TOP 5 * FROM Pasien WHERE NoCM LIKE '%" . $_POST['keyword'] . "%' OR NamaLengkap LIKE '%" . $_POST['keyword'] . "%' ORDER BY TglDaftarMembership DESC");
    $stmt->execute();
    $data_pasien = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    if(!empty($data_pasien)) {
?>
      <ul class="list-group" id="country-list" style="width: 30vw;">
<?php
      foreach($data_pasien as $row) {
      $TglLahir = strftime("%d %B %Y", strtotime($row['TglLahir']));
?>
        <li class="list-group-item font-weight-bold"><a href="<?= "page.php?modul=data_pasien&NoCM=" . $row["NoCM"] ?>"><?php echo $row["NoCM"] . ', ' . $row['NamaLengkap'] . ', ' . $TglLahir; ?></a></li>
<?php 
      }
?>
        <li class="list-group-item font-weight-bold text-center"><a href="<?= "page.php?modul=cari_pasien&NoPasien=" . $_POST['keyword'] ?>"><i class="fas fa-search"></i> Data Lainnya</a></li>
      </ul>
<?php
    }
  }
} elseif ( $_GET['act'] == 'tembak_noka' ) {
    session_start();
    $_SESSION['data_pasien']['IdAsuransi'] = $_POST["keyword"];
    var_dump($_SESSION['data_pasien']);
}