<div class="card shadow-sm" style="background-color: #ffffffd4;">
  <div class="card-body">
    <div class="row">
      <div class="col-6 text-center">
        <div class="card shadow-sm pointer" style="height: 200px;" onclick="window.location.href='page.php?modul=isi_data_ranap&ranap=umum'">
          <div class="card-body font-weight-bold d-flex justify-content-center align-items-center text-pointer" style="font-size: 1.3em;">
            UMUM
          </div>
        </div>
      </div>
      <div class="col-6 text-center">
        <div class="card shadow-sm pointer" style="height: 200px;" onclick="window.location.href='page.php?modul=isi_data_ranap&ranap=bpjs'">
          <div class="card-body font-weight-bold d-flex justify-content-center align-items-center text-pointer" style="font-size: 1.3em;">
            BPJS
          </div>
        </div>
      </div>
    </div>
  </div>
</div>