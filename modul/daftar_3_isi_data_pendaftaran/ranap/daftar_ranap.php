<?php if ( isset($_GET['nokartu_to_session']) ) : ?>
  <?php
    session_start();
    $_SESSION['data_pasien']['IdAsuransi'] = $_GET['nokartu_to_session'];
    header("Location: ../../../page.php?modul=daftar_ranap&ranap=bpjs");
  ?>
<?php endif ?>
<?php if ( $_GET['ranap'] == 'bpjs' && ($_SESSION['data_pasien']['IdAsuransi'] == '' || $_SESSION['data_pasien']['IdAsuransi'] == NULL) ) : ?>
  <div class="d-flex justify-content-center">
    <div class="card">
      <div class="card-body text-center">
        <div class="h2 font-weight-bold">Mohon Isi NoKartu BPJS Terlebih Dahulu !</div>
        <form action="modul/daftar_3_isi_data_pendaftaran/ranap/daftar_ranap.php">
          <input class="form-control text-center" type="text" name="nokartu_to_session" autofocus placeholder="Masukkan No Kartu BPJS Pasien">
          <button type="submit" class="btn btn-primary mt-3 w-100">Daftar</button>
        </form>
      </div>
    </div>
  </div>
<?php else : ?>
<?php if ( $_GET['ranap'] == 'umum' ) : ?>
  <div class="custom-control form-control-lg custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="pegawai_klinik">
    <label class="custom-control-label font-weight-bold text-danger" for="pegawai_klinik">Pegawai Klinik</label>
  </div>
<?php endif ?>
<div class="card shadow-sm mb-3" style="background-color: #ffffffd4;">
  <div class="card-body">
  <h6 class="card-title font-weight-bold">Ruangan Dan Kelas Rawat Inap</h6>
    <div class="row">
    <?php
      $umum_bpjs = $_GET['ranap'];
      $stmt = $dbConnection->prepare("SELECT DISTINCT KdKelas, Kelas, KdRuangan, NamaRuangan FROM V_KelasPelayanan WHERE KdInstalasi = :instalasi and Expr2 = :status");
      $stmt->execute([ 'instalasi' => '03', 'status' => '1' ]);
      $kelas_ranap = $stmt->fetchAll(PDO::FETCH_ASSOC);
    ?>
    <?php foreach ( $kelas_ranap as $row ) : ?>
      <?php $KdRuangan = $row['KdRuangan']; $NamaRuangan = $row['NamaRuangan']; $KdKelas = $row['KdKelas']; $Kelas = $row['Kelas']; ?>
      <div class="col-3 text-center" onclick="daftar_kelas_kamar_per_ruangan_langsung('<?= $KdRuangan ?>', '<?= $KdKelas ?>', '<?= $umum_bpjs ?>');">
        <div class="card shadow-sm pointer" style="height: 100px;">
          <div class="card-body font-weight-bold d-flex justify-content-center align-items-center text-pointer-ranap" style="font-size: 1.2em;">
            <?= $NamaRuangan . '<br />' . $Kelas ?>
          </div>
        </div>
      </div>
    <?php endforeach ?>
    </div>
  </div>
</div>
<div id="kamar_bed_ranap"></div>
<div class="card shadow-sm mb-3" style="background-color: #ffffffd4;">
  <div class="card-body">
  <h6 class="card-title font-weight-bold">SMF</h6>
    <div class="row">
    <?php
      $stmt = $dbConnection->prepare("SELECT c.KdSubInstalasi, c.NamaSubInstalasi FROM Ruangan a
      INNER JOIN SubInstalasiRuangan b ON a.KdRuangan = b.KdRuangan
      INNER JOIN SubInstalasi c ON b.KdSubInstalasi = c.KdSubInstalasi
      WHERE a.KdInstalasi = :instalasi AND c.StatusEnabled = :status ORDER BY c.NamaSubInstalasi ASC");
      $stmt->execute([ 'instalasi' => '03', 'status' => '1' ]);
      $smf = $stmt->fetchAll(PDO::FETCH_ASSOC);
    ?>
    <?php foreach ( $smf as $row ) : ?>
      <?php $KdSubInstalasi = $row['KdSubInstalasi']; $NamaSubInstalasi = $row['NamaSubInstalasi']; ?>
      <div class="col-2 text-center">
        <div class="card shadow-sm pointer" style="height: 100px;">
          <div class="card-body font-weight-bold d-flex justify-content-center align-items-center text-pointer-ranap" style="font-size: 1.2em;">
            <?= $NamaSubInstalasi ?>
          </div>
        </div>
      </div>
    <?php endforeach ?>
    </div>
  </div>
</div>
<?php endif ?>