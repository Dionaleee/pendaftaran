<?php if ( isset($_GET['nokartu_to_session']) ) : ?>
  <?php
    session_start();
    $_SESSION['data_pasien']['IdAsuransi'] = $_GET['nokartu_to_session'];
    header("Location: ../../../page.php?modul=poli&rajal=bpjs");
  ?>
<?php endif ?>
<?php if ( $_GET['rajal'] == 'bpjs' && ($_SESSION['data_pasien']['IdAsuransi'] == '' || $_SESSION['data_pasien']['IdAsuransi'] == NULL) ) : ?>
  <div class="d-flex justify-content-center">
    <div class="card">
      <div class="card-body text-center">
        <div class="h2 font-weight-bold">Mohon Isi NoKartu BPJS Terlebih Dahulu !</div>
        <form action="modul/daftar_3_isi_data_pendaftaran/rajal/poli.php">
          <input class="form-control text-center" type="text" name="nokartu_to_session" autofocus placeholder="Masukkan No Kartu BPJS Pasien">
          <button type="submit" class="btn btn-primary mt-3 w-100">Daftar</button>
        </form>
      </div>
    </div>
  </div>
<?php else : ?>
<?php if ( $_GET['rajal'] == 'umum' ) : ?>
  <div class="custom-control form-control-lg custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="pegawai_klinik">
    <label class="custom-control-label font-weight-bold text-danger" for="pegawai_klinik">Pegawai Klinik</label>
  </div>
<?php endif ?>
<div class="row d-flex justify-content-center">
  <?php
    $umum_bpjs = $_GET['rajal'];
    if ( $_SESSION['TempatLogin_ADM'] == 'klinik' ) {
      /* poli yang ada didepan cuma poli anak dan obgyn */
      /* tambah poli umum dan imunisasi 2020-12-30 011,009 */
      $stmt = $dbConnection->prepare("SELECT * FROM DaftarPoli WHERE KdRuangan IN ('007','009','011','016','026','142','027','028','029','030','031','034') AND StatusEnabled = :status");
    } else {
      $stmt = $dbConnection->prepare('SELECT * FROM DaftarPoli WHERE StatusEn4abled = :status');
    }

    $stmt->execute([ 'status' => 1 ]);
    $daftar_poli = $stmt->fetchAll(PDO::FETCH_ASSOC);
  ?>
  <?php foreach ( $daftar_poli as $row ) : ?>
    <?php $KdRuangan = $row['KdRuangan']; $NamaRuangan = $row['NamaRuangan']; $KdSubInstalasi = $row['KdSubInstalasi']; ?>
    <div class="col-2 px-2" style="flex: 0 0 16.66667%; max-width: 16.66667%;">
      <div class="card py-2 mb-3 poli-card">
        <div class="card-body p-0">
          <img class="rounded mx-auto mx-2 d-block" src="<?= 'assets/img/daftar_poli/' . $row['GambarPoli'] ?>" alt="<?= $row['NamaRuangan'] ?>" style="height: 30vh; cursor: pointer;" onclick="daftar_dokter('<?= $KdRuangan ?>', '<?= $NamaRuangan ?>', '<?= $KdSubInstalasi ?>', '<?= $umum_bpjs ?>')">
          <div class="teks-dalam-gambar"><?= $NamaRuangan ?></div>
        </div>
      </div>
    </div>
  <?php endforeach ?>
</div>
<?php endif ?>