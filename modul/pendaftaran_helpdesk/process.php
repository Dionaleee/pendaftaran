<?php
include('../../tong_sys/sqlsrv.php');
include('../../tong_sys/function_sp.php');
if (  @$_GET['act'] == 'tampil_daftar_helpdesk' ) {
  if ( $_POST['KdInstalasi'] == '02' ) {
    include('rajal.php');
  } elseif ( $_POST['KdInstalasi'] == '03' ) {
    include('ranap.php');
  } elseif ( $_POST['KdInstalasi'] == '09' ) {
    include('lab.php');
  }
} elseif ( @$_GET['act'] == 'daftar_subinstalasi_per_ruangan_ranap' ) {
  $KdRuangan = explode('|', $_POST['kdruangan'])[1];
  // $stmt = $dbConnection->prepare("SELECT KdSubInstalasi, NamaSubInstalasi FROM V_SubInstalasiRuangan WHERE (KdRuangan = :ruangan) AND StatusEnabled = :status ORDER BY NamaSubInstalasi");
  $stmt = $dbConnection->prepare("SELECT DISTINCT KdSubInstalasi, NamaSubInstalasi FROM V_SubInstalasiRuangan WHERE (KdInstalasi = :instalasi) AND StatusEnabled = :status ORDER BY NamaSubInstalasi");
  $stmt->execute([ 'instalasi' => '03', 'status' => '1' ]);
  echo "<option value=''>Pilih SMF...</option>";
  foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $row) {
    echo "<option value='" . $row['KdSubInstalasi'] . "'>". $row['NamaSubInstalasi'] ."</option>";
  }
} elseif ( @$_GET['act'] == 'daftar_kelas_kamar_per_ruangan_ranap' ) {
  $KdKelas = explode('|', $_POST['kdruangan'])[0];
  $KdRuangan = explode('|', $_POST['kdruangan'])[1];
  $stmt = $dbConnection->prepare("SELECT DISTINCT KdKelas, Kelas, KdKamar, NoKamar, NoBed FROM V_KamarRegRawatInap  WHERE (KdRuangan = :ruangan) AND KdKelas = :kelas AND StatusEnabled = :status");
  $stmt->execute([ 'ruangan' => $KdRuangan, 'kelas' => $KdKelas, 'status' => '1' ]);
  echo "<option value=''>Pilih Kelas Kamar...</option>";
  foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $row) {
    echo "<option value='" . $row['KdKamar'] . '|' . $row['NoBed'] . "'>". $row['NoKamar'] . ' - No Bed. ' . $row['NoBed'] ."</option>";
  }
} elseif ( @$_GET['act'] == 'buat_daftar_lab' ) {
  session_start();
  $data_pasien = $_SESSION['data_pasien'];
  $NoCM = $_POST['NoCM'];
  $KdKelompokPasien = $_POST['KdKelompokPasien'];
  /* INI DI PATOK DULU GESS 2021-01-04 */
  $Ruangan = '005'; // $_POST['Ruangan'];
  $IdDokter = $_POST['IdDokter'];
  $TglDaftarMembership	= gmdate("Y/m/d H:i:s", time()+60*60*7);
  $today = date('Y-m-d');
  $IdUser = $_SESSION['IdPegawai_ADM'];

  // $cek_daftar_lab = $dbConnection->query("SELECT * FROM RegistrasiLaboratorium WHERE NoCM = '$NoCM' AND NamaRujukanAsal = 'Datang Sendiri' AND CONVERT(VARCHAR(10), TglPendaftaran, 20) = '$today' ORDER BY TglPendaftaran DESC")->fetch(PDO::FETCH_ASSOC);

  $pelayanan = $dbConnection->query("SELECT DISTINCT [Jenis Pelayanan] as JenisPelayanan,[Nama Pelayanan] as NamaPelayanan,Kelas,JenisTarif,Tarif,KdPelayananRS 
  FROM V_TarifPelayananTindakan 
  where KdKelas='06' AND KdJenisTarif='02' and [Jenis Pelayanan]='Pelayanan Laboratorium Patologi Klinik'")->fetchAll(PDO::FETCH_ASSOC);
  
  // if ( $cek_daftar_lab ) {
  //   $data_registrasi_lab = $cek_daftar_lab;
  //   include("tambah_tindakan_lab.php");
  // } else {
    // SP_AU_Pasien([$NoCM, $data_pasien['NoIdentitas'], $TglDaftarMembership, $data_pasien['Title'], $data_pasien['NamaLengkap'], $data_pasien['TempatLahir'], $data_pasien['TglLahir'], $data_pasien['JenisKelamin'], $data_pasien['Alamat'], $data_pasien['Telepon'], $data_pasien['Propinsi'], $data_pasien['Kota'], $data_pasien['Kecamatan'], $data_pasien['Kelurahan'], $data_pasien['RTRW'], $data_pasien['KodePos'], $NoCM, null, null, '346', $IdUser]);

    $DokterPerujukLainnya = $_POST['DokterPerujukLainnya'];
    if ( $DokterPerujukLainnya != '' ) {
      $IdDokter = $dokter['KodeDokter'] = 'NULL';
      $dokter_perujuk = $dokter['NamaDokter'] = $DokterPerujukLainnya;
    } else {
      $dokter = $dbConnection->query("SELECT distinct KodeDokter ,NamaDokter,JK,Jabatan FROM V_DaftarDokter WHERE KodeDokter = '$IdDokter'")->fetch(PDO::FETCH_ASSOC);
      $dokter_perujuk = $data_pasien['NamaLengkap'];
    }
  
    SP_Add_RegistrasiLaboratoryLsg([$NoCM, $TglDaftarMembership, '01', 'Datang Sendiri', $dokter_perujuk, $TglDaftarMembership, '034', $Ruangan, $IdDokter, $IdUser, NULL, '06', $KdKelompokPasien, NULL, NULL, '01']);
    
    $data_registrasi_lab = $dbConnection->query("SELECT * FROM RegistrasiLaboratorium WHERE NoCM = '$NoCM' AND NamaRujukanAsal = 'Datang Sendiri' AND CONVERT(VARCHAR(10), TglPendaftaran, 20) = '$today' ORDER BY TglPendaftaran DESC")->fetch(PDO::FETCH_ASSOC);

    include("tambah_tindakan_lab.php");
  // }
} elseif ( @$_GET['act'] == 'cek_tindakan_lab' ) {
  $NoCM = $_POST['NoCM'];
  $get_all_tindakan = $dbConnection->query("SELECT * FROM Temp_TindakanLab WHERE NoCM = '$NoCM' AND CONVERT(VARCHAR(10), CreateDate, 20) = CONVERT(VARCHAR(10), GETDATE(), 20) ORDER BY CreateDate")->fetchAll(PDO::FETCH_ASSOC);

  if ( $get_all_tindakan ) {
    if ( $_POST['Dokter'] == 'NULL' ) {
      $data_dokter['NamaDokter'] = $_POST['NamaDokter'];
    } else {
      $Dokter = $get_all_tindakan[0]['IdDokter'];
      $data_dokter = $dbConnection->query("SELECT distinct KodeDokter ,NamaDokter,JK,Jabatan FROM V_DaftarDokter WHERE KodeDokter = '$Dokter'")->fetch(PDO::FETCH_ASSOC);
    }

    include("list_tindakan_lab.php");   
  } else {
    echo '';
  }
} elseif ( @$_GET['act'] == 'tambah_tindakan_lab' ) {
  session_start();
  $NoPendaftaran = $_POST['NoPendaftaran'];
  $NamaTindakan = $_POST['Tindakan'];

  $data_temp_tindakanlab = $dbConnection->query("SELECT * FROM Temp_TindakanLab WHERE NoPendaftaran = '$NoPendaftaran' AND NamaPelayanan = '$NamaTindakan'")->fetch(PDO::FETCH_ASSOC);

  $data_registrasi_lab = $dbConnection->query("SELECT     a.NoLaboratorium, a.NoPendaftaran, a.NoCM, a.TglPendaftaran, a.KdRujukanAsal, a.NamaRujukanAsal, a.NamaPerujuk, a.TglDirujuk, a.KdSubInstalasi, a.KdRuangan, a.IdDokter, 
  a.StatusPasien, a.KdKelas, a.NoAntrian, a.KdRuanganPerujuk, a.HariTglMasuk, a.BlnTglMasuk, a.ThnTglMasuk, b.KdKelompokPasien, c.KdJenisTarif
  FROM         dbo.RegistrasiLaboratorium a INNER JOIN PasienDaftar b ON a.NoPendaftaran = b.NoPendaftaran AND a.NoCM = b.NoCM INNER JOIN KelompokPasien c ON b.KdKelompokPasien = c.KdKelompokPasien AND c.StatusEnabled = '1'
  WHERE     (a.NoPendaftaran = '$NoPendaftaran')")->fetch(PDO::FETCH_ASSOC);
  $NoLaboratorium = $data_registrasi_lab['NoLaboratorium'];
  $NoCM = $data_registrasi_lab['NoCM'];
  $KdSubInstalasi = $data_registrasi_lab['KdSubInstalasi'];
  $KdRuangan = $data_registrasi_lab['KdRuangan'];
  $KdKelas = $data_registrasi_lab['KdKelas'];
  $KdJenisTarif = $data_registrasi_lab['KdJenisTarif'];

  $data_tindakan_lab = $dbConnection->query("SELECT distinct [Jenis Pelayanan] as JenisPelayanan,[Nama Pelayanan] as NamaPelayanan,Kelas,JenisTarif,cast(Tarif as float) as Tarif,KdPelayananRS 
  FROM V_TarifPelayananTindakan 
  where [Nama Pelayanan] = '$NamaTindakan' and KdKelas='$KdKelas' AND KdJenisTarif='$KdJenisTarif' and [Jenis Pelayanan]='Pelayanan Laboratorium Patologi Klinik'")->fetch(PDO::FETCH_ASSOC);
  $KdPelayananRS = $data_tindakan_lab['KdPelayananRS'];
  $Tarif = $data_tindakan_lab['Tarif'];

  $now = gmdate("Y-m-d H:i:s", time()+60*60*7);
  $IdUser = $_SESSION['IdPegawai_ADM'];

  $Jumlah = $_POST['Jumlah'];
  $Dokter = $_POST['Dokter'];
  if ( $Dokter == 'NULL' || $Dokter == '' ) {
    $data_dokter['NamaDokter'] = $_POST['NamaDokter'];
  } else {
    $data_dokter = $dbConnection->query("SELECT distinct KodeDokter ,NamaDokter,JK,Jabatan FROM V_DaftarDokter WHERE KodeDokter = '$Dokter'")->fetch(PDO::FETCH_ASSOC);
  }

  if ( $data_temp_tindakanlab ) {
    echo "<script>alert('Tindakan lab sudah ada!')</script>";
  } else {
    $temp_tindakan = $dbConnection->exec("INSERT INTO Temp_TindakanLab (NoPendaftaran, NoLaboratorium, NoCM, JenisPelayanan, NamaPelayanan, KdSubInstalasi, KdRuangan, KdKelas, StatusAPBD, KdJenisTarif, TarifCito, StatusCITO, KdPelayananRS, Tarif, CreateDate, IdDokter, IdUser, JmlPelayanan, StatusVerif, StatusSP, KeteranganTindakan)
    VALUES ('$NoPendaftaran', '$NoLaboratorium', '$NoCM', 'Pelayanan Laboratorium Patologi Klinik', '$NamaTindakan', '$KdSubInstalasi', '$KdRuangan', '$KdKelas', '01', '02', '0', '0', '$KdPelayananRS', '$Tarif', '$now', '$Dokter', '$IdUser', '$Jumlah', 'T', 'T', NULL)");
  }

  $get_all_tindakan = $dbConnection->query("SELECT * FROM Temp_TindakanLab WHERE NoPendaftaran = '$NoPendaftaran'")->fetchAll(PDO::FETCH_ASSOC);

  include("list_tindakan_lab.php");
} elseif ( @$_GET['act'] == 'hapus_tindakan_lab' ) {
  $NoPendaftaran = $_POST['NoPendaftaran'];
  $Tindakan = $_POST['Tindakan'];
  $temp_tindakan = $dbConnection->exec("DELETE FROM Temp_TindakanLab WHERE NoPendaftaran = '$NoPendaftaran' AND KdPelayananRS = '$Tindakan'");

  $get_all_tindakan = $dbConnection->query("SELECT * FROM Temp_TindakanLab WHERE NoPendaftaran = '$NoPendaftaran'")->fetchAll(PDO::FETCH_ASSOC);

  if ( $get_all_tindakan ) {    
    $Dokter = $get_all_tindakan[0]['IdDokter'];
    $data_dokter = $dbConnection->query("SELECT distinct KodeDokter ,NamaDokter,JK,Jabatan FROM V_DaftarDokter WHERE KodeDokter = '$Dokter'")->fetch(PDO::FETCH_ASSOC);

    include("list_tindakan_lab.php");    
  } else {
    echo "";
  }
} elseif ( @$_GET['act'] == 'simpan_daftar_lab' ) {
  session_start();
  $NoPendaftaran = $_POST['NoPendaftaran'];

  $get_all_tindakan = $dbConnection->query("SELECT *, CAST(Tarif AS Float) AS TarifA FROM Temp_TindakanLab WHERE NoPendaftaran = '$NoPendaftaran'")->fetchAll(PDO::FETCH_ASSOC);

  $TglPelayanan	=	date("Y-m-d H:i:s");
  $IdUser = $_SESSION['IdPegawai_ADM'];

  $NoCM = $get_all_tindakan[0]['NoCM'];
  $data_pasien = $_SESSION['data_pasien'];
  SP_AU_Pasien([$NoCM, $data_pasien['NoIdentitas'], $TglPelayanan, $data_pasien['Title'], $data_pasien['NamaLengkap'], $data_pasien['TempatLahir'], $data_pasien['TglLahir'], $data_pasien['JenisKelamin'], $data_pasien['Alamat'], $data_pasien['Telepon'], $data_pasien['Propinsi'], $data_pasien['Kota'], $data_pasien['Kecamatan'], $data_pasien['Kelurahan'], $data_pasien['RTRW'], $data_pasien['KodePos'], $NoCM, null, null, '346', $IdUser]);

  $TempatLogin = $_SESSION['TempatLogin_ADM'];
  // $dbConnection->exec("EXEC SimpanPendaftaranLab_HILDA '$NoPendaftaran','$NoCM', '$TempatLogin'");
  $NoPendaftaran = $get_all_tindakan[0]['NoPendaftaran'];
  $NoCM = $get_all_tindakan[0]['NoCM'];
  $KdRuangan = $get_all_tindakan[0]['KdRuangan'];
  $IdDokter = $get_all_tindakan[0]['IdDokter'];
  // $dbConnection->exec("INSERT INTO PasienSumberDaftar (NoPendaftaran, NoCM, KdInstalasi, TglPendaftaran, KdRuangan, IdUser, IdDokter, TempatLogin) VALUES ('$NoPendaftaran','$NoCM','09','$TglPelayanan','$KdRuangan','$IdUser','$IdDokter','$TempatLogin')");
  

  foreach ($get_all_tindakan as $row) {
    $dbConnection->exec("EXEC Add_BiayaPelayananPenunjangM 
    '$row[NoPendaftaran]', 
    '$row[KdSubInstalasi]', 
    '$row[KdRuangan]', 
    '$row[KdPelayananRS]', 
    '$row[KdKelas]', 
    '0', 
    $row[TarifA], 
    '$row[JmlPelayanan]', 
    '$TglPelayanan', 
    '$row[NoLaboratorium]', 
    '$row[IdUser]', 
    '01', 
    '$row[KdJenisTarif]', 
    '0', 
    '$row[IdUser]', 
    '$row[IdUser]', 
    null
    ");
  }

  $dbConnection->exec("DELETE FROM Temp_TindakanLab WHERE NoCM = '$NoCM'");

  echo json_encode([ 'NoPendaftaran' => $NoPendaftaran, 'NoCM' => $NoCM ]);
} elseif ( @$_POST['act'] == 'simpan_data_ranap' ) {
  session_start();
  //if ( empty($_SESSION['TempatLogin_ADM']) || $_SESSION['TempatLogin_ADM'] == '' || empty($_SESSION['IdPegawai_ADM']) || $_SESSION['IdPegawai_ADM'] == '' ) {
 //   echo "<script>alert('sesi anda telah habis, silahkan login ulang.'); window.location.href='index.php'</script>"; die();
 // }
  $data_pasien = $_SESSION['data_pasien'];
  $noka = $data_pasien['IdAsuransi'];
  $KelasRuangan = explode('|', $_POST['kelas_ruangan_ranap']);
  $KamarBed = explode('|', $_POST['kamar_bed_ranap']);
  $NoCM = $_POST['NoCM'];
  $NoIdentitasPasien = $_POST['NoIdentitas'];
  // $TglDaftarMembership	= gmdate("Y/m/d H:i:s", time()+60*60*7);
  $TglDaftarMembership	= date("Y/m/d H:i:s", strtotime($_POST['TglDaftarRanap']));
  $TglLahir	= $_POST['TglLahir'];
  $IdUser = $_SESSION['IdPegawai_ADM'];
  $KdSubInstalasi = ($_POST['smf'] == '' ? '034' : $_POST['smf']);
  $KdRuangan = $KelasRuangan[1];

  $nmJenisPeserta = ''; // $rujukan['nmJenisPeserta'];
  $HubunganPasien = '01'; // $rujukan['hubungan_pasien'];
  $PKUkdProvider = ''; // $rujukan['PKUkdProvider'];
  $PKUnmProvider = ''; // rtrim($rujukan['PKUnmProvider']);
  $noKunjungan = ''; // $rujukan['noKunjungan'];
  $AsalRujukan = '02'; // $rujukan['asal_rujukan'];
  $nmDiag = ''; // $rujukan['nmDiag'];
  
  // $kdKelas = '3'; // $rujukan['kdKelas'];
  $KdKelasDiTanggung = '03';
  $KdKelompokPasien		= $_POST['KdKelompokPasien'];

  $KdRujukanAsal = $_POST['asalrujukan'];
  $KdKelasRanap = $KelasRuangan[0];
  $KdKelasKamar = $KelasRuangan[0];
  $CaraMasuk = $_POST['cara_masuk'];

  $KdKamar = $KamarBed[0];
  $NoBed = $KamarBed[1];

  SP_AU_Pasien([$NoCM, $data_pasien['NoIdentitas'], $TglDaftarMembership, $data_pasien['Title'], $data_pasien['NamaLengkap'], $data_pasien['TempatLahir'], $data_pasien['TglLahir'], $data_pasien['JenisKelamin'], $data_pasien['Alamat'], $data_pasien['Telepon'], $data_pasien['Propinsi'], $data_pasien['Kota'], $data_pasien['Kecamatan'], $data_pasien['Kelurahan'], $data_pasien['RTRW'], $data_pasien['KodePos'], $NoCM, null, null, '346', $IdUser]);
  
  SP_Add_RegistrasiPasienMRS([$NoCM, $KdSubInstalasi, $KdRuangan, $TglDaftarMembership, $TglDaftarMembership, $KdKelasRanap, $KdKelompokPasien, $IdUser, null, null, null, null, '01', null, $KdRujukanAsal]);

  $TglPendaftaran = date('Y-m-d', strtotime($_POST['TglDaftarRanap']));
  $stmt = $dbConnection->prepare("SELECT TOP 1 NoPendaftaran FROM PasienMasukRumahSakit WHERE NoCM = :nocm AND CONVERT(VARCHAR(10), TglMasuk, 20) = :tgl AND KdRuangan = :ruangan ORDER BY TglMasuk DESC");
  $stmt->execute([ 'nocm' => $NoCM, 'tgl' => $TglPendaftaran, 'ruangan' => $KdRuangan ]);
  $data_pendaftaran = $stmt->fetch(PDO::FETCH_ASSOC);
  $NoPendaftaran = $data_pendaftaran['NoPendaftaran'];

  /* simpan penanggungjawab 23 juni 2021 */
  // if ( isset($_SESSION['NamaPJ']) ) {
  //   $dbConnection->exec("INSERT INTO PenanggungJawabPasien (NoPendaftaran, NoCM, NamaPJ, AlamatPJ, TeleponPJ, Hubungan, Propinsi, Kota, Kecamatan, Kelurahan, RTRW, KodePos, Pekerjaan, NoIdentitas) VALUES ('$NoPendaftaran', '$NoCM', '$_SESSION[NamaPJ]', '$_SESSION[AlamatPJ]', '$_SESSION[TeleponPJ]', '$_SESSION[HubunganPJ]', '$_SESSION[PropinsiPJ]', '$_SESSION[KotaPJ]', '$_SESSION[KecamatanPJ]', '$_SESSION[KelurahanPJ]', '$_SESSION[RTRWPJ]', '$_SESSION[KodePosPJ]', '$_SESSION[PekerjaanPJ]', '$_SESSION[NoIdentitasPJ]')");
  //   $NamaLengkapPenanggung = $_SESSION['NamaPJ'];
  //   $PekerjaanPenanggung = $_SESSION['PekerjaanPJ'];
  //   $HubunganPenanggung = $_SESSION['HubunganPJ'];
  //   $AlamatPenanggung = $_SESSION['AlamatPJ'];
  //   $ProvinsiPenanggung = $_SESSION['PropinsiPJ'];
  //   $KotaPenanggung = $_SESSION['KotaPJ'];
  //   $KecamatanPenanggung = $_SESSION['KecamatanPJ'];
  //   $KelurahanPenanggung = $_SESSION['KelurahanPJ'];
  //   $RTRWPenanggung = $_SESSION['RTRWPJ'];
  //   $KodePosPenanggung = $_SESSION['KodePosPJ'];
  //   $TeleponPenanggung = $_SESSION['TeleponPJ'];
  //   $NoIdentitasPenanggung = $_SESSION['NoIdentitasPJ'];
  // } else {
    $data_pj = $dbConnection->query("SELECT TOP 1 * FROM PenanggungJawabPasien WHERE NoCM = '$NoCM' ORDER BY NoPendaftaran DESC")->fetch(PDO::FETCH_ASSOC);
    $dbConnection->exec("INSERT INTO PenanggungJawabPasien (NoPendaftaran, NoCM, NamaPJ, AlamatPJ, TeleponPJ, Hubungan, Propinsi, Kota, Kecamatan, Kelurahan, RTRW, KodePos, Pekerjaan, NoIdentitas) VALUES ('$NoPendaftaran', '$NoCM', '$data_pj[NamaPJ]', '$data_pj[AlamatPJ]', '$data_pj[TeleponPJ]', '$data_pj[Hubungan]', '$data_pj[Propinsi]', '$data_pj[Kota]', '$data_pj[Kecamatan]', '$data_pj[Kelurahan]', '$data_pj[RTRW]', '$data_pj[KodePos]', '$data_pj[Pekerjaan]', '$data_pj[NoIdentitas]')");
    $NamaLengkapPenanggung = $data_pj['NamaPJ'];
    $PekerjaanPenanggung = $data_pj['Pekerjaan'];
    $HubunganPenanggung = $data_pj['Hubungan'];
    $AlamatPenanggung = $data_pj['AlamatPJ'];
    $ProvinsiPenanggung = $data_pj['Propinsi'];
    $KotaPenanggung = $data_pj['Kota'];
    $KecamatanPenanggung = $data_pj['Kecamatan'];
    $KelurahanPenanggung = $data_pj['Kelurahan'];
    $RTRWPenanggung = $data_pj['RTRW'];
    $KodePosPenanggung = $data_pj['KodePos'];
    $TeleponPenanggung = $data_pj['TeleponPJ'];
    $NoIdentitasPenanggung = $data_pj['NoIdentitas'];
  // }

  SP_Add_RegistrasiPasienRI([$NoPendaftaran, $NoCM, $KdSubInstalasi, $KdKelasKamar, $TglDaftarMembership, null, $CaraMasuk, $KdRujukanAsal, $NamaLengkapPenanggung, $PekerjaanPenanggung, $HubunganPenanggung, $AlamatPenanggung, $ProvinsiPenanggung, $KotaPenanggung, $KecamatanPenanggung, $KelurahanPenanggung, $RTRWPenanggung, $KodePosPenanggung, $TeleponPenanggung, $NoIdentitasPenanggung]);

  SP_Add_PasienMasukKamar([$NoPendaftaran, $NoCM, $KdSubInstalasi, $KdRuangan, null, $KdKelasKamar, $KdKamar, $NoBed, $TglDaftarMembership, $KdKelasRanap, null, $IdUser, $CaraMasuk, null, 'MA']);

  /* Kalo BPJS */
  if ( $KdKelompokPasien == '20' ) {
    SP_AU_AsuransiPasienJoinProgramAskesMultipel(['0000000022', $noka, $NoCM, $data_pasien['NamaLengkap'], $data_pasien['NoIdentitas'], NULL, $TglLahir, $data_pasien['Alamat'], $NoPendaftaran, $HubunganPasien, '', $TglDaftarMembership, $IdUser, '-', '1', null, 'M', 0, $nmJenisPeserta, null, $noKunjungan, $AsalRujukan, $PKUnmProvider, $PKUkdProvider, '-', $TglDaftarMembership, $nmDiag, '', '0024', $KdKelasDiTanggung, $TglDaftarMembership, '03', $KdRuangan]);
  }
  
  $stmt = $dbConnection->prepare("INSERT INTO PasienBelumBayar(NoPendaftaran, NoCM) VALUES ('$NoPendaftaran', '$NoCM')");
  $stmt->execute();

  // inject_ulang:
  // $TempatLogin = $_SESSION['TempatLogin_ADM'];
  // $TglDaftarReal = gmdate("Y/m/d H:i:s", time()+60*60*7);
  // $dbConnection->exec("INSERT INTO PasienSumberDaftar (NoPendaftaran, NoCM, KdInstalasi, TglPendaftaran, KdRuangan, IdUser, IdDokter, TempatLogin) VALUES ('$NoPendaftaran', '$NoCM', '03', '$TglDaftarReal', '$KdRuangan', '$IdUser', NULL, '$TempatLogin')");

  // $cek_sudah_masuk = $dbConnection->query("SELECT * FROM PasienSumberDaftar WHERE NoPendaftaran = '$NoPendaftaran' AND NoCM = '$NoCM'")->fetch(PDO::FETCH_ASSOC);

  // if ( !$cek_sudah_masuk ) {
  //   goto inject_ulang;
  // }
  
  SP_Add_BiayaPelayananOtomatisNew([$NoPendaftaran, $KdSubInstalasi, $KdRuangan, $TglDaftarMembership, $KdKelasKamar, $KdKelasRanap, NULL, $IdUser, 'MA']);
  
  echo "<script>alert('Berhasil Mendaftarkan Ranap Pasien NoRM $NoCM Dengan NoPendaftaran $NoPendaftaran'); window.location.href='../../page.php?modul=pendaftaran'</script>";

  /* GATAU NANTI DIPAKAI ATAU TIDAK */
  // /* SP_DetailPasien */
  // $detailPasien['NoCM'] = $NoCM;
  // $detailPasien['NamaKeluarga'] = '-';
  // $detailPasien['WargaNegara'] = ' ';
  // $detailPasien['GolDarah'] = NULL;
  // $detailPasien['Rhesus'] = '-';
  // $detailPasien['StatusNikah'] = '-';
  // $detailPasien['Pekerjaan'] = '';
  // $detailPasien['Agama'] = '-';
  // $detailPasien['Suku'] = '-';
  // $detailPasien['Pendidikan'] = '-';
  // $detailPasien['NamaAyah'] = NULL;
  // $detailPasien['NamaIbu'] = NULL;
  // $detailPasien['NamaIstriSuami'] = NULL;

  // $this->db->query("EXEC AU_DetailPasien ?,?,?,?,?,?,?,?,?,?,?,?,?", $detailPasien);
  // /* Sampai Sini */

} elseif ( @$_GET['act'] == 'cek_pj_pasien' ) {
  session_start();
  $data_pj = $dbConnection->query("SELECT TOP 1 * FROM PenanggungJawabPasien WHERE NoCM = '$_POST[NoCM]' ORDER BY NoPendaftaran DESC")->fetch(PDO::FETCH_ASSOC);
  echo json_encode(($data_pj || isset($_SESSION['NamaPJ'])) ? ['code' => 200, 'message' => 'Ada PJ'] : ['code' => 404, 'message' => 'Belum Ada Penanggung Jawab. Isi Data Penanggung Jawab Terlebih Dahulu!']);
}