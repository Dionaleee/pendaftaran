<div class="card mb-3 shadow-sm">
  <div class="card-body">
  <h6 class="card-title font-weight-bold">List Pelayanan Lab</h6>
  <table class="table table-bordered table-hover">
    <thead class="thead-dark">
      <tr>
        <th>NoPendaftaran</th>
        <th>NoCM</th>
        <th>Pelayanan</th>
        <th>Jumlah</th>
        <th>Tarif</th>
        <th>Dokter</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody>
      <?php $NoPendaftaranSave = ''; foreach ( $get_all_tindakan as $row ) : ?>
      <?php $NoPendaftaranTbl = $row['NoPendaftaran']; $KdPelayananRS = $row['KdPelayananRS'] ?>
      <tr>
        <td><?= $row['NoPendaftaran'] ?></td>
        <td><?= $row['NoCM'] ?></td>
        <td><?= $row['NamaPelayanan'] ?></td>
        <td><?= $row['JmlPelayanan'] ?></td>
        <td><?= $row['Tarif'] ?></td>
        <td><?= $data_dokter['NamaDokter'] ?></td>
        <td><span class="btn btn-danger" onclick="hapus_tindakan_lab('<?= $NoPendaftaranTbl ?>', '<?= $KdPelayananRS ?>')">Hapus</span></td>
      </tr>
      <?php $NoPendaftaranSave = $NoPendaftaranTbl; endforeach ?>
    </tbody>
  </table>
  <span class="btn btn-primary" onclick="simpan_daftar_lab('<?= $NoPendaftaranSave ?>')">Simpan</span>
  </div>
</div>