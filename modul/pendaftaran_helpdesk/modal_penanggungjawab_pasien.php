<?php
include('../../tong_sys/sqlsrv.php');
$NoCM = $_POST['NoCM'];
$stmt = $dbConnection->prepare("SELECT TOP 1 NoPendaftaran, NoCM, NamaPJ, AlamatPJ, TeleponPJ, Hubungan, Propinsi, Kota, Kecamatan, Kelurahan, RTRW, KodePos, Pekerjaan, NoIdentitas
FROM PenanggungJawabpasien WHERE NoCM = :nocm ORDER BY NoPendaftaran DESC");
$stmt->execute([ 'nocm' => $NoCM ]);
$detail_pj = $stmt->fetch(PDO::FETCH_ASSOC);
$hubungan = $dbConnection->query("SELECT * FROM HubunganKeluarga WHERE StatusEnabled = '1'")->fetchAll(PDO::FETCH_ASSOC);
$pekerjaan = $dbConnection->query("SELECT * FROM Pekerjaan WHERE StatusEnabled = '1'")->fetchAll(PDO::FETCH_ASSOC);

$ada = 'ada';

if ( $detail_pj ) {
    $detail_pj = $detail_pj;
} else {
    $detail_pj['NoCM'] = $NoCM;
    $detail_pj['NamaPJ'] = '';
    $detail_pj['AlamatPJ'] = '';
    $detail_pj['TeleponPJ'] = '';
    $detail_pj['Hubungan'] = '';
    $detail_pj['Propinsi'] = '';
    $detail_pj['Kota'] = '';
    $detail_pj['Kecamatan'] = '';
    $detail_pj['Kelurahan'] = '';
    $detail_pj['RTRW'] = '';
    $detail_pj['KodePos'] = '';
    $detail_pj['Pekerjaan'] = '';
    $detail_pj['NoIdentitas'] = '';
    $ada = 'tidak ada';
}
?>
<div class="modal-dialog modal-xl" role="document" id="modal-xl">
  <div class="modal-content">
    <div class="modal-body">
    <form action="modul/data_pasien/process.php?act=proses_pj_pasien" method="POST">
      <div class="form-row">
        <div class="form-group col-md-3">
          <label for="NoCM">NoCM</label>
          <input type="text" class="form-control" name="NoCM" id="NoCM" value="<?= $detail_pj['NoCM'] ?>" readonly>
        </div>
        <div class="form-group col-md-3">
          <label for="NamaPJ">Nama Penanggung Jawab</label>
          <input type="text" class="form-control" name="NamaPJ" id="NamaPJ" value="<?= $detail_pj['NamaPJ'] ?>">
        </div>
        <div class="form-group col-md-5">
          <label for="AlamatPJ">Alamat</label>
          <input type="text" class="form-control" name="AlamatPJ" id="AlamatPJ" value="<?= $detail_pj['AlamatPJ'] ?>">
        </div>
        <div class="form-group col-md-1">
          <label for="RTRW">RT/RW</label>
          <input type="text" class="form-control" name="RTRW" id="RTRW" value="<?= $detail_pj['RTRW'] == '' || $detail_pj['RTRW'] == null ? '/' : $detail_pj['RTRW'] ?>">
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-3">
          <label for="NoIdentitas">No Identitas</label>
          <input type="text" class="form-control" name="NoIdentitas" id="NoIdentitas" value="<?= $detail_pj['NoIdentitas'] ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="TeleponPJ">No Telepon</label>
          <input type="text" class="form-control" name="TeleponPJ" id="TeleponPJ" value="<?= $detail_pj['TeleponPJ'] ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="Hubungan">Hubungan</label>
          <select class="form-control" name="Hubungan" id="Hubungan">
            <?php foreach ( $hubungan as $row ) : ?>
              <option value="<?= $row['Hubungan'] ?>" <?= $row['Hubungan'] == $detail_pj['Hubungan'] ? 'selected' : '' ?>><?= $row['NamaHubungan'] ?></option>
            <?php endforeach ?>
          </select>
        </div>
        <div class="form-group col-md-3">
          <label for="Pekerjaan">Pekerjaan</label>
          <select class="form-control" name="Pekerjaan" id="Pekerjaan">
            <?php foreach ( $pekerjaan as $kerja ) : ?>
              <option value="<?= $kerja['KdPekerjaan'] ?>" <?= $kerja['KdPekerjaan'] == $detail_pj['Pekerjaan'] ? 'selected' : '' ?>><?= $kerja['Pekerjaan'] ?></option>
            <?php endforeach ?>
          </select>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-3">
          <label for="Kelurahan">Kelurahan</label>
          <input type="text" class="form-control" name="Kelurahan" id="KelurahanPJ" value="<?= $detail_pj['Kelurahan'] ?>">
          <div id="suggesstion-box-pj" style="position: absolute; z-index: 9999;"></div>
        </div>
        <div class="form-group col-md-3">
          <label for="Kecamatan">Kecamatan</label>
          <input type="text" class="form-control" name="Kecamatan" id="KecamatanPJ" value="<?= $detail_pj['Kecamatan'] ?>">
        </div>
        <div class="form-group col-md-2">
          <label for="Kota">Kota</label>
          <input type="text" class="form-control" name="Kota" id="KotaPJ" value="<?= $detail_pj['Kota'] ?>">
        </div>
        <div class="form-group col-md-2">
          <label for="Propinsi">Propinsi</label>
          <input type="text" class="form-control" name="Propinsi" id="PropinsiPJ" value="<?= $detail_pj['Propinsi'] ?>">
        </div>
        <div class="form-group col-md-2">
          <label for="KodePos">KodePos</label>
          <input type="text" class="form-control" name="KodePos" id="KodePosPJ" value="<?= $detail_pj['KodePos'] ?>">
        </div>
      </div>
      <button class="btn btn-primary">Simpan</button>
    </form>
    </div>
  </div>
</div>
