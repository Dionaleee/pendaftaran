<?php
include('../../tong_sys/sqlsrv.php');
if ( $_POST['KdKelompokPasien'] == '20' ) {
  $pilihrajal = 'BPJS';
  $stmt = $dbConnection->prepare("SELECT TOP 1 * FROM AsuransiPasien WHERE NoCM = :nocm AND KdInstitusiAsal = :kode ORDER BY TglBerlaku DESC");
  $stmt->execute([ 'nocm' => $_POST['NoCM'], 'kode' => '0024' ]);
  $cek_noka = $stmt->fetch(PDO::FETCH_ASSOC);  
} else {
  $pilihrajal = 'UMUM';
  $cek_noka = '';
}

$stmt = $dbConnection->prepare("SELECT * FROM Ruangan WHERE KdInstalasi = :instalasi AND StatusEnabled = :status");
$stmt->execute([ 'instalasi' => '09', 'status' => '1' ]);
$daftar_lab = $stmt->fetchAll(PDO::FETCH_ASSOC);
$daftar_dokter = $dbConnection->query("SELECT * FROM DataPegawai WHERE KdJenisPegawai IN ('001','002','006','011')")->fetchAll(PDO::FETCH_ASSOC);
?>
<div class="card mb-3 shadow-sm">
  <div class="card-body">
  <h6 class="card-title font-weight-bold">Data Registrasi Laboratorium</h6>
    <div class="row">
      <?php if ( !$cek_noka && $pilihrajal == 'BPJS' ) : ?>
        <div class="col-md-3">
          <div class="form-group">
            <label for="NoKartu">NoKartu BPJS Pasien</label>
            <input class="form-control" type="text" name="NoKartu" id="NoKartu" required>
          </div>
        </div>
      <?php endif ?>
      <div class="col-md-3">
        <div class="form-group">
          <label for="Ruangan">Ruangan Lab</label>
          <select class="form-control" name="Ruangan" id="Ruangan">
            <option value="">--PILIH RUANGAN LAB--</option>
            <?php foreach ( $daftar_lab as $row ) : ?>
              <option value="<?= $row['KdRuangan'] ?>"><?= $row['NamaRuangan'] ?></option>
            <?php endforeach ?>
          </select>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label for="Dokter">Dokter Perujuk</label>
          <select class="form-control" name="Dokter" id="Dokter">
            <option value="">--PILIH DOKTER PERUJUK--</option>
            <?php foreach ( $daftar_dokter as $row ) : ?>
              <option value="<?= $row['IdPegawai'] ?>"><?= $row['NamaLengkap'] ?></option>
            <?php endforeach ?>
          </select>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label for="Dokter">Perujuk Lainnya</label>
          <input type="text" class="form-control" name="DokterLainnya" id="DokterLainnya">
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label class="d-none d-md-block">&nbsp;</label>
          <?php $NoCM = $_POST['NoCM']; $KdKelompokPasien = $_POST['KdKelompokPasien'] ?>
          <span class="btn btn-primary" onclick="buat_daftar_lab('<?= $NoCM ?>', document.getElementById('Ruangan').value, '<?= $KdKelompokPasien ?>',document.getElementById('Dokter').value,document.getElementById('DokterLainnya').value)">Buat Pendaftaran</button>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="menu_tambah_tindakan_lab"></div>