<div class="card mb-3 shadow-sm">
  <div class="card-body">
  <h6 class="card-title font-weight-bold">Tambah Tindakan Lab</h6>
    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          <label for="TindakanLab">Tindakan Lab</label>
          <select class="form-control js-example-basic-single" name="TindakanLab" id="TindakanLab">
            <option value="">--PILIH TINDAKAN LAB--</option>
            <?php foreach ( $pelayanan as $row ) : ?>
              <option value="<?= $row['NamaPelayanan'] ?>"><?= $row['KdPelayananRS'] . ', ' . $row['NamaPelayanan'] ?></option>
            <?php endforeach ?>
          </select>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label for="Jumlah">Jumlah</label>
          <input class="form-control" type="text" name="Jumlah" id="Jumlah" value="1">
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label for="Dokter">Dokter Perujuk</label>
          <input type="hidden" name="Dokter" id="Dokter" value="<?= $dokter['KodeDokter'] ?>">
          <input type="text" class="form-control" name="NamaDokter" id="NamaDokter" value="<?= $dokter['NamaDokter'] ?>" readonly>
          <!-- <select class="form-control js-example-basic-single" name="Dokter" id="Dokter">
            <option value="">--PILIH DOKTER--</option>
            <?php //foreach ( $dokter as $row ) : ?>
              <option value="<//?= $row['KodeDokter'] ?>"><//?= $row['NamaDokter'] ?></option>
            <?php //endforeach ?>
          </select> -->
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label class="d-none d-md-block">&nbsp;</label>
          <?php $NoPendaftaran = $data_registrasi_lab['NoPendaftaran'] ?>
          <span class="btn btn-primary" onclick="tambah_tindakan_lab('<?= $NoPendaftaran ?>', document.getElementById('TindakanLab').value, document.getElementById('Jumlah').value, document.getElementById('Dokter').value, document.getElementById('NamaDokter').value)">Tambah Tindakan</button>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="list_tindakan_lab"></div>