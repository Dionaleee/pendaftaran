<?php
$NoPendaftaran = $_POST['NoPendaftaran'];
$stmt = $dbConnection->prepare("SELECT a.* FROM V_BiayaPelayananTindakan a WHERE a.NoPendaftaran = :nopendaftaran");
$stmt->execute([ 'nopendaftaran' => $NoPendaftaran ]);
$data_tindakan = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>
<div class="modal-dialog modal-xl" role="document" id="modal-xl">
  <div class="modal-content">
    <div class="modal-body">
        <h3 class="text-dark my-2">Detail Tindakan</h3>
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                <thead class="thead-dark">
                    <tr>
                        <th>NoPendaftaran</th>
                        <th>RuanganPelayanan</th>
                        <th>NamaPelayanan</th>
                        <th>TglPelayanan</th>
                        <th>JmlPelayanan</th>
                        <th>JenisPelayanan</th>
                        <th>BiayaTotal</th>
                        <th>StatusBayar</th>
                        <th>NoStruk</th>
                        <th>Operator</th>
                        <th>#</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ( $data_tindakan as $row ) : ?>
                        <tr>
                            <td><?= $row['NoPendaftaran'] ?></td>
                            <td><?= $row['NamaRuangan'] ?></td>
                            <td><?= $row['NamaPelayanan'] ?></td>
                            <td><?= $row['TglPelayanan'] ?></td>
                            <td><?= $row['JmlPelayanan'] ?></td>
                            <td><?= $row['JenisPelayanan'] ?></td>
                            <td><?= $row['BiayaTotal'] ?></td>
                            <td><?= $row['Status Bayar'] ?></td>
                            <td><?= $row['NoStruk'] ?></td>
                            <td><?= $row['Operator'] ?></td>
                            <td>
                                <?php if ( $row['NoStruk'] == NULL ) : ?>
                                    <a href="modul/daftar_pasien/process.php?act=batal_tindakan_lab&NoPendaftaran=<?= $row['NoPendaftaran'] ?>&KdPelayananRS=<?= $row['KdPelayananRS'] ?>" class="badge badge-danger" onclick="return confirm('Anda yakin membatalkan tindakan ini?')">Batal Tindakan</a></td>
                                <?php endif ?>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
  </div>
</div>