<?php
$NoBooking = $_POST['NoBooking'];
$stmt = $dbConnection->prepare("SELECT TOP 1 a.NoBooking, a.KdKelompokPasien, a.NoCM, a.KdRuangan, d.NamaRuangan AS RuanganPerawatan, a.KdSubInstalasi, b.NamaLengkap AS NamaPasien, a.IdDokter, c.NamaLengkap AS DokterPemeriksa, CONVERT(VARCHAR(20), a.TglPerjanjian, 20) AS TglPerjanjian FROM Booking_Online a INNER JOIN Pasien b ON a.NoCM = b.NoCM INNER JOIN DataPegawai c ON a.IdDokter = c.IdPegawai INNER JOIN DaftarPoli d ON a.KdRuangan = d.KdRuangan WHERE a.NoBooking = :nobooking");
$stmt->execute([ 'nobooking' => $NoBooking ]);
$data_pasien = $stmt->fetch(PDO::FETCH_ASSOC);
$KdRuangan = $data_pasien['KdRuangan'];
$stmt1 = $dbConnection->prepare("SELECT IdPegawai AS IdDokter, NamaLengkap AS NamaDokter FROM DataPegawai WHERE KdJenisPegawai in ('001','002','006') ORDER BY NamaLengkap");
$stmt1->execute();
$data_dokter = $stmt1->fetchAll(PDO::FETCH_ASSOC);
?>
<div class="modal-dialog modal-lg" role="document" id="modal-lg">
  <div class="modal-content">
    <div class="modal-body">
    <form action="modul/daftar_pasien/process.php" method="POST">
      <input type="hidden" name="act" value="proses_ubah_jadwal_perjanjian">
      <input type="hidden" name="KdKelompokPasien" value="<?= $data_pasien['KdKelompokPasien'] ?>">
      <div class="form-group row">
        <label for="NamaPasien" class="col-sm-4 col-form-label">Nama Pasien</label>
        <div class="col-sm-8">
          <input type="text" readonly class="form-control" id="NamaPasien" name="NamaPasien" value="<?= $data_pasien['NamaPasien'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="NoBooking" class="col-sm-4 col-form-label">NoPerjanjian</label>
        <div class="col-sm-8">
          <input type="text" readonly class="form-control" id="NoBooking" name="NoBooking" value="<?= $data_pasien['NoBooking'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="NoCM" class="col-sm-4 col-form-label">NoCM</label>
        <div class="col-sm-8">
          <input type="text" readonly class="form-control" id="NoCM" name="NoCM" value="<?= $data_pasien['NoCM'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="RuanganPerawatan" class="col-sm-4 col-form-label">Nama Ruangan</label>
        <div class="col-sm-8">
          <input type="text" readonly class="form-control" id="RuanganPerawatan" name="RuanganPerawatan" value="<?= $data_pasien['RuanganPerawatan'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="TglPerjanjian" class="col-sm-4 col-form-label">Tgl Perjanjian</label>
        <div class="col-sm-8">
          <input type="text" readonly class="form-control" id="TglPerjanjian" name="TglPerjanjian" value="<?= date('Y-m-d', strtotime($data_pasien['TglPerjanjian'])) ?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="TglPerjanjianBaru" class="col-sm-4 col-form-label">Tgl Perjanjian Baru</label>
        <div class="col-sm-8">
          <input type="date" class="form-control" id="TglPerjanjianBaru" name="TglPerjanjianBaru" value="<?= date('Y-m-d', strtotime($data_pasien['TglPerjanjian'])) ?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="Ruangan" class="col-sm-4 col-form-label">Ruangan</label>
        <div class="col-sm-8">
          <input type="text" readonly class="form-control" id="Ruangan" name="Ruangan" value="<?= $data_pasien['RuanganPerawatan'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="RuanganBaru" class="col-sm-4 col-form-label">Ruangan Baru</label>
        <div class="col-sm-8">
          <select class="form-control" name="RuanganBaru" id="RuanganBaru">
            <?php
                $data_ruangan = $dbConnection->query("SELECT * FROM DaftarPoli WHERE StatusEnabled = '1' ORDER BY NamaRuangan")->fetchAll(PDO::FETCH_ASSOC);
                foreach ( $data_ruangan as $row ) :
            ?>
            <option value="<?= $row['KdRuangan'] . '|' . $row['KdSubInstalasi'] ?>" <?= $row['KdRuangan'] == $data_pasien['KdRuangan'] ? 'selected' : '' ?>><?= $row['NamaRuangan'] ?></option>
            <?php endforeach ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label for="NamaDokter" class="col-sm-4 col-form-label">Nama Dokter</label>
        <div class="col-sm-8">
          <input type="text" readonly class="form-control" id="NamaDokter" name="NamaDokter" value="<?= $data_pasien['DokterPemeriksa'] ?>">
          <input type="hidden" name="IdDokterLama" value="<?= $data_pasien['IdDokter'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="IdDokterBaru" class="col-sm-4 col-form-label">Dokter Baru</label>
        <div class="col-sm-8">
          <select class="form-control" name="IdDokterBaru" id="IdDokterBaru" required>
            <option value="">--PILIH DOKTER--</option>
            <?php foreach ( $data_dokter as $row ) : ?>
              <option value="<?= $row['IdDokter'] ?>" <?= $row['IdDokter'] == $data_pasien['IdDokter'] ? 'selected' : '' ?>><?= $row['NamaDokter'] ?></option>
            <?php endforeach ?>
          </select>
        </div>
      </div>
      <button class="btn btn-success">Ubah</button>
    </form>
    </div>
  </div>
</div>
