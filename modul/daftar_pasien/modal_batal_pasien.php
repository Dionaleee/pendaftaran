<?php
$NoPendaftaran = $_POST['NoPendaftaran'];
$stmt = $dbConnection->prepare("SELECT TOP 1 a.NoPendaftaran, a.NoCM, RuanganPerawatan, NamaPasien, DokterPemeriksa, CONVERT(VARCHAR(20), TglMasuk, 20) AS TglMasuk, a.KdSubInstalasi, a.KdRuangan, b.IdAsuransi AS NoKartu, b.NoSJP AS NoSEP FROM V_DaftarInfoPasienAll a LEFT JOIN PemakaianAsuransi b ON a.NoPendaftaran = b.NoPendaftaran AND a.NoCM = b.NoCM WHERE a.NoPendaftaran = :nopendaftaran");
$stmt->execute([ 'nopendaftaran' => $NoPendaftaran ]);
$data_pasien = $stmt->fetch(PDO::FETCH_ASSOC);
$cek_bayar = $dbConnection->query("SELECT DISTINCT NoPendaftaran, NoStruk FROM V_BiayaPelayananTindakan WHERE NoPendaftaran = '$NoPendaftaran'")->fetch(PDO::FETCH_ASSOC);

?>
<div class="modal-dialog modal-lg" role="document" id="modal-lg">
  <div class="modal-content">
    <div class="modal-body">
    <form action="modul/daftar_pasien/process.php" method="POST">
      <input type="hidden" name="act" value="proses_batal_pasien">
      <input type="hidden" name="KdSubInstalasi" value="<?= $data_pasien['KdSubInstalasi'] ?>">
      <?php if ( TRIM($data_pasien['KdRuangan']) == '' || $data_pasien['KdRuangan'] == NULL ) : ?>
        <?php
          $data_pasien_lab = $dbConnection->query("SELECT * FROM RegistrasiLaboratorium WHERE NoPendaftaran = '$data_pasien[NoPendaftaran]'")->fetch(PDO::FETCH_ASSOC);
        ?>
        <?php if ( $data_pasien_lab ) : ?>
          <input type="hidden" name="KdRuangan" value="005">
        <?php else : ?>
          <input type="hidden" name="KdRuangan" value="<?= $data_pasien['KdRuangan'] ?>">
        <?php endif ?>
      <?php else : ?>
        <input type="hidden" name="KdRuangan" value="<?= $data_pasien['KdRuangan'] ?>">
      <?php endif ?>
      <div class="form-group row">
        <label for="NamaPasien" class="col-sm-4 col-form-label">Nama Pasien</label>
        <div class="col-sm-8">
          <input type="text" readonly class="form-control" id="NamaPasien" name="NamaPasien" value="<?= $data_pasien['NamaPasien'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="NoPendaftaran" class="col-sm-4 col-form-label">NoPendaftaran</label>
        <div class="col-sm-8">
          <input type="text" readonly class="form-control" id="NoPendaftaran" name="NoPendaftaran" value="<?= $data_pasien['NoPendaftaran'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="NoCM" class="col-sm-4 col-form-label">NoCM</label>
        <div class="col-sm-8">
          <input type="text" readonly class="form-control" id="NoCM" name="NoCM" value="<?= $data_pasien['NoCM'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="RuanganPerawatan" class="col-sm-4 col-form-label">Nama Ruangan</label>
        <div class="col-sm-8">
          <input type="text" readonly class="form-control" id="RuanganPerawatan" name="RuanganPerawatan" value="<?= $data_pasien['RuanganPerawatan'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="TglMasuk" class="col-sm-4 col-form-label">Tgl Pendaftaran</label>
        <div class="col-sm-8">
          <input type="text" readonly class="form-control" id="TglMasuk" name="TglMasuk" value="<?= $data_pasien['TglMasuk'] ?>">
        </div>
      </div>
      <?php if ( $data_pasien['NoKartu'] != null ) : ?>
        <div class="form-group row">
          <label for="NoKartu" class="col-sm-4 col-form-label">No Kartu Peserta</label>
          <div class="col-sm-8">
            <input type="text" readonly class="form-control" id="NoKartu" name="NoKartu" value="<?= $data_pasien['NoKartu'] == null ? '' : $data_pasien['NoKartu'] ?>">
          </div>
        </div>
        <div class="form-group row">
          <label for="NoSEP" class="col-sm-4 col-form-label">No SEP</label>
          <div class="col-sm-8">
            <input type="text" readonly class="form-control is-invalid" id="NoSEP" name="NoSEP" value="<?= $data_pasien['NoSEP'] == null ? '' : $data_pasien['NoSEP'] ?>">
            <small id="NoSEP" class="form-text font-weight-bold text-danger">Pastikan NoSEP sama dengan yang tertera di lembar SEP Pendaftaran Pasien!</small>
          </div>
        </div>
      <?php endif ?>
      <div class="form-group row">
        <label for="Keterangan" class="col-sm-4 col-form-label">Keterangan</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" id="Keterangan" name="Keterangan">
        </div>
      </div>
      <?php if ( $cek_bayar['NoStruk'] != '' || $cek_bayar['NoStruk'] != NULL ) : ?>
        <div class="form-group row">
          <label for="StatusBayar" class="col-sm-4 col-form-label">Status Bayar</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" id="StatusBayar" name="StatusBayar" value="SUDAH DIBAYAR" readonly>
          </div>
        </div>
      </form>
      <button class="btn btn-danger disabled">Batal</button>
      <?php else : ?>
        <button class="btn btn-danger">Batal</button>
      </form>
      <?php endif ?>
    </div>
  </div>
</div>
