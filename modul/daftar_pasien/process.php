<?php
session_start();
include('../../tong_sys/sqlsrv.php');
include('../../tong_sys/function_sp.php');
if ( @$_POST['act'] == 'modal_batal_pasien' ) {
  include('modal_batal_pasien.php');
} elseif ( @$_POST['act'] == 'modal_batal_pasien_perjanjian' ) {
  include('modal_batal_pasien_perjanjian.php');
} elseif ( @$_POST['act'] == 'modal_cek_tindakan_lab' ) {
  include('modal_cek_tindakan_lab.php');
} elseif ( @$_POST['act'] == 'proses_batal_pasien' ) {
  // var_dump($_POST); die();
  $param1 = [$_POST['NoPendaftaran'], $_POST['NoCM'], $_POST['KdSubInstalasi'], $_POST['KdRuangan'], date('Y/m/d H:i:s', strtotime($_POST['TglMasuk'])), gmdate("Y/m/d H:i:s", time()+60*60*7), $_POST['Keterangan'], $_SESSION['IdPegawai_ADM']];

  // $param2 = [$_POST['NoPendaftaran'], $_POST['KdRuangan'], date('Y/m/d H:i:s', strtotime($_POST['TglMasuk'])), 'B'];
  
  // var_dump($param1); echo '<br>'; var_dump($param2); die();

  // && $_POST['KdRuangan'] == '005'

  if ( ($_SESSION['IdPegawai_ADM'] != 'P000000019' && $_SESSION['IdPegawai_ADM'] != 'P000000034')  ) {
    echo "<script>alert('user ini tidak memiliki authorisasi untuk membatalkan pendaftaran lab ini!'); window.location.href='../../page.php?modul=daftar_pasien';</script>"; die();
  }

  $cek_pelayanan = $dbConnection->query("SELECT DISTINCT jmlpelayanan, tarif, kdpelayananrs, nolab_rad, nopendaftaran, nostruk FROM DetailBiayaPelayanan WHERE NoPendaftaran = '$_POST[NoPendaftaran]' AND KdPelayananRS NOT IN ('001001','001013','000158','000158')")->fetchAll(PDO::FETCH_ASSOC);
  
  if ( $cek_pelayanan ) {
    echo "<script>alert('tidak dapat membatalkan pendaftaran karena ada tindakan lain selain administrasi!'); window.location.href='../../page.php?modul=daftar_pasien';</script>"; die();
  }

  $stmt = $dbConnection->prepare('EXEC Add_PasienBatalDiPeriksaEDP ?,?,?,?,?,?,?,?');
  $key = 0;
  for ($i=1; $i <= 8; $i++) { 
    $stmt->bindParam($i, $param1[$key++]);
  }
  $stmt->execute();

  $stmt = $dbConnection->prepare('SELECT TOP 1 * FROM PasienBatalDirawat WHERE NoPendaftaran = :nopendaftaran ORDER BY TglBatal DESC');
  $stmt->execute([ 'nopendaftaran' => $_POST['NoPendaftaran'] ]);
  $cek1 = $stmt->fetch(PDO::FETCH_ASSOC);
  
  // if ( $cek1['NoPendaftaran'] && $_POST['KdRuangan'] != '005' ) {
  //   $stmt = $dbConnection->prepare('EXEC Update_RegistrasiPasienMRS ?,?,?,?');
  //   $key = 0;
  //   for ($i=1; $i <= 4; $i++) { 
  //     $stmt->bindParam($i, $param2[$key++]);
  //   }
  //   $stmt->execute();

  //   $stmt = $dbConnection->prepare('SELECT TOP 1 * FROM PasienMasukRumahSakit WHERE NoPendaftaran = :nopendaftaran AND StatusPeriksa = :status ORDER BY TglMasuk DESC');
  //   $stmt->execute([ 'nopendaftaran' => $_POST['NoPendaftaran'], 'status' => 'B' ]);
  //   $cek2 = $stmt->fetch(PDO::FETCH_ASSOC);

    if ( $cek1['NoPendaftaran'] ) {
      echo "<script>alert('berhasil membatalkan pasien'); window.location.href='../../page.php?modul=daftar_pasien';</script>";
    } else {
      echo "<script>alert('gagal membatalkan pasien'); window.location.href='../../page.php?modul=daftar_pasien';</script>";
    }
  // } else {
    // echo "<script>alert('gagal membatalkan pasien'); window.location.href='../../page.php?modul=daftar_pasien';</script>";
  // }
} elseif ( @$_POST['act'] == 'proses_batal_pasien_perjanjian' ) {
  $NoBooking = $_POST['NoBooking'];
  $KdRuangan = $_POST['KdRuangan'];
  $KdSubInstalasi = $_POST['KdSubInstalasi'];
  $NoCM = $_POST['NoCM'];
  $TglBatal = gmdate("Y/m/d H:i:s", time()+60*60*7);
  $Keterangan = "BATAL PERJANJIAN - " . $_POST['Keterangan'];
  $IdUser = $_SESSION['IdPegawai_ADM'];

  $dbConnection->exec("DELETE FROM Booking_Online WHERE NoBooking = '$NoBooking'");
  $dbConnection->exec("INSERT INTO PasienBatalDirawat (NoPendaftaran, NoCM, KdSubInstalasi, KdRuangan, TglBatal, Keterangan, IdPegawai) VALUES ('$NoBooking', '$NoCM', '$KdSubInstalasi', '$KdRuangan', '$TglBatal', '$Keterangan', '$IdUser')");
  echo "<script>alert('berhasil membatalkan pasien'); window.location.href='../../page.php?modul=daftar_pasien_perjanjian';</script>";

} elseif ( @$_POST['act'] == 'modal_ubah_dokter' ) {
  include('modal_ubah_dokter.php');
} elseif ( @$_POST['act'] == 'modal_ubah_jadwal_perjanjian' ) {
  include('modal_ubah_jadwal_perjanjian.php');
} elseif ( @$_POST['act'] == 'proses_ubah_jadwal_perjanjian' ) {
  // var_dump($_POST); die();
  $ruangan_exp = explode('|', $_POST['RuanganBaru']);
  SP_Add_Pasien_Perjanjian([$_POST['NoBooking'], $_POST['TglPerjanjianBaru'], gmdate("Y/m/d H:i:s", time()+60*60*7), $_POST['NoCM'], $_POST['KdKelompokPasien'], $ruangan_exp[0], $ruangan_exp[1], $_POST['IdDokterBaru'], '08.00', '09.00', NULL, $_SESSION['IdPegawai_ADM']]);

  echo "<script>alert('berhasil mengubah jadwal perjanjian'); window.location.href='../../page.php?modul=daftar_pasien_perjanjian';</script>";
} elseif ( @$_POST['act'] == 'proses_ubah_dokter' ) {
  $stmt1 = $dbConnection->prepare("UPDATE RegistrasiRJ SET IdDokter = :iddokterbaru WHERE NoPendaftaran = :nopendaftaran AND NoCM = :nocm AND IdDokter = :iddokterlama");
  $stmt1->execute([ 'iddokterbaru' => $_POST['IdDokterBaru'], 'nopendaftaran' => $_POST['NoPendaftaran'], 'nocm' => $_POST['NoCM'], 'iddokterlama' => $_POST['IdDokterLama'] ]);
  
  $stmt2 = $dbConnection->prepare("UPDATE RegistrasiRJJam SET IdDokter = :iddokterbaru WHERE NoPendaftaran = :nopendaftaran AND NoCM = :nocm AND IdDokter = :iddokterlama");
  $stmt2->execute([ 'iddokterbaru' => $_POST['IdDokterBaru'], 'nopendaftaran' => $_POST['NoPendaftaran'], 'nocm' => $_POST['NoCM'], 'iddokterlama' => $_POST['IdDokterLama'] ]);

  $stmt3 = $dbConnection->prepare("SELECT TOP 1 * FROM RegistrasiRJJam WHERE NoPendaftaran = :nopendaftaran AND NoCM = :nocm AND IdDokter = :iddokterbaru");
  $stmt3->execute([ 'nopendaftaran' => $_POST['NoPendaftaran'], 'nocm' => $_POST['NoCM'], 'iddokterbaru' => $_POST['IdDokterBaru'] ]);
  $update_dokter = $stmt3->fetch(PDO::FETCH_ASSOC);

  if ( $update_dokter ) {
    echo "<script>alert('berhasil ubah dokter'); window.location.href='../../page.php?modul=daftar_pasien';</script>";
  } else {
    echo "<script>alert('gagal ubah dokter'); window.location.href='../../page.php?modul=daftar_pasien';</script>";
  }
} elseif ( @$_GET['act'] == 'batal_tindakan_lab' ) {
  $IdPegawai = $_SESSION['IdPegawai_ADM'];
  $NamaPegawai = $_SESSION['NamaPegawai_ADM'];
  $KdRuangan = $_SESSION['KdRuangan_ADM'];
  $NoPendaftaran = $_GET['NoPendaftaran'];
  $KdPelayananRS = $_GET['KdPelayananRS'];
  $dbConnection->exec("INSERT INTO his_delete (CreateDate, IdUser, NamaUser, KdRuangan, Keterangan) VALUES (GETDATE(), '$IdPegawai', '$NamaPegawai', '$KdRuangan', 'HAPUS TINDAKAN LAB NoPendaftaran $NoPendaftaran KdPelayananRS $KdPelayananRS')");
  $dbConnection->exec("DELETE TOP(1) FROM BiayaPelayanan WHERE NoPendaftaran = '$NoPendaftaran' AND KdPelayananRS = '$KdPelayananRS'");
  $dbConnection->exec("DELETE TOP(1) FROM DetailBiayaPelayanan WHERE NoPendaftaran = '$NoPendaftaran' AND KdPelayananRS = '$KdPelayananRS'");
  $dbConnection->exec("DELETE TOP(1) FROM TempHargaKomponen WHERE NoPendaftaran = '$NoPendaftaran' AND KdPelayananRS = '$KdPelayananRS' AND KdKomponen = '01'");
  $dbConnection->exec("DELETE TOP(1) FROM TempHargaKomponen WHERE NoPendaftaran = '$NoPendaftaran' AND KdPelayananRS = '$KdPelayananRS' AND KdKomponen = '12'");
  echo "<script>alert('berhasil hapus tindakan'); window.location.href='../../page.php?modul=daftar_pasien';</script>";
}