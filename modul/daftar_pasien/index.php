<?php
  // include('../../tong_sys/sqlsrv.php');
  $modul = "daftar_pasien";
  // $top = "TOP 20";
  $top = "";
  if ( isset($_GET['cari']) ) {
    $TglAwal = $_GET['TglAwal'];
    $TglAkhir = $_GET['TglAkhir'];
    $Instalasi = $_GET['instalasi'] != '' ? "AND KdInstalasi = '" . $_GET['instalasi'] . "'" : '';
    $InstalasiValue = $_GET['instalasi'];
    $Ruangan = $_GET['ruangan'] != '' ? "AND KdRuangan = '" . $_GET['ruangan'] . "'" : '';
    $RuanganValue = $_GET['ruangan'];
    $IsianRadio = $_GET['isian_radio'];
    $RadioParam = isset($_GET['radio_param']) ? $_GET['radio_param'] : '';
    if ( $RadioParam == 'NoRM' ) {
      $PutParam = "AND NoCM = '$IsianRadio'";
    } elseif ( $RadioParam == 'NoPendaftaran' ) {
      $PutParam = "AND NoPendaftaran = '$IsianRadio'";
    } elseif ( $RadioParam == 'NamaPasien' ) {
      $PutParam = "AND NamaPasien LIKE '%$IsianRadio%'";
    } else {
      $PutParam = '';
    }
    $top = '';
  } else {
    $TglAwal = $TglAkhir = date('Y-m-d');
    $Instalasi = $InstalasiValue = $Ruangan = $RuanganValue = $IsianRadio = $RadioParam = $PutParam = '';
  }

  $TempatLogin = $_SESSION['TempatLogin_ADM'];
  // if ( isset($_GET['cari']) ) {
  //   $query = "SELECT $top NamaInstalasi, RuanganPerawatan, NoPendaftaran, NoRM, NamaPasien, JK, Umur, JenisPasien, NamaPenjamin, Kelas, TglMasuk, TglKeluar, StatusKeluar, KondisiPulang, KasusPenyakit, NoKamar, NoBed, Kecamatan, Kelurahan, Alamat, DokterPemeriksa, UmurTahun, UmurBulan, UmurHari, KdKelas, KdJenisTarif, KdSubInstalasi, KdRuangan, IdDokter, KdInstalasi, NoCM, TempatLogin
  // FROM V_DaftarInfoPasienAll WHERE (TglMasuk BETWEEN '$TglAwal 00:00:00' AND '$TglAkhir 23:59:59') $PutParam $Instalasi $Ruangan AND (TempatLogin IS NULL OR TempatLogin = '$TempatLogin') ORDER BY TglMasuk";
  // } else {
     $query = "SELECT TOP 20 NamaInstalasi, RuanganPerawatan, NoPendaftaran, NoRM, NamaPasien, JK, Umur, JenisPasien, NamaPenjamin, Kelas, TglMasuk, TglKeluar, StatusKeluar, KondisiPulang, KasusPenyakit, NoKamar, NoBed, Kecamatan, Kelurahan, Alamat, DokterPemeriksa, UmurTahun, UmurBulan, UmurHari, KdKelas, KdJenisTarif, KdSubInstalasi, KdRuangan, IdDokter, KdInstalasi, NoCM
     FROM V_DaftarInfoPasienAll WHERE (TglMasuk BETWEEN '$TglAwal 00:00:00' AND '$TglAkhir 23:59:59') $PutParam $Instalasi $Ruangan ORDER BY TglMasuk";
  // }
  // echo $query;
  $stmt = $dbConnection->prepare($query);
  $stmt->execute();
  $data_pasien = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>
<div class="card">
  <div class="card-body">
    <div class="d-sm-flex align-items-center justify-content-between mb-2">
      <h1 class="h3 mb-0 text-gray-800">Daftar Pasien</h1>
    </div>
    <form action="">
      <input type="hidden" name="modul" value="<?= $modul ?>">
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="TglAwal">TglPendaftaran</label>
          <input type="date" class="form-control" name="TglAwal" id="TglAwal" value="<?= $TglAwal ?>">
        </div>
        <div class="form-group col-md-6">
          <label class="d-none d-md-block" for="TglAkhir">&nbsp;</label>
          <input type="date" class="form-control" name="TglAkhir" id="TglAkhir" value="<?= $TglAkhir ?>">
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="instalasi">Instalasi</label>
          <select class="form-control" name="instalasi" id="instalasi">
            <option value="" <?= $InstalasiValue == '' ? 'selected' : '' ?>>--PILIH Instalasi--</option>
            <option value="02" <?= $InstalasiValue == '02' ? 'selected' : '' ?>>Rawat Jalan</option>
            <option value="03" <?= $InstalasiValue == '03' ? 'selected' : '' ?>>Rawat Inap</option>
            <option value="09" <?= $InstalasiValue == '09' ? 'selected' : '' ?>>Laboratorium</option>
          </select>
        </div>
        <div class="form-group col-md-6">
          <label for="ruangan">Ruangan</label>
          <select class="form-control" name="ruangan" id="ruangan">
            <option value="">--PILIH Ruangan--</option>
            <?php
              $stmt = $dbConnection->prepare("SELECT * FROM Ruangan WHERE StatusEnabled = :status AND KdInstalasi IN ('02','03') ORDER BY KdInstalasi, NamaRuangan");
              $stmt->execute([ 'status' => 1 ]);
              foreach ( $stmt->fetchAll(PDO::FETCH_ASSOC) as $row ) :
            ?>
            <option value="<?= $row['KdRuangan'] ?>" <?= $row['KdRuangan'] == $RuanganValue ? 'selected' : '' ?>><?= $row['NamaRuangan'] ?></option>
            <?php endforeach ?>
          </select>
        </div>
      </div>
      <div class="form-row mt-2">
        <div class="form-group col-md-4">
          <!-- <label class="d-none d-md-block" for="isian_radio">&nbsp;</label> -->
          <input class="form-control" type="text" name="isian_radio" id="isian_radio" value="<?= $IsianRadio ?>">
        </div>
        <div class="form-group col-md-4 d-flex justify-content-center">
          <!-- <label class="w-100 d-none d-md-block" for="isian_radio">&nbsp;</label> -->
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="radio_param" id="NoRM" value="NoRM" <?= $RadioParam == 'NoRM' ? 'checked' : '' ?>>
            <label class="form-check-label" for="NoRM">NoRM</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="radio_param" id="NoPendaftaran" value="NoPendaftaran" <?= $RadioParam == 'NoPendaftaran' ? 'checked' : '' ?>>
            <label class="form-check-label" for="NoPendaftaran">NoPendaftaran</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="radio_param" id="NamaPasien" value="NamaPasien" <?= $RadioParam == 'NamaPasien' ? 'checked' : '' ?>>
            <label class="form-check-label" for="NamaPasien">NamaPasien</label>
          </div>
        </div>
        <div class="form-group col-md-4 text-right">
          <button type="submit" class="btn btn-primary col-12" name="cari" value="true">Cari</button>
        </div>
      </div>
    </form>
  </div>
</div>
<div class="card my-3">
  <div class="card-body">
    <?php //echo $query ?>
    <div class="table-responsive">
      <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
        <thead class="thead-dark">
          <tr>
            <th>#</th>
            <th>NoPendaftaran</th>
            <th>NoRM</th>
            <th>NamaPasien</th>
            <th>NamaInstalasi</th>
            <th>RuanganPerawatan</th>
            <th>DokterPemeriksa</th>
            <th>JenisKelamin</th>
            <th>Umur</th>
            <th>JenisPasien</th>
            <th>NamaPenjamin</th>
            <th>Kelas</th>
            <th>TglMasuk</th>
            <th>TglKeluar</th>
            <th>StatusKeluar</th>
            <th>KondisiPulang</th>
          </tr>
        </thead>
        <tbody style="white-space: nowrap;">
          <?php foreach ( $data_pasien as $row ) : ?>
            <?php $NoPendaftaran = $row['NoPendaftaran'] ?>
            <?php $NoCM = $row['NoCM'] ?>
            <tr>
              <td class="daftar_pasien_row" align="center"><input class="NoPendCheckbox" type="radio" name="NoPendaftaran" id="NoPendaftaran" value="<?= $row['NoPendaftaran'] ?>"> <br /><span class="badge badge-danger p-2" onclick="cetakSEPdariDaftarPasien('<?= $NoPendaftaran ?>', '<?= $NoCM ?>')">Cetak Ulang Struk</span></td>
              <td class="daftar_pasien_row"><?= $row['NoPendaftaran'] ?></td>
              <td class="daftar_pasien_row"><?= $row['NoCM'] ?></td>
              <td class="daftar_pasien_row"><?= $row['NamaPasien'] ?></td>
              <td class="daftar_pasien_row"><?= $row['NamaInstalasi'] ?></td>
              <td class="daftar_pasien_row"><?= $row['RuanganPerawatan'] ?></td>
              <td class="daftar_pasien_row"><?= $row['DokterPemeriksa'] ?></td>
              <td class="daftar_pasien_row"><?= $row['JK'] ?></td>
              <td class="daftar_pasien_row"><?= $row['Umur'] ?></td>
              <td class="daftar_pasien_row"><?= $row['JenisPasien'] ?></td>
              <td class="daftar_pasien_row"><?= $row['NamaPenjamin'] ?></td>
              <td class="daftar_pasien_row"><?= $row['Kelas'] ?></td>
              <td class="daftar_pasien_row"><?= $row['TglMasuk'] ?></td>
              <td class="daftar_pasien_row"><?= $row['TglKeluar'] ?></td>
              <td class="daftar_pasien_row"><?= $row['StatusKeluar'] ?></td>
              <td class="daftar_pasien_row"><?= $row['KondisiPulang'] ?></td>
            </tr>
          <?php endforeach ?>
        </tbody>
      </table>
    </div>
    <button class="btn btn-danger mt-3 mr-2" onclick="
    let getId = document.querySelector('.NoPendCheckbox:checked'); getId == null ? alert('Data belum dipilih.') : konfirmasi_batal_pasien(getId.value);
    ">Batal Periksa</button>
    <button class="btn btn-success mt-3 mr-2" onclick="
    let getId = document.querySelector('.NoPendCheckbox:checked'); getId == null ? alert('Data belum dipilih.') : konfirmasi_ubah_dokter(getId.value);
    ">Ubah Dokter</button>
    <button class="btn btn-warning mt-3 mr-2" onclick="
    let getId = document.querySelector('.NoPendCheckbox:checked'); getId == null ? alert('Data belum dipilih.') : cek_tindakan_lab(getId.value);
    ">Cek Tindakan</button>
  </div>
</div>
