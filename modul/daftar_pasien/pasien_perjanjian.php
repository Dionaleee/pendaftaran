<?php
  // include('../../tong_sys/sqlsrv.php');
  $modul = "daftar_pasien_perjanjian";
  if ( isset($_GET['cari']) ) {
    $TglAwal = $_GET['TglAwal'];
    $TglAkhir = $_GET['TglAkhir'];
    $Ruangan = $_GET['ruangan'] != '' ? "AND KdRuangan = '" . $_GET['ruangan'] . "'" : '';
    $RuanganValue = $_GET['ruangan'];
    $IsianRadio = $_GET['isian_radio'];
    $RadioParam = isset($_GET['radio_param']) ? $_GET['radio_param'] : '';
    if ( $RadioParam == 'NoRM' ) {
      $PutParam = "AND NoCM = '$IsianRadio'";
    } elseif ( $RadioParam == 'NoPendaftaran' ) {
      $PutParam = "AND NoBooking = '$IsianRadio'";
    } elseif ( $RadioParam == 'NamaPasien' ) {
      $PutParam = "AND c.NamaLengkap LIKE '%$IsianRadio%'";
    } else {
      $PutParam = '';
    }
  } else {
    $TglAwal = $TglAkhir = date('Y-m-d');
    $Ruangan = $RuanganValue = $IsianRadio = $RadioParam = $PutParam = '';
  }

    $query = "SELECT a.*, b.NamaRuangan, c.NamaLengkap AS NamaPasien, d.NamaLengkap AS NamaDokter FROM Booking_Online a INNER JOIN DaftarPoli b ON a.KdRuangan = b.KdRuangan INNER JOIN Pasien c ON a.NoCM = c.NoCM INNER JOIN DataPegawai d ON a.IdDokter = d.IdPegawai WHERE (TglPerjanjian BETWEEN '$TglAwal 00:00:00' AND '$TglAkhir 23:59:59') $PutParam $Ruangan ORDER BY TglPerjanjian";
  
  $stmt = $dbConnection->prepare($query);
  $stmt->execute();
  $data_pasien = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>
<div class="card">
  <div class="card-body">
    <div class="d-sm-flex align-items-center justify-content-between mb-2">
      <h1 class="h3 mb-0 text-gray-800">Daftar Pasien Perjanjian</h1>
    </div>
    <form action="">
      <input type="hidden" name="modul" value="<?= $modul ?>">
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="TglAwal">TglPerjanjian</label>
          <input type="date" class="form-control" name="TglAwal" id="TglAwal" value="<?= $TglAwal ?>">
        </div>
        <div class="form-group col-md-6">
          <label class="d-none d-md-block" for="TglAkhir">&nbsp;</label>
          <input type="date" class="form-control" name="TglAkhir" id="TglAkhir" value="<?= $TglAkhir ?>">
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="ruangan">Ruangan</label>
          <select class="form-control" name="ruangan" id="ruangan">
            <option value="">--PILIH Ruangan--</option>
            <?php
              $stmt = $dbConnection->prepare("SELECT * FROM Ruangan WHERE StatusEnabled = :status AND KdInstalasi IN ('02') ORDER BY KdInstalasi, NamaRuangan");
              $stmt->execute([ 'status' => 1 ]);
              foreach ( $stmt->fetchAll(PDO::FETCH_ASSOC) as $row ) :
            ?>
            <option value="<?= $row['KdRuangan'] ?>" <?= $row['KdRuangan'] == $RuanganValue ? 'selected' : '' ?>><?= $row['NamaRuangan'] ?></option>
            <?php endforeach ?>
          </select>
        </div>
      </div>
      <div class="form-row mt-2">
        <div class="form-group col-md-4">
          <!-- <label class="d-none d-md-block" for="isian_radio">&nbsp;</label> -->
          <input class="form-control" type="text" name="isian_radio" id="isian_radio" value="<?= $IsianRadio ?>">
        </div>
        <div class="form-group col-md-4 d-flex justify-content-center">
          <!-- <label class="w-100 d-none d-md-block" for="isian_radio">&nbsp;</label> -->
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="radio_param" id="NoRM" value="NoRM" <?= $RadioParam == 'NoRM' ? 'checked' : '' ?>>
            <label class="form-check-label" for="NoRM">NoRM</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="radio_param" id="NoPendaftaran" value="NoPendaftaran" <?= $RadioParam == 'NoPendaftaran' ? 'checked' : '' ?>>
            <label class="form-check-label" for="NoPendaftaran">NoPerjanjian</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="radio_param" id="NamaPasien" value="NamaPasien" <?= $RadioParam == 'NamaPasien' ? 'checked' : '' ?>>
            <label class="form-check-label" for="NamaPasien">NamaPasien</label>
          </div>
        </div>
        <div class="form-group col-md-4 text-right">
          <button type="submit" class="btn btn-primary col-12" name="cari" value="true">Cari</button>
        </div>
      </div>
    </form>
  </div>
</div>
<div class="card my-3">
  <div class="card-body">
    <?php //echo $query ?>
    <div class="table-responsive">
      <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
        <thead class="thead-dark">
          <tr>
            <th>#</th>
            <th>NoPerjanjian</th>
            <th>NoRM</th>
            <th>NamaPasien</th>
            <th>RuanganPerawatan</th>
            <th>DokterPemeriksa</th>
            <th>TglPerjanjian</th>
            <th>TglDaftar</th>
            <th>NoAntrian</th>
          </tr>
        </thead>
        <tbody style="white-space: nowrap;">
          <?php foreach ( $data_pasien as $row ) : ?>
            <tr>
              <td class="daftar_pasien_row" align="center"><input class="NoPendCheckbox" type="radio" name="NoPendaftaran" id="NoPendaftaran" value="<?= $row['NoBooking'] ?>"></td>
              <td class="daftar_pasien_row"><?= $row['NoBooking'] ?></td>
              <td class="daftar_pasien_row"><?= $row['NoCM'] ?></td>
              <td class="daftar_pasien_row"><?= $row['NamaPasien'] ?></td>
              <td class="daftar_pasien_row"><?= $row['NamaRuangan'] ?></td>
              <td class="daftar_pasien_row"><?= $row['NamaDokter'] ?></td>
              <td class="daftar_pasien_row"><?= $row['TglPerjanjian'] ?></td>
              <td class="daftar_pasien_row"><?= $row['TglDaftar'] ?></td>
              <td class="daftar_pasien_row"><?= $row['NoAntrian'] ?></td>
            </tr>
          <?php endforeach ?>
        </tbody>
      </table>
    </div>
    <button class="btn btn-danger mt-3 mr-2" onclick="
    let getId = document.querySelector('.NoPendCheckbox:checked'); getId == null ? alert('Data belum dipilih.') : konfirmasi_batal_pasien_perjanjian(getId.value);
    ">Batal Periksa</button>
    <button class="btn btn-success mt-3" onclick="
    let getId = document.querySelector('.NoPendCheckbox:checked'); getId == null ? alert('Data belum dipilih.') : konfirmasi_ubah_jadwal_perjanjian(getId.value);
    ">Ubah Jadwal</button>
  </div>
</div>
