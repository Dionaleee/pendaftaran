<script>
  $(document).ready(function(){
    $('.daftar_pasien_row').css('cursor', 'pointer').click(function() {
      var checkBoxes = $(this).parent('tr').find('input:radio')
      checkBoxes.prop("checked", !checkBoxes.prop("checked"));
    });

    $('.daftar_user_row').css('cursor', 'pointer').click(function() {
      var checkBoxes = $(this).parent('tr').find('input:radio')
      checkBoxes.prop("checked", !checkBoxes.prop("checked"));
    });

    $("#kelurahan").keyup(function(){
      $.ajax({
      type: "POST",
      url: "modul/all_process.php?act=cari_kelurahan",
      data: {
        'keyword':$(this).val()
      },
      success: function(data){
        $("#suggesstion-box").show();
        $("#suggesstion-box").html(data);
        $("#kelurahan").css("background","#FFF");
      }
      });
    });

    $('.js-example-basic-multiple').select2();
    
    $('#RTRW').mask('00/00');
    
    $("#cari-pasien").keyup(function(){
      $.ajax({
      type: "POST",
      url: "modul/all_process.php?act=cari_pasien",
      data:{
        'keyword': $(this).val()
      },
      success: function(data){
        $("#suggesstion-box-pasien").show();
        $("#suggesstion-box-pasien").html(data);
        $("#cari-pasien").css("background","#FFF");
      }
      });
    });
  })

  function edit_user(p_pin) {
    $.ajax({
      type: "POST",
      url: "modul/daftar_user/process.php?act=edit_user",
      data: { PIN: p_pin },
      success: function (response) {
        $('#ajaxModal').html(response).modal();
        $("#PIN_baru").keyup(function(){
          $.ajax({
          type: "POST",
          url: "modul/daftar_user/process.php?act=cek_pin",
          data: {
            'keyword':$(this).val()
          },
          dataType: 'JSON',
          success: function (response) {
            if (response.code == '1') {
              console.log(response)
              document.getElementById('PIN_baru').value = document.getElementById('PIN_lama').value;
              alert('PIN sudah digunakan, silahkan kombinasikan PIN lain!');
            }
          }
          });
        });
      }
    });
  }

  function tambah_user() {
    $.ajax({
      type: "POST",
      url: "modul/daftar_user/process.php?act=tambah_user",
      success: function (response) {
        setTimeout(() => {
          $('.js-example-basic-single').select2();
        }, 100);
        $('#ajaxModal').html(response).modal();
        $("#PIN").keyup(function(){
          $.ajax({
          type: "POST",
          url: "modul/daftar_user/process.php?act=cek_pin",
          data: {
            'keyword':$(this).val()
          },
          dataType: 'JSON',
          success: function (response) {
            if (response.code == '1') {
              console.log(response)
              // document.getElementById('PIN').value = '';
              // alert('PIN sudah digunakan, silahkan kombinasikan PIN lain!');
              $("#IdPegawai").val(response.data.IdPegawai)
              $("#NamaPegawai").val(response.data.NamaPegawai).trigger("change")
              $("#Password").val(response.data.UserPassword)
              $("#Permissions").val(response.data.PermissionsAllowed)
            }
          }
          });
        });
      }
    });
  }

  function pilih_kelurahan(p_kdkelurahan) {
    $.ajax({
      type: "POST",
      url: "modul/all_process.php?act=update_wilayah",
      data: {
        kelurahan: p_kdkelurahan,
      },
      success: function (response) {
        let result = JSON.parse(response)
        $("#kelurahan").val(result.NamaKelurahan);
        $("#kecamatan").val(result.NamaKecamatan);
        $("#kota").val(result.NamaKotaKabupaten);
        $("#provinsi").val(result.NamaPropinsi);
        $("#kodepos").val(result.KodePos);
        $("#suggesstion-box").hide();
      }
    });
  }

  function pilih_kelurahan_pj(p_kdkelurahan) {
    $.ajax({
      type: "POST",
      url: "modul/all_process.php?act=update_wilayah",
      data: {
        kelurahan: p_kdkelurahan,
      },
      success: function (response) {
        let result = JSON.parse(response)
        $("#KelurahanPJ").val(result.NamaKelurahan);
        $("#KecamatanPJ").val(result.NamaKecamatan);
        $("#KotaPJ").val(result.NamaKotaKabupaten);
        $("#PropinsiPJ").val(result.NamaPropinsi);
        $("#KodePosPJ").val(result.KodePos);
        $("#suggesstion-box-pj").hide();
      }
    });
  }

  function pilih_diagnosa(p_kddiagnosa) {
    $.ajax({
      type: "POST",
      url: "modul/all_process.php?act=update_diagnosa",
      data: {
        KdDiagnosa: p_kddiagnosa,
      },
      success: function (response) {
        let result = JSON.parse(response)
        $("#Diagnosa").val(result.KdDiagnosa + ' - ' + result.NamaDiagnosa);
        $("#suggesstion-box-diagnosa").hide();
      }
    });
  }

  function update_data_pasien() {
    $.ajax({
      type: "POST",
      url: "modul/data_pasien/process.php?act=update_data_pasien",
      data: {
        'NoCM': $("#NoCM").val(),
        'NoIdentitas': $("#NoIdentitas").val(),
        'NamaDepan': $("#NamaDepan").val(),
        'NamaLengkap': $("#NamaLengkap").val(),
        'TempatLahir': $("#TempatLahir").val(),
        'TglLahir': $("#TglLahir").val(),
        'JenisKelamin': $("#JenisKelamin").val(),
        'Alamat': $("#AlamatLengkap").val(),
        'Telepon': $("#Telepon").val(),
        'Propinsi': $("#provinsi").val(),
        'Kota': $("#kota").val(),
        'Kecamatan': $("#kecamatan").val(),
        'Kelurahan': $("#kelurahan").val(),
        'RTRW': $("#RTRW").val(),
        'KodePos': $("#kodepos").val()
      },
      dataType: "JSON",
      success: function (response) {
        alert(response.message)
      }
    });
  }
  
  let redirect = 'pendaftaran'
  $('body').keydown( function(e) {
    if ( e.which == '113' ) {
      window.location.href = "page.php?modul=daftar_pasien";
      return false;
    } else if ( e.which == '121' ) {
      let getId = document.querySelector('.NoPendCheckbox:checked'); 
      redirect = 'daftar_pasien'
      getId == null ? alert('Data belum dipilih.') : cetakSEP(getId.value);
    }
  })

  function cetakSEPdariDaftarPasien(p_NoPendaftaran, p_NoCM) {
    redirect = 'daftar_pasien'
    cetakSEP(p_NoPendaftaran, p_NoCM);
  }

  let kdruangan, kdsubinstalasi, pilihrajal, iddokter, jammulai, jamselesai, cek = false
  let kdkelompokpasien = '01'
  let perjanjian = 'engga'
  /* belum ada bridging sama bpjs */
  let sep = 'manual'

  function daftar_dokter(p_kdruangan, p_namaruangan, p_kdsubinstalasi, p_pilihan_rajal) {
    let result

    kdruangan = p_kdruangan
    kdsubinstalasi = p_kdsubinstalasi
    pilihrajal = p_pilihan_rajal
    $.ajax({
      type: "POST",
      url: "modul/daftar_3_isi_data_pendaftaran/rajal/process.php",
      data: {
        kdruangan: p_kdruangan,
        cek_kuota_dokter: 'yes'
      },
      async: false,
      success: function (response) {
        result = JSON.parse(response)
      }
    });

    if ( result.kuota === 0 ) {
      simpan_daftar('L000000004', '08.00', '09.00')
      // alert("Tidak Ada Jadwal Dokter " + p_namaruangan + " Hari Ini..!")
      // return false
    } else {
      $.ajax({
        type: "POST",
        url: "modul/daftar_3_isi_data_pendaftaran/rajal/process.php",
        data: {
          kdruangan: p_kdruangan,
          namaruangan: p_namaruangan,
          cek: cek
        },
        success: function(response){
          $('#ajaxModal').html(response).modal();
        }
      });
    }
  }

  function daftar_dokter_rajal_helpdesk(p_kdruangan, p_pilihan_rajal) {
    let noka = $("#NoKartu");
    let q_pilihrajal
    if ( p_pilihan_rajal == '20' ) {
      q_pilihrajal = 'bpjs'
    } else {
      q_pilihrajal = 'umum'
    }

    if ( noka.val() == '' ) {
      $("#Ruangan").val('')
      alert('NoKartu Tidak Boleh Kosong')
      noka.focus()
      return false
    } else {
      $.ajax({
        type: "POST",
        url: "modul/daftar_3_isi_data_pendaftaran/rajal/process.php",
        data: {
          kdruangan: p_kdruangan,
          cek_kelengkapan_param_daftar_dokter: 'yes'
        },
        async: false,
        success: function (response) {
          let result = JSON.parse(response)
          if ( result.message == 'Dokter Belum Diisi' ) {
            kdruangan = result.KdRuangan
            kdsubinstalasi = result.KdSubInstalasi
            pilihrajal = p_pilihan_rajal
            simpan_daftar('L000000004', '08.00', '09.00')
            // $("#Ruangan").val('')
            // alert("Tidak Ada Jadwal Dokter Hari Ini..!")
          } else {
            daftar_dokter(result.KdRuangan, result.NamaRuangan, result.KdSubInstalasi, q_pilihrajal)
          }
        }
      });
    }
  }

  $("#pegawai_klinik").on('click', (e) => {
    if( $('#pegawai_klinik').is(":checked") )
    {
      kdkelompokpasien = '07'
    }else{
      kdkelompokpasien = '01'
    }
  })

  function tampil_pendaftaran_helpdesk(p_instalasi, p_penjamin, p_nocm) {
    $.ajax({
      type: "POST",
      url: "modul/pendaftaran_helpdesk/process.php?act=cek_pj_pasien",
      data: {NoCM: p_nocm},
      dataType: "JSON",
      success: function (response) {
        if ( response.code == 200 ) {
          if ( p_instalasi != '' && p_penjamin != '' ) {
            kdkelompokpasien = p_penjamin
            $.ajax({
              type: "POST",
              url: "modul/pendaftaran_helpdesk/process.php?act=tampil_daftar_helpdesk",
              data: {
                KdInstalasi: p_instalasi,
                KdKelompokPasien: p_penjamin,
                NoCM: p_nocm
              },
              success: function (response) {
                $("#sub_pendaftaran_helpdesk").html(response)
                $("#Diagnosa").keyup(function(){
                  $.ajax({
                  type: "POST",
                  url: "modul/all_process.php?act=cari_diagnosa",
                  data: {
                    'keyword':$(this).val()
                  },
                  success: function(data){
                    $("#suggesstion-box-diagnosa").show();
                    $("#suggesstion-box-diagnosa").html(data);
                    $("#Diagnosa").css("background","#FFF");
                  }
                  });
                });          
                $("#perjanjian").on('click', (e) => {
                  if( $('#perjanjian').is(":checked") )
                  {
                    $("#tgl_perjanjian").css('display', 'block')
                    perjanjian = 'iya'
                  }else{
                    $("#tgl_perjanjian").css('display', 'none')
                    perjanjian = 'engga'
                  }
                })
              }
            });
          } else {
            $("#sub_pendaftaran_helpdesk").html('')
          }
        } else {
          document.getElementById('Penjamin').value = ''
          document.getElementById('Instalasi').value = ''
          alert(response.message);
          $("#sub_pendaftaran_helpdesk").html('');
        }
      }
    });
    
  }

  function buat_noka(p_noka) {
    // $("#NoKartu").keyup(function(){
      setTimeout(() => {
        $.ajax({
        type: "POST",
        url: "modul/all_process.php?act=tembak_noka",
        data: {
          'keyword':p_noka
        }
        });
      }, 10);
    // });
  }

  function simpan_daftar(p_iddokter, p_jammulai, p_jamselesai) {
    if ( perjanjian == 'iya' ) {
      let tgl_perjanjian = $("#TglPerjanjian").val();
      $.ajax({
        type: "POST",
        url: "modul/daftar_4_simpan_pendaftaran/rajal/process.php?act=simpan_perjanjian",
        data: {
          KdRuangan: kdruangan,
          KdSubInstalasi: kdsubinstalasi,
          JamMulai: p_jammulai,
          JamSelesai: p_jamselesai,
          IdDokter: p_iddokter,
          KdKelompokPasien: kdkelompokpasien,
          TglPerjanjian: tgl_perjanjian
        },
        dataType: "JSON",
        success: function (response) {
          alert("Pasien NoRM " + response.NoCM + " Berhasil Didaftarkan Untuk Tanggal " + response.TglPerjanjian + " Dengan NoBooking " + response.NoBooking)
          // cetakStrukPerjanjian(result.NoBooking)
          window.location.href = 'page.php?modul=pendaftaran'
        }
      });
    } else {
      if ( pilihrajal == 'bpjs' ) {
        let responseSEP
        if ( sep === 'manual' ) {
          responseSEP = {
            response: {
              NoSEP: '',
              MessageSEP: 'OK'
            }
          };
        } else {
          // $.ajax({
          //   type: "POST",
          //   url: '',
          //   data: {
          //     dokter: p_iddokter,
          //     koderuangan: kdruangan
          //   },
          //   dataType: "JSON",
          //   async: false,
          //   success: function(response){
          //     console.log(response)
          //     responseSEP = response
          //   }
          // })
        }
  
        let MessageSEP = responseSEP.response.MessageSEP
        let NoSEP = responseSEP.response.NoSEP
  
        if ( MessageSEP !== 'OK' ) {
          alert('SEP Tidak Dapat Diterbitkan. ' + responseSEP.response.MessageSEP)
          window.location.href = "page.php?modul=pendaftaran"
        } else {
          let result
          $.ajax({
            type: "POST",
            url: "modul/daftar_4_simpan_pendaftaran/rajal/process.php?act=simpan_bpjs",
            data: {
              KdRuangan: kdruangan,
              KdSubInstalasi: kdsubinstalasi,
              JamMulai: p_jammulai,
              JamSelesai: p_jamselesai,
              IdDokter: p_iddokter,
              NoSEP: NoSEP
            },
            dataType: "JSON",
            async: false,
            success: function (response) {
              result = response
            }
          });
          
          if ( result.NoPendaftaran == '' ) {
            alert(result.message)
            window.location.href = "page.php?modul=pendaftaran"
          } else {
            cetakSEP(result.NoPendaftaran)
          }
        }
      } else {
        let result
        let perujuk_rajalnya = document.getElementById('Perujuk_Rajal') == null ? '' : document.getElementById('Perujuk_Rajal').value
        $.ajax({
          type: "POST",
          url: "modul/daftar_4_simpan_pendaftaran/rajal/process.php?act=simpan_umum",
          data: {
            KdRuangan: kdruangan,
            KdSubInstalasi: kdsubinstalasi,
            JamMulai: p_jammulai,
            JamSelesai: p_jamselesai,
            IdDokter: p_iddokter,
            KdKelompokPasien: kdkelompokpasien,
            Perujuk_Rajal: perujuk_rajalnya
          },
          dataType: "JSON",
          async: false,
          success: function (response) {
            result = response
          }
        });
        // alert(result.message)
        cetakSEP(result.NoPendaftaran)
        // window.location.href = "page.php?modul=pendaftaran"
      }
    }
  }

  function cetakSEP(NoPendaftaran, p_NoCM = '') {
    let p_url;
    // if ( "<//?= $_SESSION['PIN_ADM'] ?>" == '5909' ) {
        p_url = "modul/cetak/struk_daftar2.php";
      // } else {
        // p_url = "modul/cetak/struk_daftar.php";
      // }
    $.ajax({
      url: p_url,
      type: 'GET',
      data: {
        nopendaftaran: NoPendaftaran,
        nocm: p_NoCM
      }
    })
    .done(function(response) {
      let a = document.getElementById('printing-css').value;
      let b = $(".print-area").html(response);
      let c = document.getElementById('print-area-1').innerHTML;
      window.frames["print_frame"].document.title = document.title;
      window.frames["print_frame"].document.body.innerHTML = '<style>' + a + '</style>' + c;
      setTimeout(function() {
        window.frames["print_frame"].window.focus();
        window.frames["print_frame"].window.print();
        window.location.href = "page.php?modul=" + redirect;
      }, 2000);
    });
  }

  function konfirmasi_batal_pasien(p_nopendaftaran) {
    $.ajax({
      type: "POST",
      url: "modul/daftar_pasien/process.php",
      data: {
        NoPendaftaran: p_nopendaftaran,
        act: 'modal_batal_pasien'
      },
      success: function(response){
        $('#ajaxModal').html(response).modal();
      }
    });
  }

  function cek_tindakan_lab(p_nopendaftaran) {
    $.ajax({
      type: "POST",
      url: "modul/daftar_pasien/process.php",
      data: {
        NoPendaftaran: p_nopendaftaran,
        act: 'modal_cek_tindakan_lab'
      },
      success: function(response){
        $('#dataTable').DataTable();
        $('#ajaxModal').html(response).modal();
      }
    });
  }

  function modal_detail_pasien(p_nocm) {
    $.ajax({
      type: "POST",
      url: "modul/data_pasien/modal_detail_pasien.php",
      data: {
        NoCM: p_nocm
      },
      success: function(response){
        $('#ajaxModal').html(response).modal();
      }
    });
  }

  function modal_penanggungjawab_pasien(p_nocm) {
    $.ajax({
      type: "POST",
      url: "modul/pendaftaran_helpdesk/modal_penanggungjawab_pasien.php",
      data: {
        NoCM: p_nocm
      },
      success: function(response){
        $('#ajaxModal').html(response).modal();
        $("#KelurahanPJ").keyup(function(){
          $.ajax({
          type: "POST",
          url: "modul/all_process.php?act=cari_kelurahan_pj",
          data: {
            'keyword':$(this).val()
          },
          success: function(data){
            $("#suggesstion-box-pj").show();
            $("#suggesstion-box-pj").html(data);
            $("#KelurahanPJ").css("background","#FFF");
          }
          });
        });
      }
    });
  }

  function konfirmasi_batal_pasien_perjanjian(p_nobooking) {
    $.ajax({
      type: "POST",
      url: "modul/daftar_pasien/process.php",
      data: {
        NoBooking: p_nobooking,
        act: 'modal_batal_pasien_perjanjian'
      },
      success: function(response){
        $('#ajaxModal').html(response).modal();
      }
    });
  }

  function konfirmasi_ubah_dokter(p_nopendaftaran) {
    $.ajax({
      type: "POST",
      url: "modul/daftar_pasien/process.php",
      data: {
        NoPendaftaran: p_nopendaftaran,
        act: 'modal_ubah_dokter'
      },
      success: function(response){
        $('#ajaxModal').html(response).modal();
      }
    });
  }

  function konfirmasi_ubah_jadwal_perjanjian(p_nobooking) {
    $.ajax({
      type: "POST",
      url: "modul/daftar_pasien/process.php",
      data: {
        NoBooking: p_nobooking,
        act: 'modal_ubah_jadwal_perjanjian'
      },
      success: function(response){
        $('#ajaxModal').html(response).modal();
      }
    });
  }

  function daftar_subinstalasi_per_ruangan(p_kdruangan) {
    let noka = $("#NoKartu");
    if ( noka.val() == '' ) {
      $("#kelas_ruangan_ranap").val('')
      alert('NoKartu Tidak Boleh Kosong')
      noka.focus()
      return false
    } else {
      $.ajax({
        type: "POST",
        url: "modul/pendaftaran_helpdesk/process.php?act=daftar_subinstalasi_per_ruangan_ranap",
        data: {
          kdruangan: p_kdruangan,
        },
        success: function (response) {
          $("#smf").removeAttr("readonly").html(response)
        }
      });
    }

  }

  function daftar_kelas_kamar_per_ruangan(p_kdruangan) {
    $.ajax({
      type: "POST",
      url: "modul/pendaftaran_helpdesk/process.php?act=daftar_kelas_kamar_per_ruangan_ranap",
      data: {
        kdruangan: p_kdruangan,
      },
      success: function (response) {
        $("#kamar_bed_ranap").removeAttr("readonly").html(response)
      }
    });
  }

  function daftar_kelas_kamar_per_ruangan_langsung(p_kdruangan, p_kdkelas, p_pilihan_ranap) {
    $.ajax({
      type: "POST",
      url: "modul/daftar_3_isi_data_pendaftaran/ranap/process.php?act=daftar_kelas_kamar_per_ruangan_ranap",
      data: { KdRuangan: p_kdruangan, KdKelas: p_kdkelas, PilihanRanap: p_pilihan_ranap },
      success: function (response) {
        $("#kamar_bed_ranap").html(response)
      }
    });
  }

  function buat_daftar_lab(p_nocm, p_kdruangan, p_kdkelompokpasien, p_dokter, p_dokterlainnya) {
    $.ajax({
      type: "POST",
      url: "modul/pendaftaran_helpdesk/process.php?act=buat_daftar_lab",
      data: { NoCM: p_nocm, Ruangan: p_kdruangan, KdKelompokPasien: p_kdkelompokpasien, IdDokter: p_dokter, DokterPerujukLainnya: p_dokterlainnya },
      success: function (response) {
        setTimeout(() => {
          $('.js-example-basic-single').select2();
        }, 100);
        $("#menu_tambah_tindakan_lab").html(response)

        $.ajax({
          type: "POST",
          url: "modul/pendaftaran_helpdesk/process.php?act=cek_tindakan_lab",
          data: { NoCM: p_nocm },
          success: function (hasil) {
            $("#list_tindakan_lab").html(hasil)
          }
        });
      }
    });
  }

  function tambah_tindakan_lab(p_NoPendaftaran, p_TindakanLab, p_Jumlah, p_Dokter, p_NamaDokter) {
    $("#list_tindakan_lab").html('')
    $.ajax({
      type: "POST",
      url: "modul/pendaftaran_helpdesk/process.php?act=tambah_tindakan_lab",
      data: { NoPendaftaran: p_NoPendaftaran, Tindakan: p_TindakanLab, Jumlah : p_Jumlah, Dokter: p_Dokter, NamaDokter: p_NamaDokter },
      success: function (response) {
        setTimeout(() => {
          $('.js-example-basic-single').select2();
        }, 100);
        $("#list_tindakan_lab").html(response)
      }
    });
  }

  function hapus_tindakan_lab(p_NoPendaftaran, p_TindakanLab) {
    $("#list_tindakan_lab").html('')
    $.ajax({
      type: "POST",
      url: "modul/pendaftaran_helpdesk/process.php?act=hapus_tindakan_lab",
      data: { NoPendaftaran: p_NoPendaftaran, Tindakan: p_TindakanLab },
      success: function (response) {
        setTimeout(() => {
          $('.js-example-basic-single').select2();
        }, 100);
        $("#list_tindakan_lab").html(response)
      }
    });
  }

  function simpan_daftar_lab(p_NoPendaftaran) {
    $.ajax({
      type: "POST",
      url: "modul/pendaftaran_helpdesk/process.php?act=simpan_daftar_lab",
      data: { NoPendaftaran: p_NoPendaftaran },
      dataType: "JSON",
      success: function (response) {
        alert("Berhasil Mendaftarkan Pasien " + response.NoCM + " Laboratorium Dengan NoPendaftaran " + response.NoPendaftaran + ".")
        window.location.href='page.php?modul=pendaftaran'
      }
    });
  }

  function cek_data_pasien_by_nik(p_Nik) {
    $.ajax({
      type: "POST",
      url: "modul/daftar_pasien_baru/process.php?act=cek_data_pasien_by_nik",
      data: { NIK: p_Nik },
      dataType: "JSON",
      success: function (response) {
        if ( response.code == '200' ) {
          alert(response.message)
        } else {
          return false;
        }
      }
    });
  }

  
  <?php
      if ( isset($_SESSION['data_perjanjian']) ) {
        $data_perjanjian = $_SESSION['data_perjanjian'];
        echo "
          kdruangan = '" . $data_perjanjian['KdRuangan'] . "'
          kdsubinstalasi = '" . $data_perjanjian['KdSubInstalasi'] . "'
          kdkelompokpasien = '" . $data_perjanjian['KdKelompokPasien'] . "'
          if ( kdkelompokpasien == '20' ) {
            pilihrajal = 'bpjs'
          } else {
            pilihrajal = 'umum'
          }
          window.onload = simpan_daftar('" . $data_perjanjian['IdDokter'] . "', '08.00', '09.00')
        ";
      }
    ?>
</script>