<?php
  $stmt = $dbConnection->prepare("SELECT * FROM DataPegawai ORDER BY NamaLengkap");
  $stmt->execute();
  $data_pegawai = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>
<div class="modal-dialog modal-lg" role="document" id="modal-lg">
  <div class="modal-content">
    <div class="modal-body">
      <form action="modul/daftar_user/process.php?act=simpan_tambah_user" method="POST">
        <div class="form-group">
          <label for="PIN">PIN</label>
          <input type="text" class="form-control" name="PIN" id="PIN" maxlength="4" required>
        </div>
        <div class="form-group">
          <label for="NamaPegawai">Nama Pegawai</label>
          <select class="form-control js-example-basic-single" name="NamaPegawai" id="NamaPegawai" onchange="var IdPegawai = document.getElementById('IdPegawai'); IdPegawai.value = (this.options[this.selectedIndex].getAttribute('data-id')); IdPegawai.removeAttribute('readonly');">
            <option value="">--PILIH PEGAWAI--</option>
            <?php foreach ( $data_pegawai as $row ) : ?>
              <option value="<?= $row['NamaLengkap'] ?>" data-id="<?= $row['IdPegawai'] ?>"><?= $row['NamaLengkap'] ?></option>
            <?php endforeach ?>
          </select>
        </div>
        <div class="form-group">
          <label for="IdPegawai">Id Pegawai</label>
          <input type="text" class="form-control" name="IdPegawai" id="IdPegawai" required readonly>
        </div>
        <div class="form-group">
          <label for="Password">Password</label>
          <input type="text" class="form-control" name="Password" id="Password" required>
        </div>
        <div class="form-group">
          <label for="Ruangan">Ruangan</label>
          <select class="form-control js-example-basic-single" name="Ruangan" id="Ruangan">
            <option value="">--PILIH RUANGAN--</option>
            <?php
              $data_ruangan = $dbConnection->query("SELECT KdRuangan, NamaRuangan FROM Ruangan WHERE StatusEnabled = '1' ORDER BY NamaRuangan")->fetchAll(PDO::FETCH_ASSOC);
              foreach ($data_ruangan as $row) :
            ?>
            <option value="<?= $row['KdRuangan'] ?>"><?= $row['NamaRuangan'] ?></option>
            <?php
              endforeach;
            ?>
          </select>
        </div>
        <div class="form-group">
          <label for="Permissions">Hak Akses User</label>
          <select class="form-control" name="Permissions" id="Permissions">
            <option value="">--PILIH HAK AKSES--</option>
            <option value="1">SuperAdmin</option>
            <option value="2">Admin</option>
          </select>
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </form>
    </div>
  </div>
</div>