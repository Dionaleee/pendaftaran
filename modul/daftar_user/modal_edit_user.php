<?php
$stmt = $dbConnection->prepare("SELECT * FROM LoginAplikasi WHERE PIN = :pin");
$stmt->execute([ 'pin' => $_POST['PIN'] ]);
$user = $stmt->fetch(PDO::FETCH_ASSOC);
?>
<div class="modal-dialog modal-lg" role="document" id="modal-lg">
  <div class="modal-content">
    <div class="modal-body">
      <form action="modul/daftar_user/process.php?act=simpan_edit_user" method="POST">
        <div class="form-group">
          <label for="PIN_baru">PIN</label>
          <input type="text" class="form-control" name="PIN_baru" id="PIN_baru" value="<?= $user['PIN'] ?>" maxlength="4">
          <input type="hidden" class="form-control" name="PIN_lama" id="PIN_lama" value="<?= $user['PIN'] ?>">
        </div>
        <div class="form-group">
          <label for="NamaPegawai">Nama Pegawai</label>
          <input type="text" class="form-control" name="NamaPegawai" id="NamaPegawai" value="<?= $user['NamaPegawai'] ?>">
        </div>
        <div class="form-group">
          <label for="IdPegawai">Id Pegawai</label>
          <input type="text" class="form-control" name="IdPegawai" id="IdPegawai" value="<?= $user['IdPegawai'] ?>">
        </div>
        <div class="form-group">
          <label for="UserPassword">Password</label>
          <input type="text" class="form-control" name="UserPassword" id="UserPassword" value="<?= $user['UserPassword'] ?>">
        </div>
        <div class="form-group">
          <label for="Ruangan">Ruangan</label>
          <select class="form-control js-example-basic-single" name="Ruangan" id="Ruangan">
            <option value="">--PILIH RUANGAN--</option>
            <?php
              $data_ruangan = $dbConnection->query("SELECT KdRuangan, NamaRuangan FROM Ruangan WHERE StatusEnabled = '1' ORDER BY NamaRuangan")->fetchAll(PDO::FETCH_ASSOC);
              foreach ($data_ruangan as $row) :
            ?>
            <option value="<?= $row['KdRuangan'] ?>"><?= $row['NamaRuangan'] ?></option>
            <?php
              endforeach;
            ?>
          </select>
        </div>
        <div class="form-group">
          <label for="Permissions">Hak Akses User</label>
          <select class="form-control" name="Permissions" id="Permissions">
            <option value="" <?= $user['PermissionsAllowed'] == '' ? 'selected' : '' ?>>--PILIH HAK AKSES--</option>
            <option value="1" <?= $user['PermissionsAllowed'] == '1' ? 'selected' : '' ?>>SuperAdmin</option>
            <option value="2" <?= $user['PermissionsAllowed'] == '2' ? 'selected' : '' ?>>Admin</option>
          </select>
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </form>
    </div>
  </div>
</div>
