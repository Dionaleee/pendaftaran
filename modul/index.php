<?php
if ( $_GET['modul'] == 'pilih_pendaftaran' ) {
  include('daftar_2_pilih_pendaftaran/index.php');
} elseif ( $_GET['modul'] == 'daftar_rajal' ) {
  include('daftar_3_isi_data_pendaftaran/rajal/pilih_umum_bpjs.php');
} elseif ( $_GET['modul'] == 'daftar_ranap' ) {
  include('daftar_3_isi_data_pendaftaran/ranap/pilih_umum_bpjs.php');
} elseif ( $_GET['modul'] == 'poli' ) {
  include('daftar_3_isi_data_pendaftaran/rajal/poli.php');
} elseif ( $_GET['modul'] == 'isi_data_ranap' ) {
  include('daftar_3_isi_data_pendaftaran/ranap/daftar_ranap.php');
} elseif ( $_GET['modul'] == 'daftar_pasien' ) {
  include('daftar_pasien/index.php');
} elseif ( $_GET['modul'] == 'daftar_pasien_perjanjian' ) {
  include('daftar_pasien/pasien_perjanjian.php');
} elseif ( $_GET['modul'] == 'daftar_pasien_baru' ) {
  include('daftar_pasien_baru/index.php');
} elseif ( $_GET['modul'] == 'user' ) {
  include('daftar_user/index.php');
} elseif ( $_GET['modul'] == 'laporan' ) {
  if ( $_GET['sub_modul'] == 'kunjungan_bulanan' ) {
    include('laporan/kunjungan_bulanan.php');
  } elseif ( $_GET['sub_modul'] == 'kunjungan_harian' ) {
    include('laporan/kunjungan_harian.php');
  } elseif ( $_GET['sub_modul'] == 'rekap_perbulan' ) {
    include('laporan/rekap_perbulan.php');
  } elseif ( $_GET['sub_modul'] == 'kunjungan_per_wilayah' ) {
    include('laporan/kunjungan_per_wilayah.php');
  } elseif ( $_GET['sub_modul'] == 'rujukan_rawat_jalan' ) {
    include('laporan/rujukan_rawat_jalan.php');
  }
} elseif ( $_GET['modul'] == 'data_pasien' ) {
  include('data_pasien/index.php');
} elseif ( $_GET['modul'] == 'cari_pasien' ) {
  include('data_pasien/cari_pasien.php');
} elseif ( $_GET['modul'] == 'rekam_sesi_pasien_untuk_pendaftaran_helpdesk' ) {
  $data_perjanjian = $dbConnection->query("SELECT a.* FROM Booking_Online a LEFT JOIN Pasien b ON a.NoCM = b.NoCM LEFT JOIN (SELECT TOP 1 * FROM AsuransiPasien WHERE NoCM = '$_GET[NoCM]' OR IdAsuransi = '$_GET[NoCM]' ORDER BY TglBerlaku DESC) c ON a.NoCM = c.NoCM
  WHERE (a.NoCM = '$_GET[NoCM]' OR b.NoIdentitas = '$_GET[NoCM]' OR c.IdAsuransi = '$_GET[NoCM]') AND CONVERT(VARCHAR(10), a.TglPerjanjian, 20) = CONVERT(VARCHAR(10), GETDATE(), 20) AND StatusKehadiran = '0' ORDER BY a.TglPerjanjian DESC")->fetch(PDO::FETCH_ASSOC);

  if ( $data_perjanjian ) {
    $_SESSION['data_perjanjian'] = $data_perjanjian;
  }

  $stmt = $dbConnection->prepare('SELECT TOP 1 a.*, b.IdAsuransi, dbo.S_HitungUmur(a.TglLahir, getdate()) AS UmurPasien FROM Pasien a LEFT JOIN (SELECT TOP 1 * FROM AsuransiPasien WHERE NoCM = ? OR IdAsuransi = ? AND IdPenjamin = ? ORDER BY TglBerlaku DESC) b ON a.NoCM = b.NoCM WHERE a.NoCM = ? OR a.NoIdentitas = ? OR b.IdAsuransi = ? ORDER BY a.TglDaftarMembership DESC');
  for ($i=1; $i <= 6; $i++) { 
    $stmt->bindParam($i, $_GET['NoCM']);
  }
  $stmt->execute();
  $row = $stmt->fetch(PDO::FETCH_ASSOC);
  $_SESSION['data_pasien'] = $row;
  $NoCM = $_GET['NoCM'];
  echo "<script>window.location = 'page.php?modul=pendaftaran_helpdesk&NoCM=$NoCM'</script>";
} elseif ( $_GET['modul'] == 'pendaftaran_helpdesk' ) {
  include('pendaftaran_helpdesk/index.php');
} elseif ( $_GET['modul'] == 'redirect_ke_pendaftaran_helpdesk' ) {
  $NoCM = $_SESSION['data_pasien']['NoCM'];
  echo "<script>window.location = 'page.php?modul=pendaftaran_helpdesk&NoCM=$NoCM'</script>";
}