<?php
  $stmt = $dbConnection->prepare("SELECT NamaTitle FROM Title WHERE StatusEnabled = '1'");
  $stmt->execute();
  $title = $stmt->fetchAll(PDO::FETCH_ASSOC);
  
  $stmt = $dbConnection->prepare("SELECT Singkatan, JenisKelamin FROM JenisKelamin WHERE StatusEnabled = '1'");
  $stmt->execute();
  $jenis_kelamin = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>
<div style="margin-bottom: 10vh;">
  <form action="modul/daftar_pasien_baru/process.php" method="POST">
    <!-- <input type="hidden" name="NoCM" value=""> -->
    <input type="hidden" name="act" value="daftar_pasien_baru">
    <div class="card mb-3 shadow-sm">
      <div class="card-body my-0 py-3">
        <h6 class="card-title font-weight-bold">Data Pasien</h6>
        <div class="form-row">
          <div class="form-group col-md-2">
            <label for="NoCM">No RM</label>
            <input class="form-control" type="text" name="NoCM" id="NoCM" readonly>
          </div>
          <div class="form-group col-md-1">
            <label for="NamaDepan">Nama Depan</label>
            <select name="NamaDepan" id="NamaDepan" class="form-control">
              <option value="-">Pilih...</option>
              <?php foreach ( $title as $row ) : ?>
                <option value="<?= $row['NamaTitle'] ?>"><?= $row['NamaTitle'] ?></option>
              <?php endforeach ?>
            </select>
          </div>
          <div class="form-group col-md-5">
            <label for="NamaLengkap">Nama Lengkap</label>
            <input type="text" class="form-control" name="NamaLengkap" id="NamaLengkap" value="" required>
          </div>
          <div class="form-group col-md-3">
            <label for="NoIdentitas">No. Identitas (KTP/SIM/...)</label>
            <input type="text" class="form-control" name="NoIdentitas" id="NoIdentitas" value="" required>
          </div>
          <div class="form-group col-md-1">
            <label class="d-none d-md-block">&nbsp;</label>
            <div class="btn btn-info w-100" onclick="if(document.getElementById('NoIdentitas').value == '') { alert('NIK Tidak Boleh Kosong!'); return false; } else { cek_data_pasien_by_nik(document.getElementById('NoIdentitas').value) }">cek</div>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-2">
            <label for="JenisKelamin">Jenis Kelamin</label>
            <select name="JenisKelamin" id="JenisKelamin" class="form-control" required>
              <option value="">Pilih...</option>
              <?php foreach ( $jenis_kelamin as $row ) : ?>
                <option value="<?= $row['Singkatan'] ?>"><?= $row['JenisKelamin'] ?></option>
              <?php endforeach ?>
            </select>
          </div>
          <div class="form-group col-md-3">
            <label for="TempatLahir">Tempat Lahir</label>
            <input type="text" class="form-control" name="TempatLahir" id="TempatLahir" value="">
          </div>
          <div class="form-group col-md-2">
            <label for="TglLahir">Tanggal Lahir</label>
            <input type="date" class="form-control" name="TglLahir" id="TglLahir" value="" required>
          </div>
        </div>
      </div>
    </div>
    <div class="card mb-3 shadow-sm">
      <div class="card-body my-0 py-3">
        <h6 class="card-title font-weight-bold">Alamat Pasien</h6>
        <div class="form-row">
          <div class="form-group col-md-4">
            <label for="AlamatLengkap">Alamat Lengkap</label>
            <input type="text" class="form-control" name="Alamat" id="AlamatLengkap" value="">
          </div>
          <div class="form-group col-md-2">
            <label for="RTRW">RT/RW</label>
            <input type="text" class="form-control" name="RTRW" id="RTRW" value="" placeholder="00/00">
          </div>
          <div class="form-group col-md-2">
            <label for="Telepon">Telepon</label>
            <input type="text" class="form-control" name="Telepon" id="Telepon" value="">
          </div>
          <div class="form-group col-md-4">
            <label for="kelurahan">Kelurahan</label>
            <input type="text" class="form-control" name="Kelurahan" id="kelurahan" value="" required>
            <div id="suggesstion-box" style="position: absolute; z-index: 9999;"></div>
          </div>
          <div class="form-group col-md-3">
            <label for="kecamatan">Kecamatan</label>
            <input type="text" class="form-control" name="Kecamatan" id="kecamatan" value="">
          </div>
          <div class="form-group col-md-3">
            <label for="kota">Kota (Kabupaten)</label>
            <input type="text" class="form-control" name="Kota" id="kota" value="">
          </div>
          <div class="form-group col-md-3">
            <label for="provinsi">Provinsi</label>
            <input type="text" class="form-control" name="Propinsi" id="provinsi" value="">
          </div>
          <div class="form-group col-md-3">
            <label for="kodepos">KodePos</label>
            <input type="text" class="form-control" name="KodePos" id="kodepos" value="">
          </div>
          <div class="form-group col-md-2">
            <button class="btn btn-primary col-12" type="submit">Simpan</button>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>