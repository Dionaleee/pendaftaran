<?php
include('../../tong_sys/sqlsrv.php');
if ( $_GET['act'] == 'do_login' ) {
  $pin = $_POST['pin'];
  $pass = $_POST['pass'];
  
  if ( empty($pin) || empty($pass) ) {
    echo "<script>alert('pin atau password tidak boleh kosong!'); window.location.href='../../index.php'</script>";
  } else {
    $stmt = $dbConnection->prepare('SELECT * FROM LoginAplikasi WHERE PIN = :pin AND UserPassword = :pass');
    $stmt->execute([ 'pin' => $pin, 'pass' => $pass ]);
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    // var_dump($row); exit();
    if ( $row ) {
      session_start();
      /* buat session */
      $_SESSION['PIN_ADM'] = $row['PIN'];
      $_SESSION['IdPegawai_ADM'] = $row['IdPegawai'];
      $_SESSION['NamaPegawai_ADM'] = $row['NamaPegawai'];
      $_SESSION['Username_ADM'] = $row['Username'];
      $_SESSION['UserPassword_ADM'] = $row['UserPassword'];
      $_SESSION['KdRuangan_ADM'] = $row['KdRuangan'];
      $_SESSION['PermissionsAllowed_ADM'] = $row['PermissionsAllowed'];
      $_SESSION['TempatLogin_ADM'] = $_POST['klinik_bidan'];
  
      header("Location: ../../page.php?modul=pendaftaran");
    } else {
      echo "<script>alert('pin atau password tidak ditemukan!'); window.location.href='../../index.php'</script>";
    }
  }
} elseif ( $_GET['act'] == 'do_logout' ) {
  header("Location: ../../index.php");
}